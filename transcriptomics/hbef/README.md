# Effects of Acid Deposition on Root Microbiome Symbiosis
### Hubbard Brook Experimental Forest in North Woodstock, NH
Contact: Alex Trouern-Trend (alexander.trouern-trend@uconn.edu)
Plant Computational Genomics Laboratory

---

### Brief
1. Generate new _Acer saccharum_ and _Fagus grandifolia_ transcriptomes and 16S rRNA soil community libraries from field experimental plots that simulate a gradient of acid deposition affected soils
2. Associate new data with historical phenotypic and biogeochemical data from Hubbard Brook Experimental Forest to query relationship between _A. saccharum_ decline and potential perturbations in the tripartite system of soil chemistry, root microbiome and forest tree interactions.

### Background
Nutrient availability shifts caused by acid deposition are believed to be largely responsible for changing tree demographics in the Northeastern hardwood forests. Nutritionally important cations in the soil are displaced by excess H+ and cationic pollutants, while the bioavailability of phytotoxic and antimicrobial Al cations increases (Kunito et al. 2016). Field growth performance monitoring has pointed towards sugar maple decline unless the soils are restored (Juice et al. 2006; Green et al. 2013). Contrarily, American beech is performing well in exacerbated cation depleted soils (Halman et al. 2014). Controlled field experiments have examined the effects of Ca and Al treatments when applied to the soil. Dominant sugar maples (Acer saccharum) remained unaffected but non-dominant trees responded positively to Ca amendment. On the other hand, American beech (Fagus grandifolia) grew faster in Al amended plots filling the void remaining after increased tree mortality (Halman et al. 2014).

With its unprecendented biogeochemical data (since 1963) and 12 Nutrient perturbation (NuPert) plots (45m x 45m, est. 1995), the Long-Term Ecological Research Site (LTER), Hubbard Brook Experimental Forest (HBEF) provides an ideal arrangement for understanding the consequences of acid deposition. These experimental sites for Ca depletion (4 control plots), Ca amendment (4 plots that have been restored to historic Ca levels by controlled addition of wollastonite) and Al addition (4 plots) (Schaberg and Hawley 2010, Schaberg et al. 2011, Halman et al. 2013) have been intensely studied to understand the demographic and physiological effects of the treatments. This wealth of data makes HBEF is an excellent phenotypic framework to complement and associate with plant transcriptomic and microbial community structure data. Understanding correlations between gene expression, soil condition and microbial community structure could help predict future trajectories of forest composition. Furthermore, the genetic mechanisms of Ca deprivation and Al toxicity tolerance or susceptibility in the beech and maple populations under these new selective pressures could be exposed by making drawing connections between new sequence data and preexisting phenotypic and environmental data.

This project aims to begin to elucidate the tripartite system of soil chemistry, root microbiome and forest tree interactions using robust soil community and transcriptomic sampling from across 9 NuPert plots (three of each condition; Al addition, Ca remediation and control) for each of the two dominant forest trees (sugar maple and American beech) and across two sampling seasons (spring and autumn). A dataset of 36 transcriptomes and 36 16S rRNA libraries to represent community structures from the base of each sampled tree will be compared and associated with existing ecological data as an attempt to achieve project aims.

### Design
(36 transcriptomes, 36 microbiomes)
- 2 forest tree species (_Acer saccharum_,  _Fagus grandifolia_)
- 3 soil conditions (Calcium amendent, Aluminum addition, control)
- 2 sampling seasons (spring, autumn)
- triplicates

### Sampling

- Saplings < 21 cm in diameter trunk width.
- Stem tissue sampled from 5 years of growth (measured visibly by spring wood bands). Here it is notable that although 5 years of growth was selected, the distance up the stem that we had to cut to reach 5 years was highly variable, with beech always demonstrating much ore annual growth.
- All stem samples were immediately flash frozen in liquid Nitrogen and kept at or below -80 F until extraction.

Sampling dates
- Fall sampling (Oct 13-15, 2017)
- Spring sampling (May 13-18, 2018) 



**Sample key**

| sample  | season | species | condition | replicate | coordinates                           | circumference (cm) | trimmed reads |
| :-----  | :----- | :-----  | :-------- | :-------  | :----------------------------------   | :----------------- | :-----------  |
| faasal1 | fall   | _Acer_  | Aluminum  | 1         | 43.97637999999999, -71.71188833333333 | 20.5               | 9422252 |
| faasal2 | fall | _Acer_ | Aluminum | 2 | 43.94236333333334, -71.711485 | 12 | 10911286 |
| faasal3 | fall | _Acer_ | Aluminum | 3 | 43.927735000000006, -71.74913166666667 | 15.4 | 8402577 |
| faasca1 | fall | _Acer_ | Calcium | 1 | 43.93117666666668, -71.73323 | 8.3 | 10494297 |
| faasca2 | fall | _Acer_ | Calcium | 2 | 43.93546499999999, -71.77200333333334 | 7 | 10089867 |
| faasca3 | fall | _Acer_ | Calcium | 3 | 43.91801166666666, -71.73980499999999 | 25 | 8046092 |
| faasco1 | fall | _Acer_ | control | 1 | 43.947726666666675, -71.73404500000001 | 21 | 10482979 |
| faasco2 | fall | _Acer_ | control | 2 | 43.96314833333333, -71.77731499999999 | 9.3 | 8805358 |
| faasco3 | fall | _Acer_ | control | 3 | 43.929526666666675, -71.78209 | 13.4 | 8954588 |
| fafgal1 | fall | _Fagus_ | Aluminum | 1 | 43.987475, -71.74856833333332 | 9 | 10070864 |
| fafgal2 | fall | _Fagus_ | Aluminum | 2 | 43.95547666666667, -71.76467833333335 | 5.8 | 11025108 |
| fafgal3 | fall | _Fagus_ | Aluminum | 3 | 43.94704333333334, -71.73243333333335 | 13 | 9677788 |
| fafgca1 | fall | _Fagus_ | Calcium | 1 | 43.93977666666668, -71.75951666666667 | 5 | 11694676 |
| fafgca2 | fall | _Fagus_ | Calcium | 2 | 43.95189333333334, -71.77865499999999 | 12 | 9286516 |
| fafgca3 | fall | _Fagus_ | Calcium | 3 | 43.94704333333334, -71.73243333333335 | 13 | 9886949 |
| fafgco1 | fall | _Fagus_ | control | 1 | 43.959718333333335, -71.75773500000001 | 8.5 | 9138775 |
| fafgco2 | fall | _Fagus_ | control | 2 | 43.988145, -71.74650333333334 | 8.3 | 10525308 |
| fafgco3 | fall | _Fagus_ | control | 3 | 43.95573666666667, -71.77841166666667 | 23 | 9871462 |
| spasal1 | spring | _Acer_ | Aluminum | 1 | - | - | 10990522 |
| spasal2 | spring | _Acer_ | Aluminum | 2 | - | - | 10920105 |
| spasal3 | spring | _Acer_ | Aluminum | 3 | - | - | - |
| spasca1 | spring | _Acer_ | Calcium | 1 | - | - | 14026987 |
| spasca2 | spring | _Acer_ | Calcium | 2 | - | - | 6801599 |
| spasca3 | spring | _Acer_ | Calcium | 3 | - | - | - |
| spasco1 | spring | _Acer_ | control | 1 | - | - | 9366050 |
| spasco2 | spring | _Acer_ | control | 2 | - | - | 10617751 |
| spasco3 | spring | _Acer_ | control | 3 | - | - | 9366050 |
| spfgal1 | spring | _Fagus_ | Aluminum | 1 | - | - | 10894808 |
| spfgal2 | spring | _Fagus_ | Aluminum | 2 | - | - | 13406933 |
| spfgal3 | spring | _Fagus_ | Aluminum | 3 | - | - | - |
| spfgca1 | spring | _Fagus_ | Calcium | 1 | - | - | 21636354 |
| spfgca2 | spring | _Fagus_ | Calcium | 2 | - | - | 10602321 |
| spfgca3 | spring | _Fagus_ | Calcium | 3 | - | - | 12060807 |
| spfgco1 | spring | _Fagus_ | control | 1 | - | - | 10790770 |
| spfgco2 | spring | _Fagus_ | control | 2 | - | - | 7695026 |
| spfgco3 | spring | _Fagus_ | control | 3 | - | - | 14503357 |

### 1. cDNA libraries Quality Control
**Summary:** Thirty-six libraries (triplicates across three soil conditions, two species and two seasons) were generated using paired-end sequencing on Illumina HiSeq 2500 at ??? coverage. The reads were quality controlled usng fastQC and trimmed using sickle. .

**Data availability:** 
- Quality controlled & trimmed read sets are available in .fastq format in the following directory: `/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed`.
- Singles are available in a subdirectory of the above location. 
- For readsets pre-QC and trimming: `/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_cat`

**scripts:**
- [fastqc.sh](../scripts/fastqc.sh)
- [sickle.sh](../scripts/sickle.sh)

**Heatmaps:** Details of FastQC summaries with transcriptomes grouped by season and species. Both R1 and R2 paired libraries shown for each transcriptome. Fall results for _Acer saccharum_ shown. Follow links for additional heatmaps.

![hm_faas.png](images/hm_faas.png)
[hm_fafg.png](images/hm_fafg.png)
[hm_spas.png](images/hm_spas.png)
[hm_spfg.png](images/hm_spfg.png)

### 2. Genome Indexing
**Summary:** HISAT2 (v 2.2.0) indexed most recent _Fagus grandifolia_ genome. 
**Data availability:** Indices available `/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/hisat/build` under prefix fagr_allmaps

**hisatBuild.sh** - created indices for recent _Fagus grandifolia_ assembly, `wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta`

```bash
#SBATCH --job-name=hisat
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --cpus-per-task=20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=250G
#SBATCH -o hisat_%j.out
#SBATCH -e hisat_%j.err

module load hisat2/2.2.0

basedir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes"
genome="wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta"

hisat2-build -p 20 -f "$basedir/$genome"  fagr_allmaps

```

### 3. HISAT2 Read Mapping
**Summary:** Reads from each library were mapped to the indexed reference genome to produce SAM files, which were coordinate sorted for input to Trinity Genomne-Guided De-Novo Assembly.

**hisat.sh** - Aligns trimmed and QC fastq reads to indexed genome.
- _--dta_ flag reports alignments tailored for transcript assemblers
- _-1_ and _-2_ flags because transcriptome constructed using paired end sequencing. 

```bash
#!/bin/bash
#SBATCH --job-name=hisatfg
#SBATCH -o hisat-%j.out
#SBATCH -e hisat-%j.err
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=14
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=180G

module load hisat2/2.2.0

trimmeddir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed"
genomeidx="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes/fagr_allmaps"

FaFgAl1_1=$trimmeddir"/FaFgAl1_R1_trimmed.fastq"
FaFgAl1_2=$trimmeddir"/FaFgAl1_R2_trimmed.fastq"

SpFgCo1_1=$trimmeddir"/SpFgCo1_R1_trimmed.fastq"
SpFgCo1_2=$trimmeddir"/SpFgCo1_R2_trimmed.fastq"

hisat2 -p 12 --dta -x $genomeidx -1 $FaFgAl1_1 -2 $FaFgAl1_2 -S FaFgAl1.sam
hisat2 -p 12 --dta -x $genomeidx -1 $SpFgCo1_1 -2 $SpFgCo1_2 -S SpFgCo1.sam
```

**samsort.sh** - coordinate sorting output .sam files from hisat.
```bash
#!/bin/bash
#SBATCH --job-name=samsort
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=120G
#SBATCH -o samsort_%j.out
#SBATCH -e samsort_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load samtools

for i in *Fg**; do
    name=$(echo $i | cut -d'.' -f 1);
    echo "$name";
    echo "$i";
    samtools sort -@ 16 -o ${name}.bam ${name}.sam;
    done;
```

### Trinity Genome-Guided De novo Transcriptome Assembly for _Fagus grandifolia_
**Summary:** Based on the [Trinity Docs](https://github.com/trinityrnaseq/trinityrnaseq/wiki/Genome-Guided-Trinity-Transcriptome-Assembly), it was not clear that the genome-guided assembly feature would produce a higher quality assembly of the _F. grandifolia_ transcriptome libraries than the standard de novo approach. In the genome-guided de novo approach, the reads are first mapped to a reference genome to form clusters which are then assembled separately using the standard Trinity _De novo_ proocess. In my case, a highly contiguous hybrid read assembly (unreleased) was used as the reference for a Trinity (v. 2.8.5) genome-guided de novo assembly approach. By comparing these assemblies to others constructed using the standard Trinity non-guided approach (from an earlier version, v. 2.5.1) I observed a significant descrease in average transcript length, a doubling of monoexonic transcripts and a slight (insignificant?) decrease in the average percent identities and coverage of the alignments back to genomes for the genome-guided denovo assemblies.

```
GFACS STATS
```

##### Averages across transcriptome features & alignment

** In the following figures, blue bars represent the genome-guided approach **

For each library the total number of transcripts aligning back to genome is ~ 15,000 greater in genome-guided
[Assemby size](images/assemblylens.png)

Most of these transcripts are short and monoexonic
[Assemby size](images/transcriptlens.png)
[transcript length](images/Len.spfgco1.png)
[transcript length](images/Len.fafgal1.png)

When aligning assembled transcriptomes back to genome (GMAP), percent coverage was sligbtly higher in genome-guided
[alignment coverage](images/coverage.png)
[GMAP alignment coverage](images/Cov.spfgco1.png)
[GMAP alignment coverage](images/Cov.fafgal1.png)

**Percent Identity Distributions**
[GMAP alignment % id](images/Pid.spfgco1.png)
[GMAP alignment % id](images/pid.fafgal1.png)

**Exon Count Distributions**
[exon count](images/Ex.spfgco1.png)
[exon count](images/Ex.fafgal1.png)


