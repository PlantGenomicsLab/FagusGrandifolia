# Differential Expression Analysis of the American Beech Using a Reference Genome

## Introduction

Directory is /labs/Wegrzyn/DE_Beech

**Total Reads**

 |  | FaFgAl1| FaFgAl2| FaFgAl3 | FaFgCa1 | FaFgCa2 | FaFgCa3 | FaFgCo1 | FaFgCo2 | FaFgCo3 | SpFgAl1 | SpFgAl2 | SpFgAl3 | SpFgCa1 | SpFgCa2 | SpFgCa3 | SpFgCo1 | SpFgCo2 | SpFgCo3 |
| ------ | ------ | ------| ------| ------| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Reads | 22723527 | 24711396 | 21843215 | 26112531 | 20912498 | 22522022 | 20603668 | 23612910 | 22270529 | 24305936 | 29807460 | 0 | 45875763 | 23837062 | 26776104 | 24164840 | 17187359 | 32271224 |

## HISAT2

Align reads to reference genome.

```
#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=100G
#SBATCH -o hisat2_%j.out
#SBATCH -e hisat2_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in FaFgAl1 FaFgAl2 FaFgAl3 FaFgCa1 FaFgCa2 FaFgCo1 FaFgCo2 FaFgCo3 SpFgAl1 SpFgAl2 SpFgCa1 SpFgCa2 SpFgCa3 SpFgCo1 SpFgCo2 SpFgCo3
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/fastq_trimmed/fastq_trimmed/${i}_R1_trimmed.fastq" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/fastq_trimmed/fastq_trimmed/${i}_R2_trimmed.fastq" -p 16 -S "${i}.sam" 
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 
```

## Mapping

**Number of Mapped Reads for Reference Genome**

|  | FaFgAl1 | FaFgAl2 | FaFgAl3 | FaFgCa1 | FaFgCa2 | FaFgCa3 | FaFgCo1 | FaFgCo2 | FaFgCo3 | SpFgAl1 | SpFgAl2 | SpFgAl3 | SpFgCa1 | SpFgCa2 | SpFgCa3 | SpFgCo1 | SpFgCo2 | SpFgCo3 |
| ------ | ------ | ------ |------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Mapped Reads | 21674386 | 23501276| 20819932 | 24750835 | 19983590 | 21271878 | 19596182 | 21934993 | 21004811 | 22940942 | 27842824 | 0 | 21912749 | 22193465 | 25458444 | 22334344 | 15700984 | 30248903 |

**Mapping Rate for Reference Genome**

|  | FaFgAl1 | FaFgAl2 |FaFgAl3 | FaFgCa1 | FaFgCa2 | FaFgCa3 |FaFgCo1 | FaFgCo2 | FaFgCo3 | SpFgAl1 | SpFgAl2 | SpFgCa1 | SpFgCa2 | SpFgCa3 | SpFgCo1 | SpFgCo2 | SpFgCo3 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Mapping Rate | 95.38% | 95.01%| 95.32% | 94.79%| 95.56% |94.45% | 95.11% | 92.89% | 94.32% | 94.38% | 93.41% | 47.77% | 93.10% | 95.08% | 92.42%| 91.35% | 93.73% |

The average mapping rate for the reference genome analysis is 85.7%, however, SpFgAl3 had a mapping rate of 0%. If it is are removed, the mapping rate is 91.0%.

**Mapping Rate for De Novo Transcriptome**

|  | FaFgAl1 | FaFgAl2 | FaFgAl3 | FaFgCa1 | FaFgCa2 | FaFgCo1 | FaFgCo2 | FaFgCo3 | SpFgAl1 | SpFgAl2 | SpFgCa2 | SpFgCa3 | SpFgCo1 | SpFgCo3 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Mapping Rate | 71.02% | 70.79%| 70.09% |74.38% | 69.40% | 71.86% | 73.84% | 20.86% | 73.12% | 74.05% | 72.78% | 71.18% | 69.78% | 69.69% |

The average mapping rate for the de novo analysis is 68.08%.

## HTSeq

Generate total read counts.

```
#!/bin/bash
#SBATCH --job-name=htseq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=20G
#SBATCH --mail-user=allyson.derry@uconn.edu
#SBATCH -o htseq%j.out
#SBATCH -e htseq%j.err

module load htseq/0.11.2

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgAl1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgAl1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgAl2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgAl2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgAl3.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgAl3.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgCa1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgCa1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgCa2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgCa2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgCo1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgCo1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgCo2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgCo2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_FaFgCo3.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > FaFgCo3.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgAl1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 > SpFgAl1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgAl2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgAl2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgAl3.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgAl3.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCa1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCa1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCa2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCa2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCa3.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCa3.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCo1.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCo1.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCo2.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCo2.counts

htseq-count -s no -r pos -i Parent -f bam sorted_SpFgCo3.bam /projects/EBP/Wegrzyn/fagr/uconn/annotation/gfacs_renamed/gfacs_o/out.gff3 >SpFgCo3.counts
```

## Compare PCA plots
PCA plots are compared removing SpFgCa1 and then SpFgCo2. The PCA comparisons are the in /PCA_Comparisons/ directory.

**DESeq2**

DESeq2 is used to find the differentially expressed genes for all of the samples.

```
library("DESeq2")
directory<-getwd()
list.files()

sample_fa_al<-c("FaFgAl1,","FaFgAl2","FaFgAl3")
sample_fa_ca<-c("FaFgCa1","FaFgCa2","FaFgCa3")
sample_fa_co<-c("FaFgCo1","FaFgCo2","FaFgCo3")
sample_sp_al<-c("SpFgAl1","SpFgAl2")
sample_sp_ca<-c("SpFgCa1","SpFgCa2","SpFgCa3")
sample_sp_co<-c("SpFgCo1","SpFgCo2","SpFgCo3")
sample<-c(sample_fa_al,sample_fa_ca,sample_fa_co,sample_sp_al,sample_sp_ca,sample_sp_co)
sample_fa<-c(sample_fa_al,sample_fa_ca,sample_fa_co)
sample_sp<-c(sample_sp_al,sample_sp_ca,sample_sp_co)

type<-c(rep("Al",3),rep("Ca",3), rep("Co",3),rep("Al",2),rep("Ca",3),rep("Co",3))
type_fa<-c(rep("Al",3),rep("Ca",3),rep("Co",3))
type_sp<-c(rep("Al",2),rep("Ca",3),rep("Co",3))

sampleNames <- sample
sampleCondition <- type
sampleFiles <- list.files(pattern = "Al|Ca|Co")
sampleTable <- data.frame(sampleName = sampleNames,fileName = sampleFiles,condition = sampleCondition)
ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable,directory = directory,design = ~ condition)

sampleNames_fa <- sample_fa
sampleCondition_fa <- type_fa
sampleFiles_fa <- list.files(pattern = "FaFgAl|FaFgCa|FaFgCo")
sampleTable_fa <- data.frame(sampleName = sampleNames_fa,fileName = sampleFiles_fa,condition = sampleCondition_fa)
ddsHTSeq_fa <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable_fa,directory = directory,design = ~ condition)

sampleNames_sp <- sample_sp
sampleCondition_sp <- type_sp
sampleFiles_sp <- list.files(pattern = "SpFgAl|SpFgCa|SpFgCo")
sampleTable_sp <- data.frame(sampleName = sampleNames_sp,fileName = sampleFiles_sp,condition = sampleCondition_sp)
ddsHTSeq_sp <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable_sp,directory = directory,design = ~ condition)

treatments <- c("Ca","Co","Al")

colData(ddsHTSeq)$condition <- factor(colData(ddsHTSeq)$condition,levels = treatments)
colData(ddsHTSeq_fa)$condition <- factor(colData(ddsHTSeq_fa)$condition,levels = treatments)
colData(ddsHTSeq_sp)$condition <- factor(colData(ddsHTSeq_sp)$condition,levels = treatments)
colData(ddsHTSeq)$condition <- relevel(colData(ddsHTSeq)$condition, ref = "Ca")
colData(ddsHTSeq_fa)$condition <- relevel(colData(ddsHTSeq_fa)$condition, ref = "Ca")
colData(ddsHTSeq_sp)$condition <- relevel(colData(ddsHTSeq_sp)$condition, ref = "Ca")
ddsHTSeq$condition
```

Generate the differentially expressed genes. 

```
dds <- DESeq(ddsHTSeq)
dds_fa <- DESeq(ddsHTSeq_fa)
dds_sp <- DESeq(ddsHTSeq_sp)

res_co_ca <- results(dds, contrast=c("condition","Co","Ca"))
res_al_ca <- results(dds, contrast=c("condition","Al","Ca"))
res_al_co <- results(dds, contrast=c("condition","Al","Co"))

res_co_ca_fa <- results(dds_fa, contrast=c("condition","Co","Ca"))
res_al_ca_fa <- results(dds_fa, contrast=c("condition","Al","Ca"))
res_al_co_fa <- results(dds_fa, contrast=c("condition","Al","Co"))

res_co_ca_sp <- results(dds_sp, contrast=c("condition","Co","Ca"))
res_al_ca_sp <- results(dds_sp, contrast=c("condition","Al","Ca"))
res_al_co_sp <- results(dds_sp, contrast=c("condition","Al","Co"))

res_co_ca= subset(res_co_ca,padj<0.1)
res_al_ca= subset(res_al_ca,padj<0.1)
res_al_co= subset(res_al_co,padj<0.1)
res_co_ca_fa= subset(res_co_ca_fa,padj<0.1)
res_al_ca_fa= subset(res_al_ca_fa,padj<0.1)
res_al_co_fa= subset(res_al_co_fa,padj<0.1)
res_co_ca_sp= subset(res_co_ca_sp,padj<0.1)
res_al_ca_sp= subset(res_al_ca_sp,padj<0.1)
res_al_co_sp= subset(res_al_co_sp,padj<0.1)

res_co_ca <- res_co_ca[order(res_co_ca$padj),]
res_al_ca <- res_al_ca[order(res_al_ca$padj),]
res_al_co <- res_al_co[order(res_al_co$padj),]
res_co_ca_fa <- res_co_ca_fa[order(res_co_ca_fa$padj),]
res_al_ca_fa <- res_al_ca_fa[order(res_al_ca_fa$padj),]
res_al_co_fa <- res_al_co_fa[order(res_al_co_fa$padj),]
res_co_ca_sp <- res_co_ca_sp[order(res_co_ca_sp$padj),]
res_al_ca_sp <- res_al_ca_sp[order(res_al_ca_sp$padj),]
res_al_co_sp <- res_al_co_sp[order(res_al_co_sp$padj),]

resdata_co_ca <- merge(as.data.frame(res_co_ca),as.data.frame(counts(dds,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_ca <- merge(as.data.frame(res_al_ca),as.data.frame(counts(dds,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_co <- merge(as.data.frame(res_al_co),as.data.frame(counts(dds,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_co_ca_fa <- merge(as.data.frame(res_co_ca_fa),as.data.frame(counts(dds_fa,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_ca_fa <- merge(as.data.frame(res_al_ca_fa),as.data.frame(counts(dds_fa,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_co_fa <- merge(as.data.frame(res_al_co_fa),as.data.frame(counts(dds_fa,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_co_ca_sp <- merge(as.data.frame(res_co_ca_sp),as.data.frame(counts(dds_sp,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_ca_sp <- merge(as.data.frame(res_al_ca_sp),as.data.frame(counts(dds_sp,normalized =TRUE)), by = 'row.names', sort = FALSE)
resdata_al_co_sp <- merge(as.data.frame(res_al_co_sp),as.data.frame(counts(dds_sp,normalized =TRUE)), by = 'row.names', sort = FALSE)

names(resdata_co_ca)[1] <- 'gene'
names(resdata_al_ca)[1] <- 'gene'
names(resdata_al_co)[1] <- 'gene'
names(resdata_co_ca_fa)[1] <- 'gene'
names(resdata_al_ca_fa)[1] <- 'gene'
names(resdata_al_co_fa)[1] <- 'gene'
names(resdata_co_ca_sp)[1] <- 'gene'
names(resdata_al_ca_sp)[1] <- 'gene'
names(resdata_al_co_sp)[1] <- 'gene'

write.csv(resdata_co_ca, file = paste0("all_co_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_ca, file = paste0("all_al_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_co, file = paste0("all_al_co", "-results-with-normalized.csv"))
write.csv(resdata_co_ca_fa, file = paste0("fall_co_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_ca_fa, file = paste0("fall_al_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_co_fa, file = paste0("fall_al_co", "-results-with-normalized.csv"))
write.csv(resdata_co_ca_sp, file = paste0("sp_co_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_ca_sp, file = paste0("sp_al_ca", "-results-with-normalized.csv"))
write.csv(resdata_al_co_sp, file = paste0("sp_al_co", "-results-with-normalized.csv"))
```

Generate the volcano plots.

```
library(EnhancedVolcano)
all<-as.data.frame(res_co_ca)
all['treatment']='all_co_ca'
fall<-as.data.frame(res_co_ca_fa)
fall['treatment']='fall_co_ca'
sp<-as.data.frame(res_co_ca_sp)
sp['treatment']='sp_co_ca'

all_al<-as.data.frame(res_al_ca)
all_al['treatment']='all_al_ca'
fall_al<-as.data.frame(res_al_ca_fa)
fall_al['treatment']='fall_al_ca'
sp_al<-as.data.frame(res_al_ca_sp)
sp_al['treatment']='sp_al_ca'

all_co<-as.data.frame(res_al_co)
all_co['treatment']='all_al_co'
fall_co<-as.data.frame(res_al_co_fa)
fall_co['treatment']='fall_al_co'
sp_co<-as.data.frame(res_al_co_sp)
sp_co['treatment']='sp_al_co'

res_combo <- rbind(all,fall,sp)
res_combo_al <- rbind(all_al,fall_al,sp_al)
res_combo_co <- rbind(all_co,fall_co,sp_co)

keyvals.shape <-ifelse(
  res_combo$treatment == 'all_co_ca', 65, ifelse(
    res_combo$treatment == 'fall_co_ca', 70, ifelse(
      res_combo$treatment == 'sp_co_ca', 83, 16))
    )

keyvals.shape[is.na(keyvals.shape)] <- 16
names(keyvals.shape)[keyvals.shape == 16] <-'unsig'
names(keyvals.shape)[keyvals.shape == 65] <-'All seasons'
names(keyvals.shape)[keyvals.shape == 83] <-'Spring'
names(keyvals.shape)[keyvals.shape == 70] <-'Fall'
combined_co_ca <- EnhancedVolcano(res_combo,lab = rownames(res_combo),x = 'log2FoldChange',y = 'padj',
                                  xlim = c(-24, 23), ylim=c(0,13),
                                  title="Current vs Calcium", pCutoff = 0.1, FCcutoff = 1.5, pointSize = 4,
                col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = '', 
                shapeCustom = keyvals.shape, legendPosition = 'right', legendLabSize = 12, colAlpha = 1,
                legendLabels=c('Not significant', bquote(Log[2]~ 'fold change'),'Padj',bquote('Padj and' ~log[2]~ 'fold change'))) +
  labs(subtitle=NULL, caption = NULL) + ylab(bquote(-Log[10]~italic(Padj))) + 
  theme(axis.text.x = element_text(size=12),axis.text.y = element_text(size=12), axis.title = element_text(size=12),legend.position = "none")


keyvals.shape <-ifelse(
  res_combo_co$treatment == 'all_al_co', 65, ifelse(
    res_combo_co$treatment == 'fall_al_co', 70, ifelse(
      res_combo_co$treatment == 'sp_al_co', 83, 16))
)

keyvals.shape[is.na(keyvals.shape)] <- 16
names(keyvals.shape)[keyvals.shape == 16] <-'unsig'
names(keyvals.shape)[keyvals.shape == 65] <-'All seasons'
names(keyvals.shape)[keyvals.shape == 83] <-'Spring'
names(keyvals.shape)[keyvals.shape == 70] <-'Fall'
combined_al_co<-EnhancedVolcano(res_combo_co,lab = rownames(res_combo_co),x = 'log2FoldChange',y = 'padj', xlim = c(-24, 23), ylim=c(0,15),
                                title="Aluminum vs Current", pCutoff = 0.1, FCcutoff = 1.5, pointSize = 4,
                                col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = '', 
                                             shapeCustom = keyvals.shape, legendPosition = 'right', legendLabSize = 12, colAlpha = 1,
                                             legendLabels=c('Not significant', bquote(Log[2]~ 'fold change'),'Padj',bquote('Padj and' ~log[2]~ 'fold change'))) +
  labs(subtitle=NULL, caption = NULL)  + ylab(bquote(-Log[10]~italic(Padj))) + 
  theme(axis.text.x = element_text(size=12),axis.text.y = element_text(size=12), axis.title = element_text(size=12),legend.position = "none") 
keyvals.shape <-ifelse(
  res_combo_al$treatment == 'all_al_ca', 65, ifelse(
    res_combo_al$treatment == 'fall_al_ca', 70, ifelse(
      res_combo_al$treatment == 'sp_al_ca', 83, 16))
)
keyvals.shape[is.na(keyvals.shape)] <- 16
names(keyvals.shape)[keyvals.shape == 16] <-'unsig'
names(keyvals.shape)[keyvals.shape == 65] <-'All seasons'
names(keyvals.shape)[keyvals.shape == 83] <-'Spring'
names(keyvals.shape)[keyvals.shape == 70] <-'Fall'
combined_al_ca<-EnhancedVolcano(res_combo_al,lab = rownames(res_combo_al),x = 'log2FoldChange',y = 'padj', xlim = c(-24, 23), ylim=c(0,15),
                title="Aluminum vs Calcium", pCutoff = 0.1, FCcutoff = 1.5, pointSize = 4,
                col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = '', 
                shapeCustom = keyvals.shape, legendPosition = 'right', legendLabSize = 12, colAlpha = 1,
                legendLabels=c('Not significant', bquote(Log[2]~ 'fold change'),'Padj',bquote('Padj and' ~log[2]~ 'fold change'))) +
  labs(subtitle=NULL, caption = NULL) + ylab(bquote(-Log[10]~italic(Padj))) + 
  theme(axis.text.x = element_text(size=12),axis.text.y = element_text(size=12), axis.title = element_text(size=12),legend.position = "none") 


co_ca_all <- EnhancedVolcano(res_co_ca,lab = rownames(res_co_ca),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                                  title="Calcium vs Current", pCutoff = 0.1, FCcutoff = 1.5,
                                  col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'All Samples')

co_ca_fall <- EnhancedVolcano(res_co_ca_fa,lab = rownames(res_co_ca_fa),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                  title="Calcium vs Current", pCutoff = 0.1, FCcutoff = 1.5,
                col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'Fall Samples')
co_ca_sp <- EnhancedVolcano(res_co_ca_sp,lab = rownames(res_co_ca_sp),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                                  title="Calcium vs Current", pCutoff = 0.1, FCcutoff = 1.5,
                                  col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'Spring Samples')
al_ca_all <- EnhancedVolcano(res_al_ca,lab = rownames(res_al_ca),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                             title="Calcium vs Aluminum", pCutoff = 0.1, FCcutoff = 1.5,
                             col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'All Samples')

al_ca_fall <- EnhancedVolcano(res_al_ca_fa,lab = rownames(res_al_ca_fa),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                              title="Calcium vs Aluminum", pCutoff = 0.1, FCcutoff = 1.5,
                              col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'Fall Samples')
al_ca_sp <- EnhancedVolcano(res_al_ca_sp,lab = rownames(res_al_ca_sp),x = 'log2FoldChange',y = 'pvalue', xlim = c(-11, 11), ylim=c(0,23),
                            title="Calcium vs Aluminum", pCutoff = 0.1, FCcutoff = 1.5,
                            col=c('grey30', 'forestgreen','royalblue', 'red2'), selectLab = NA, subtitle = 'Spring Samples')


install.packages("gridExtra")
library(gridExtra)
library(grid)
library("ggplot2")

combo_plot<-grid.arrange(combined_co_ca, combined_al_co, combined_al_ca, ncol=1, widths=c(4.5))
ggsave("combined_volcanoes_vert_sm.jpg",combo_plot,width=4.5, height=9.5, dpi=300)
```
![PCA-All](DESeq2_All_Samples/combined_volcanoes_vert_sm.jpg)

Find the 1.5 up-regulated genes and the 1.5 down-regulated genes.

```
pos_resdata_al_ca <- subset(resdata_al_ca, log2FoldChange >= 1.5)
pos_resdata_al_ca_fa <- subset(resdata_al_ca_fa, log2FoldChange >= 1.5)
pos_resdata_al_ca_sp <- subset(resdata_al_ca_sp, log2FoldChange >= 1.5)
pos_resdata_al_co <- subset(resdata_al_co, log2FoldChange >= 1.5)
pos_resdata_al_co_fa <- subset(resdata_al_co_fa, log2FoldChange >= 1.5)
pos_resdata_al_co_sp <- subset(resdata_al_co_sp, log2FoldChange >= 1.5)
pos_resdata_co_ca <- subset(resdata_co_ca, log2FoldChange >= 1.5)
pos_resdata_co_ca_fa <- subset(resdata_co_ca_fa, log2FoldChange >= 1.5)
pos_resdata_co_ca_sp <- subset(resdata_co_ca_sp, log2FoldChange >= 1.5)

neg_resdata_al_ca <- subset(resdata_al_ca, log2FoldChange <= -1.5)
neg_resdata_al_ca_fa <- subset(resdata_al_ca_fa, log2FoldChange <= -1.5)
neg_resdata_al_ca_sp <- subset(resdata_al_ca_sp, log2FoldChange <= -1.5)
neg_resdata_al_co <- subset(resdata_al_co, log2FoldChange <= -1.5)
neg_resdata_al_co_fa <- subset(resdata_al_co_fa, log2FoldChange <= -1.5)
neg_resdata_al_co_sp <- subset(resdata_al_co_sp, log2FoldChange <= -1.5)
neg_resdata_co_ca <- subset(resdata_co_ca, log2FoldChange <= -1.5)
neg_resdata_co_ca_fa <- subset(resdata_co_ca_fa, log2FoldChange <= -1.5)
neg_resdata_co_ca_sp <- subset(resdata_co_ca_sp, log2FoldChange <= -1.5)
```
Generate the MA plots.

```
plotMA(dds, ylim=c(-8,8),main = "RNAseq experiment")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis2.png"))
dev.off()

plotMA(dds_fa, ylim=c(-8,8),main = "RNAseq experiment")
dev.copy(png, paste0(outputPrefix_fa, "-MAplot_initial_analysis2.png"))
dev.off()


plotMA(dds_sp, ylim=c(-8,8),main = "RNAseq experiment")
dev.copy(png, paste0(outputPrefix_sp, "-MAplot_initial_analysis2.png"))
dev.off()

rld <- rlogTransformation(dds, blind=T)
vsd <- varianceStabilizingTransformation(dds, blind=T)
rld_fa <- rlogTransformation(dds_fa, blind=T)
vsd_fa <- varianceStabilizingTransformation(dds_fa, blind=T)
rld_sp <- rlogTransformation(dds_sp, blind=T)
vsd_sp <- varianceStabilizingTransformation(dds_sp, blind=T)

write.table(as.data.frame(assay(rld)), file = paste0(outputPrefix, "-rlog-transformed-counts.txt"), sep = '\t')
write.table(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"), sep = '\t')
```
![PCA-All](DESeq2_All_Samples/all-MAplot_initial_analysis2.png)

Generate clustering plots

```
library("RColorBrewer")
library("gplots")
sampleDists <- dist(t(assay(rld)))
suppressMessages(library("RColorBrewer"))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colnames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colors <- colorRampPalette( rev(brewer.pal(8, "Blues")) )(255)
heatmap(sampleDistMatrix,col=colors,margin = c(8,8))
dev.copy(png,paste0(outputPrefix, "-clustering.png"))
dev.off()
```
![](DESeq2_All_Samples/all-clustering.png)

Generate PCA plots

```
library("genefilter")
library("ggplot2")
library("grDevices")

seasons<-c(rep(“Fall”,9),rep(“Spring”,8))

rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(vsd)[select,]))
scores <- data.frame(pc$x, type)
write.table(scores, file = paste0(outputPrefix, "-pca-scores.tsv"), sep = '\t')
(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, col = (factor(type))))
  + geom_point(aes(shape=seasons))
  + geom_point(size = 0)
  + ggtitle("Principal Components All")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c(.9,.2),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)
  ))

ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))

rv_fa <- rowVars(assay(rld_fa))
select_fa <- order(rv_fa, decreasing=T)[seq_len(min(500,length(rv_fa)))]
pc_fa <- prcomp(t(assay(vsd_fa)[select_fa,]))
scores_fa <- data.frame(pc_fa$x, type_fa)
write.table(scores_fa, file = paste0(outputPrefix_fa, "-pca-scores.tsv"), sep = '\t')


(pcaplot <- ggplot(scores_fa, aes(x = PC1, y = PC2, col = (factor(type_fa))))
  + geom_point(shape = 19) 
  + geom_point(size = 0)
  + ggtitle("Principal Components Fall")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c(.9,.2),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)
  ))

ggsave(pcaplot,file=paste0(outputPrefix_fa, "-ggplot2.png"))

rv_sp <- rowVars(assay(rld_sp))
select_sp <- order(rv_sp, decreasing=T)[seq_len(min(500,length(rv_sp)))]
pc_sp <- prcomp(t(assay(vsd_sp)[select_sp,]))
scores_sp <- data.frame(pc_sp$x, type_sp)
write.table(scores_sp, file = paste0(outputPrefix_sp, "-pca-scores.tsv"), sep = '\t')

(pcaplot <- ggplot(scores_sp, aes(x = PC1, y = PC2, col = (factor(type_sp))))
  + geom_point(shape = 17) 
  + geom_point(size = 0)
  + ggtitle("Principal Components Spring")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c(.9,.2),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)
  ))

ggsave(pcaplot,file=paste0(outputPrefix_sp, "-ggplot2.png"))
```
![](DESeq2_All_Samples/all-ggplot2.png)

Generate heatmap

```
library("RColorBrewer")
library("gplots")

select <- order(rowMeans(counts(ddsClean,normalized=T)),decreasing=T)[1:100]
my_palette <- colorRampPalette(c("blue",'white','red'))(n=100)
heatmap.2(assay(vsd)[select,], col=my_palette,
          scale="row", key=T, keysize=1, symkey=T,
          density.info="none", trace="none",
          cexCol=0.6, labRow=F,
          main="Heatmap of 100 DE Genes in Ca - Control Treatment Comparison")
dev.copy(png, paste0(outputPrefix, "-HEATMAP.png"))
dev.off()
```
![](DESeq2_All_Samples/all-HEATMAP.png)


## DE Genes

For the reference genome analysis with all samples,

Both Seasons

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5fold up-regulated | 0 | 4 | 1 | 
| 1.5-fold down-regulated | 6 | 0 | 4 |

Fall

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5-fold up-regulated | 9 | 10 | 11 | 
| 1.5-fold down-regulated | 12 | 10 | 34 |

Spring

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5-fold up-regulated | 9 | 11 | 5 | 
| 1.5-fold down-regulated | 22 |9 | 11 |

The differentially expressed genes had a fold change cut off of 1.5 and a padjusted value cut off of 0.1. 


### Re-Run DESeq2 without SpFgCa2 and SpFgCo1

Both Seasons

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5-fold up-regulated | 1 | 4 | 3 | 
| 1.5-fold down-regulated | 4 | 1 | 4 |

Fall

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5-fold up-regulated | 9 | 10 | 11 | 
| 1.5-fold down-regulated | 12 | 10 | 34 |

Spring

|  | Al vs. Co |  Ca vs. Co |  Al vs. Ca |
| ------ | ------ | ------ | ------ |
| 1.5-fold up-regulated | 18 | 10 | 12 | 
| 1.5-fold down-regulated | 18 | 14 | 12 |

The differentially expressed genes had a fold change cut off of 1.5 and a padjusted value cut off of 0.1. 

## EnTap and DE Genes combined

### All Samples
![](Annotated_DE_Genes/All_Samples.xlsx)

### Without SpFgCa1 and SpFgCo2
![](Annotated_DE_Genes/Without_SpFgCa1_SpFgCo2.xlsx)
