### QC of cDNA libraries
**Summary:** Thirty-six libraries (triplicates across three soil conditions, two species and two seasons) were generated using paired-end sequencing on Illumina HiSeq 2500 at ??? coverage. The reads were quality controlled usng fastQC and trimmed using sickle. Below are summaries of the fastQC reports before and after read trimming.

**Data availability:** 
- Quality controlled & trimmed read sets are available in .fastq format in the following directory: `/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed`.
- Singles are available in a subdirectory of the above location. 
- For readsets pre-QC and trimming: `/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_cat`



**scripts:**
- [fastqc.sh](../scripts/fastqc.sh)
- [sickle.sh](../scripts/sickle.sh)

**FastQC read summaries**

| metric                       | Fail | Pass | Warn |
| :--------------------------- | ---: | ---: | ---: |
| Adapter Content              | 0    | 66   | 0    |
| Basic Statistics             | 0    | 66   | 0    |
| Per Base N Content           | 0    | 66   | 0    |
| Per base sequence quality    | 0    | 66   | 0    |
| Per sequence quality scores  | 0    | 66   | 0    |
| Sequence length distribution | 0    | 0    | 66   |
| Overrepresented sequences    | 3    | 28   | 35   |
| Per sequence GC content      | 18   | 33   | 15   |
| Sequence Duplications levels | 53   | 0    | 13   |
| Kmer content                 | 64   | 0    | 2    |
| Per base sequence content    | 66   | 0    | 0    |

**Trimmed read summaries**

| metric | Fail | Pass | Warn |
| :----- | ---: | ---: | ---: |
| Adapter Content | 0 | 66 | 0 |
| Basic Statistics | 0 | 66 | 0 |
| Per Base N Content | 0 | 66 | 0 |
| Per base sequence quality | 0 | 66 | 0 |
| Per sequence quality scores | 0 | 66 | 0 |
| Sequence length distribution | 0 | 0 | 66 |
| Overrepresented sequences | 0 | 41 | 25 |
| Per sequence GC content | 0 | 55 | 11 |
| Sequence Duplications levels | 27 | 0 | 39 |
| Kmer content | 61 | 0 | 5 |
| Per base sequence content | 66 | 0 | 0 |

**Heatmaps:** Details of FastQC summaries with transcriptomes grouped by season and species. Both R1 and R2 paired libraries shown for each transcriptome..

![faas.png](images/faas.png)
![fafg.png](images/fafg.png)
![spas.png](images/spas.png)
![spfg.png](images/spfg.png)

---

May 28th, 2020 - Allyson Derry Joins Project!



