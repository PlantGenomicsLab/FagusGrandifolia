### Read Mapping to reference genome using HISAT2 (v 2.2.0)

A coordinate sorted .bam is needed for Trinity Genome-Guided _De-novo_ library assemblies. The first step in the process of constructing bam files is to align the trimmed reads (.fastq) to the reference genome. For this, we used the software HISAT2, a graph-based alignment program for next-gen sequencing reads. HISAT2 alignments rely on an indexed genome. We indexed our reference genome using the script [hisatBuild.sh](../scripts/hisatBuild.sh), also shown below.

```
#!/bin/bash
#SBATCH --job-name=hisat
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --cpus-per-task=20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=250G
#SBATCH -o hisat_%j.out
#SBATCH -e hisat_%j.err

module load hisat2/2.2.0

basedir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes"
genome="wegrzyn_assembly_v1_30Mar2020_chrRemoved.fasta"

# [call funct] [#cores] [path-to-genome] [prefix for constructed index]
hisat2-build -p 20 -f "$basedir/$genome"  fagr_allmaps

```

Note that the above script would include two additional flags `--ss` and `--exon` in the case that a genome annotation was available - these flags integrate splice-site information into the genome indices. Our genome was without an annotations, so we forwent these flags.

After the indices are constructed, we can run HISAT2's read alignment function. For this, we used the script [hisat.sh](../scripts/hisat.sh), also shown below.

```
#!/bin/bash
#SBATCH --job-name=hisatfg
#SBATCH -o hisat-%j.out
#SBATCH -e hisat-%j.err
#SBATCH --mail-user=alexander.trouern-trand@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=14
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=180G

module load hisat2/2.2.0

orgdir="/labs/Wegrzyn/AcerGenomes/acne"
trimmeddir="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed"
genomeidx="/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/genomes/fagr_allmaps"

SpFgCo1_1=$trimmeddir"/SpFgCo1_R1_trimmed.fastq"
SpFgCo1_2=$trimmeddir"/SpFgCo1_R2_trimmed.fastq"

# [call funct] [#cores] [output for transcriptome assemblers] [index prefix] [left reads] [right reads] [SAM output]
hisat2 -p 12 --dta -x $genomeidx -1 $SpFgCo1_1 -2 $SpFgCo1_2 -S SpFgCo1.sam

```


