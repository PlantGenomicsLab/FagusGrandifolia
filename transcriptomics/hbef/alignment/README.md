### Trinity Genome-Guided De novo Transcriptome Assembly for _Fagus grandifolia_
**Summary:** Based on the [Trinity Docs](https://github.com/trinityrnaseq/trinityrnaseq/wiki/Genome-Guided-Trinity-Transcriptome-Assembly), it was not clear that the genome-guided assembly feature would produce a higher quality assembly of the _F. grandifolia_ transcriptome libraries than the standard de novo approach. In the genome-guided de novo approach, the reads are first mapped to a reference genome to form clusters which are then assembled separately using the standard Trinity _De novo_ proocess. In my case, a highly contiguous hybrid read assembly (unreleased) was used as the reference for a Trinity (v. 2.8.5) genome-guided de novo assembly approach. By comparing these assemblies to others constructed using the standard Trinity non-guided approach (from an earlier version, v. 2.5.1) I observed a significant descrease in average transcript length, a doubling of monoexonic transcripts and a slight (insignificant?) decrease in the average percent identities and coverage of the alignments back to genomes for the genome-guided denovo assemblies.

```
GFACS STATS
```

### Averages across transcriptome features & alignment

** In the following figures, blue bars represent the genome-guided approach **

For each library the total number of transcripts aligning back to genome is ~ 15,000 greater in genome-guided
![Assemby size](images/assemblylens.png)

Most of these transcripts are short and monoexonic
![Assemby size](images/transcriptlens.png)
![transcript length](images/Len.spfgco1.png)
![transcript length](images/Len.fafgal1.png)

When aligning assembled transcriptomes back to genome (GMAP), percent coverage was sligbtly higher in genome-guided
![alignment coverage](images/coverage.png)
![GMAP alignment coverage](images/Cov.spfgco1.png)
![GMAP alignment coverage](images/Cov.fafgal1.png)

#### Percent Identity Distributions
![GMAP alignment % id](images/Pid.spfgco1.png)
![GMAP alignment % id](images/pid.fafgal1.png)

#### Exon Count Distributions
![exon count](images/Ex.spfgco1.png)
![exon count](images/Ex.fafgal1.png)


