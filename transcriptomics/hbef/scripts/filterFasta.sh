#!/bin/bash
#SBATCH --job-name=filterFasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filterFasta_%j.out
#SBATCH -e filterFasta_%j.err

hostname
echo "\nStart time:"
date

sed 's/\s.*$//g' ../transdecoder/trinity.headers.fasta.transdecoder.pep > renamed.pep
python createFasta.py --fasta renamed.pep --nameList filtered_cds_ids.txt --out fagr_transcriptome.filtered.faa

echo "\nEnd time:"
date

