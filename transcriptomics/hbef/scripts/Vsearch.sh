#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH --mail-user=allyson.derry@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general 

hostname
date

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
	--cluster_fast ../transdecoder/trinity.headers.fasta.transdecoder.cds \
	--id 0.90 \
	--centroids trinity.headers.fasta.centroids \
	--uc clusters.uc

date

