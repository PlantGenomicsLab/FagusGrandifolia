#!/bin/bash
#SBATCH --job-name=kallisto_counts
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=allyson.derry@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load kallisto/0.44.0

kallisto quant -i American_Beech_index \
	-o FaFgAl1 \
	-t 8 --pseudobam \
	-b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgAl2 \
        -t 8 --pseudobam \
        -b 100 \ 
        /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl2_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl2_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgAl3 \
        -b 100 \
        -t 8 --pseudobam \
        /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl3_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl3_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgCa1 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCa1_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCa1_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgCa2 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCa2_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCa2_R2_trimmed.fastq.gz 

kallisto quant -i American_Beech_index \
        -o FaFgCo1 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo1_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo1_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgCo2 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo2_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo2_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o FaFgCo3 \
        -t 8 --pseudobam \
        -b 100 \
	 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo3_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgCo2_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgAl1 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgAl1_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgAl1_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgAl2 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgAl2_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgAl2_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgCa2 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa2_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa2_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgCa3 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa3_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa3_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgCo1 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCo1_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed\SpFgCo1_R2_trimmed.fastq.gz

kallisto quant -i American_Beech_index \
        -o SpFgCo3 \
        -t 8 --pseudobam \
        -b 100 \
	/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCo3_R1_trimmed.fastq.gz /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCo3_R2_trimmed.fastq.gz
	
