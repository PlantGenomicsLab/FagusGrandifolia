#!/bin/bash
#SBATCH --job-name=busco_protein
#SBATCH --mail-user=allyson.derry@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 6
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general 

module load busco/4.0.2
module unload augustus/3.3.3
module unload blast/2.7.1
export PATH=/home/CAM/aderry/augustus/bin:/home/CAM/aderry/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config


busco -i ./../vsearch/fagr_transcriptome.filtered.faa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o busco_proteins -m prot -c 6


