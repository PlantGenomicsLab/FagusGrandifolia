#!/bin/bash
#SBATCH --job-name=TransDecoder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=allyson.derry.com
#SBATCH -o rename_%j.out
#SBATCH -e rename_%j.err

hostname
echo "\nStart time:"
date

cat *Fa*.bam/*.fasta *SpFgAl*.bam/*.fasta *SpFgCa2.bam/*.fasta *SpFgCa3.bam/*.fasta *SpFgCo1.bam/*.fasta *SpFgCo3.bam/*.fasta > tmp
awk '/^>/{print ">fagr_" sprintf("%06d", ++i); next}{ print }' < tmp > trinity.headers.fasta
rm tmp

echo "\nEnd time:"
date

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../Assembly/trinity_combine.fasta

hmmscan --cpu 16 \
       --domtblout pfam.domtblout \
       /isg/shared/databases/Pfam/Pfam-A.hmm \
       ../transdecoder/trinity.headers.fasta.transdecoder_dir/longest_orfs.pep \
 


TransDecoder.Predict -t ../trinity/trinity.headers.fasta  \
	--retain_pfam_hits ../transdecoder/pfam.domtblout \

