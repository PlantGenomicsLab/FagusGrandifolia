#!/bin/bash
#SBATCH --job-name=kallisto_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=allyson.derry@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load kallisto/0.44.0

kallisto index -i American_Beech_index ../vsearch/headers.fasta.transdecoder.cds


date 

