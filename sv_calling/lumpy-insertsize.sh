#!/bin/bash
#SBATCH --job-name=insertsize
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=65G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o insertsize_%j.out
#SBATCH -e insertsize_%j.err

hostname
echo "\nStart time:"
date

module load samtools/1.12

#for f in ../alignments/bwa_lumpy/cornell_asm_ArnotC*g_S?.bam
#do
#echo "${f}"
#samtools view "${f}" \
#    | tail -n+100000 \
#    | scripts/pairend_distro.py \
#    -r 101 \
#    -X 4 \
#    -N 10000 \
#    -o "${f%.*}.lib1.histo"
#done

#samtools view ../alignments/bwa_lumpy/UCONN.bam \
#    | tail -n+100000 \
#    | scripts/pairend_distro.py \
#    -r 101 \
#    -X 4 \
#    -N 10000 \
#    -o UCONN.lib1.histo

 samtools view ../alignments/bwa_lumpy/UCONNreads_UCONNasm.bam \
    | tail -n+100000 \
    | scripts/pairend_distro.py \
    -r 101 \
    -X 4 \
    -N 10000 \
    -o UCONNreads_UCONNasm.lib1.histo

#samtools view ../alignments/bwa_lumpy/cornell_miniasm_ArnotC8g_S4.bam \
#    | tail -n+100000 \
#    | scripts/pairend_distro.py \
#    -r 101 \
#    -X 4 \
#    -N 10000 \
#    -o cornell_miniasm_ArnotC8g_S4.lib1.histo

#samtools view ../alignments/bwa_lumpy/UCONNreads_UCONNminiasm.bam \
#    | tail -n+100000 \
#    | scripts/pairend_distro.py \
#    -r 101 \
#    -X 4 \
#    -N 10000 \
#    -o UCONNreads_UCONNminiasm.lib1.histo

#samtools view ../alignments/bwa_lumpy/cornell_asm_ArnotC8g_S4.bam \
#    | tail -n+100000 \
#    | scripts/pairend_distro.py \
#    -r 101 \
#    -X 4 \
#    -N 10000 \
#    -o cornell_asm_ArnotC8g_S4.lib1.histo

echo "\nEnd time:"
date

