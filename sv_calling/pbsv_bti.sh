#!/bin/bash
#SBATCH --job-name=pbsv
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o pbsv_%j.out
#SBATCH -e pbsv_%j.err

hostname
echo "\nStart time:"
date

#pbsv discover UConnReads_BTI_masurca_ch00.sort.bam UConnReads_BTI_masurca_ch00.svsig.gz

#pbsv call -j 36 /core/labs/Wegrzyn/FagusGenome/fagr/sv/Gao_uconn_reference/cornell_masurca_ch00.fasta UConnReads_BTI_masurca_ch00.svsig.gz UConnReads_BTI_masurca_ch00.var.vcf

#pbsv discover BTIReads_BTI_masurca.sort.bam BTIReads_BTI_masurca.svsig.gz
#pbsv call -j 36 /core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta BTIReads_BTI_masurca.svsig.gz BTIReads_BTI_masurca.var.vcf

pbsv discover BTIReads_BTI_miniasm.sort.bam BTIReads_BTI_miniasm.svsig.gz
pbsv call -j 36 /core/labs/Wegrzyn/FagusGenome/fagr/cornell_miniasm/assembly/Fgra_v0.1_masked.fasta BTIReads_BTI_miniasm.svsig.gz BTIReads_BTI_miniasm.var.vcf

echo "\nEnd time:"
date

