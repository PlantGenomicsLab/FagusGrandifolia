#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

hostname
echo "\nStart time:"
date

grep '^#CHROM\|\sPASS\s' BTIReads_BTI_masurca.var.vcf > BTIReads_BTI_masurcaPASS.var.vcf
grep '^#CHROM\|\sPASS\s' BTIReads_BTI_miniasm.var.vcf > BTIReads_BTI_miniasmPASS.var.vcf
grep '^#CHROM\|\sPASS\s' UConnReads_UCONN_masurca.var.vcf > UConnReads_UCONN_masurcaPASS.var.vcf
grep '^#CHROM\|\sPASS\s' UConnReads_UCONN_miniasm.var.vcf > UConnReads_UCONN_miniasmPASS.var.vcf

echo "\nEnd time:"
date

