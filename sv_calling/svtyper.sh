#!/bin/bash
#SBATCH --job-name=svtyper
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o svtyper_%j.out
#SBATCH -e svtyper_%j.err

hostname
echo "\nStart time:"
date

svtyper-sso \
    --core 25 \
    --batch_size 1000 \
    --max_reads 1000 \
    -i ../lumpy-sv/UCONNreads_UCONNminiasm.vcf \
    -B ../alignments/bwa_lumpy/UCONNreads_UCONNminiasm.sorted.bam \
    -l UCONNreads_UCONNminiasm.sorted.bam.json \
    > UCONNreads_UCONNminiasm.gt.vcf

echo "\nEnd time:"
date

