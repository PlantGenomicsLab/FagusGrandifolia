#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

hostname
echo "\nStart time:"
date

grep -v '0\/0' BTIreads_BTImasurca.gt.vcf > lumpyBTIreads_BTImasurca.gt.filtered.vcf
grep -v '0\/0' BTIreads_BTIminiasm.gt.vcf > lumpyBTIreads_BTIminiasm.gt.filtered.vcf
grep -v '0\/0' UCONNreads_UCONNasm.gt.vcf > lumpyUCONNreads_UCONNasm.gt.filtered.vcf
grep -v '0\/0' UCONNreads_UCONNminiasm.gt.vcf > lumpyUCONNreads_UCONNminiasm.gt.filtered.vcf

echo "\nEnd time:"
date

