#!/bin/bash
#SBATCH --job-name=lumpy
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o lumpy_%j.out
#SBATCH -e lumpy_%j.err

hostname
echo "\nStart time:"
date


export PATH=$PATH:/core/labs/Wegrzyn/FagusGenome/fagr/sv/lumpy-sv/bin

DIR=/core/labs/Wegrzyn/FagusGenome/fagr/sv/alignments/bwa_lumpy

lumpy \
    -mw 4 \
    -tt 0 \
    -pe id:UCONNreads_UCONNasm,bam_file:$DIR/UCONNreads_UCONNasm.discordants,histo_file:UCONNreads_UCONNasm.lib1.histo,mean:186.26416416416416,stdev:108.1746048396261,read_length:151,min_non_overlap:151,discordant_z:5,back_distance:10,weight:1,min_mapping_threshold:20 \
    -sr id:UCONNreads_UCONNasm,bam_file:$DIR/UCONNreads_UCONNasm.splitters,back_distance:10,weight:1,min_mapping_threshold:20 \
    > UCONNreads_UCONNasm_check.vcf

#lumpy \
#    -mw 4 \
#    -tt 0 \
#    -pe id:UCONNreads_UCONNminiasm,bam_file:$DIR/UCONNreads_UCONNminiasm.discordants,histo_file:UCONNreads_UCONNminiasm.lib1.histo,mean:169.060839932,stdev:110.530364237,read_length:151,min_non_overlap:151,discordant_z:5,back_distance:10,weight:1,min_mapping_threshold:20 \
#    -sr id:UCONNreads_UCONNminiasm,bam_file:$DIR/UCONNreads_UCONNminiasm.splitters,back_distance:10,weight:1,min_mapping_threshold:20 \
#    > UCONNreads_UCONNminiasm.vcf

echo "\nEnd time:"
date

