<h2>SV Tools and Filters</h2>

<h3>Lumpy</h3>
lumpy-insertsize.sh must be run first to generate the mean and stdev values for lumpy.sh<br>
Per publication, Lumpy had a high number of false positives for inversion and translocations - consider requiring consensus between lumpy and manta to include or possibly just using long read approaches for assembly error set.  See if this has improved with later development.<br>
Ran SVTyper to genotype Lumpy results<br>
No vcf filtering necessary https://github.com/arq5x/lumpy-sv/issues/108<br>
Filter out homozygous reference SVs (svtyper-filter.sh)<br>
grep -v '0\/0' BTIreads_BTImasurca.gt.vcf > lumpyBTIreads_BTImasurca.gt.filtered.vcf <br> 

<h3>Manta</h3>
There are a long list of non-PASS flags to filter on https://github.com/Illumina/manta/blob/master/docs/userGuide/README.md#vcf-filter-fields<br>
<ul>
<li>ID	-	Level -	Description</li>
<li>MinQUAL	Record	QUAL score is less than 20</li>
<li>MinGQ	Sample	GQ score is less than 15</li>
<li>MinSomaticScore	Record	SOMATICSCORE is less than 30</li>
<li>Ploidy	Record	For DEL & DUP variants, the genotypes of overlapping variants (with similar size) are inconsistent with diploid expectation</li>
<li>MaxDepth	Record	Depth is greater than 3x the median chromosome depth near one or both variant breakends</li>
<li>MaxMQ0Frac	Record	For a small variant (<1000 bases), the fraction of reads in all samples with MAPQ0 around either breakend exceeds 0.4</li>
<li>NoPairSupport	Record	For variants significantly larger than the paired read fragment size, no paired reads support the alternate allele in any sample</li>
<li>SampleFT	Record	No sample passes all the sample-level filters</li>
<li>HomRef	Sample	Homozygous reference call</li>
</ul>
manta-filter.sh<br>
grep '^#CHROM\|\sPASS\s' diploidSV.vcf > diploidSV.filtered.vcf

<h3>pbsv</h3>
assigned a non-reference genotype in at least one sample; a sample is assigned a non-reference genotype for a variant if at least --gt-min-reads [1] reads support the variant.<br>
<ul>
<li> NearReferenceGap: variant is near (< --filter-near-reference-gap [1K]) from a gap (run of >= 50 Ns in the reference assembly)</li>
<li>Decoy: variant involves a decoy sequence, where the chromosome name contains decoy, hs37d5, or hs38d1</li>
<li>NearContigEnd: variant is near (< --filter-near-contig-end [1K]) from a contig end</li>
<li>InsufficientStrandEvidence: variant is not supported by at least (--call-min-reads-per-strand-all-samples [1]) reads in forward and reverse </li>orientation
<li>NotFullySpanned: duplication variant does not have any fully spanning reads</li>
</ul>
pbsv-filter.sh<br>
grep '^#CHROM\|\sPASS\s' BTIReads_BTI_masurca.var.vcf > BTIReads_BTI_masurcaPASS.var.vcf


<h3>Sniffles</h3>
IMPRECISE/PRECISE	Indicates the confidence of the exact breakpoint positions (bp). - Decided to try keeping both.<br>
<ul>
<li>UNRESOLVED = insertion where the length could not be resolved</li>
<li>STRANDBIAS: If the sum of reads mapping to both strands is >5, a Fisher exact test is conducted to test for even distribution of reads to strands.</li>
</ul> 
FILTER is set to "STRANDBIAS" if p<0.01. This indicates possible problems with the quality of the variant, however be careful since the p-value is very strict and false positives are expected, for details see: #209<br>
Decided to not filter these.<br>
Nothing about gaps or ends in reference like pbsv<br>
Ran with --genotype flag so I can check homozygous variants here too.<br>
grep -v '0\/0' sniffles_BTIreads_BTImasurca_gt.vcf > sniffles_BTIreads_BTImasurca_gt.filtered.vcf

<h3>SVIM</h3>
Unlike other tools, output has no score based filtering. Settled on QUAL >= 10 (svim-qual-filter.sh)<br>
comments on overlapping SVs and whether or not to filter hom_ref (hard to say) here: https://github.com/eldariont/svim/issues/53<br>
FILTER: PASS, hom_ref, not_fully_covered<br>
svim-filter.sh<br>
grep '^#CHROM\|\sPASS\s' BTIreads_BTImasurca_variants_filtered10.vcf > BTIreads_BTImasurca_variants_filtered10PASS.vcf
