#!/bin/bash
#SBATCH --job-name=bcftools
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o bcftools_%j.out
#SBATCH -e bcftools_%j.err

hostname
echo "\nStart time:"
date

module load bcftools/1.12

bcftools view -i 'QUAL >= 10' variants.vcf > variants_filtered10.vcf

echo "\nEnd time:"
date

