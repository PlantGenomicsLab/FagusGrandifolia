#!/bin/bash
#SBATCH --job-name=lumpy
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o lumpy_%j.out
#SBATCH -e lumpy_%j.err

hostname
echo "\nStart time:"
date

export PATH=$PATH:/core/labs/Wegrzyn/FagusGenome/fagr/sv/lumpy-sv/bin

DIR=/core/labs/Wegrzyn/FagusGenome/fagr/sv/alignments/bwa_lumpy

lumpy -mw 4 -tt 0 \
    -pe id:cornell_asm_ArnotC8g_S4,bam_file:$DIR/cornell_asm_ArnotC8g_S4.discordants,histo_file:cornell_asm_ArnotC8g_S4.lib1.histo,mean:426.32902125952666,stdev:121.79755643457865,read_length:151,min_non_overlap:151,discordant_z:5,back_distance:10,weight:1,min_mapping_threshold:20 \
    -sr id:cornell_asm_ArnotC8g_S4,bam_file:$DIR/cornell_asm_ArnotC8g_S4.splitters,back_distance:10,weight:1,min_mapping_threshold:20 \
 > ArnotC8g_cornell_asm_check.vcf

#lumpy -mw 4 -tt 0 \
#    -pe id:cornell_miniasm_ArnotC8g_S4,bam_file:$DIR/cornell_miniasm_ArnotC8g_S4.discordants,histo_file:cornell_miniasm_ArnotC8g_S4.lib1.histo,mean:425.256441224,stdev:125.039862641,read_length:151,min_non_overlap:151,discordant_z:5,back_distance:10,weight:1,min_mapping_threshold:20 \
#    -sr id:cornell_miniasm_ArnotC8g_S4,bam_file:$DIR/cornell_miniasm_ArnotC8g_S4.splitters,back_distance:10,weight:1,min_mapping_threshold:20 \
# > ArnotC8g_cornell_miniasm.vcf

echo "\nEnd time:"
date

