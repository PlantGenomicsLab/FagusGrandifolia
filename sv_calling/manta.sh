#!/bin/bash
#SBATCH --job-name=manta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o manta_%j.out
#SBATCH -e manta_%j.err

hostname
echo "\nStart time:"
date


#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/manta-1.6.0.centos6_x86_64/bin/configManta.py --bam ../alignments/bwa_lumpy/cornell_asm_ArnotC8g_S4.sorted.bam --referenceFasta ../../cornell_masurca/assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta --runDir output/
#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/output/runWorkflow.py -j 25


#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/manta-1.6.0.centos6_x86_64/bin/configManta.py --bam ../alignments/bwa_lumpy/UCONNreads_UCONNasm.sorted.bam --referenceFasta ../../uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta --runDir UCONNreads_UCONNasm
#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/UCONNreads_UCONNasm/runWorkflow.py -j 25

#module load samtools/1.12

#samtools faidx ../../uconn_miniasm/assembly/fagr_uconn_miniasm_masked.fasta

#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/manta-1.6.0.centos6_x86_64/bin/configManta.py --bam ../alignments/bwa_lumpy/UCONNreads_UCONNminiasm.sorted.bam --referenceFasta ../../uconn_miniasm/assembly/fagr_uconn_miniasm_masked.fasta --runDir UCONNreads_UCONNminiasm
#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/UCONNreads_UCONNminiasm/runWorkflow.py -j 25

#samtools faidx ../../cornell_miniasm/assembly/Fgra_v0.1_masked.fasta
#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/manta-1.6.0.centos6_x86_64/bin/configManta.py --bam ../alignments/bwa_lumpy/cornell_miniasm_ArnotC8g_S4.sorted.bam --referenceFasta ../../cornell_miniasm/assembly/Fgra_v0.1_masked.fasta --runDir BTI_reads_BTIminiasm
#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/BTI_reads_BTIminiasm/runWorkflow.py -j 25

### min sv 50 bp for comparison

#/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/manta-1.6.0.centos6_x86_64/bin/configManta.py --bam ../alignments/bwa_lumpy/cornell_asm_ArnotC8g_S4.sorted.bam --referenceFasta ../../cornell_masurca/assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta --runDir minsize50/
/core/labs/Wegrzyn/FagusGenome/fagr/sv/manta/minsize50/runWorkflow.py -j 25


echo "\nEnd time:"
date

