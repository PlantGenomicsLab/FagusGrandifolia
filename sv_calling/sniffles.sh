#!/bin/bash
#SBATCH --job-name=sniffles
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o sniffles_%j.out
#SBATCH -e sniffles_%j.err

hostname
echo "\nStart time:"
date

export PATH=$PATH:/core/labs/Wegrzyn/FagusGenome/fagr/sv/sniffles/Sniffles-master/bin/sniffles-core-1.0.12

sniffles --genotype -m /core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/alignments/minimap/BTIReads_BTI_masurca.sort.bam -v sniffles_BTIreads_BTImasurca_gt.vcf

echo "\nEnd time:"
date

