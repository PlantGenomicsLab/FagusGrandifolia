#!/bin/bash
#SBATCH --job-name=pbsv
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o pbsv_%j.out
#SBATCH -e pbsv_%j.err

hostname
echo "\nStart time:"
date

#pbsv discover BTInanoReads_against_UConnAllMapsRenamed_chr00version.sort.bam BTInanoReads_against_UConnAllMapsRenamed_chr00version.svsig.gz

#pbsv call -j 36 /projects/EBP/Wegrzyn/fagr/sv/SV_calling/uconn_genome.fasta BTInanoReads_against_UConnAllMapsRenamed_chr00version.svsig.gz BTInanoReads_against_UConnAllMapsRenamed_chr00version.var.vcf

#pbsv discover UConnReads_UCONN_masurca.sort.bam UConnReads_UCONN_masurca.svsig.gz
#pbsv call -j 36 /core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta UConnReads_UCONN_masurca.svsig.gz UConnReads_UCONN_masurca.var.vcf

pbsv discover UConnReads_UCONN_miniasm.sort.bam UConnReads_UCONN_miniasm.svsig.gz
pbsv call -j 36 /core/labs/Wegrzyn/FagusGenome/fagr/uconn_miniasm/assembly/fagr_uconn_miniasm_masked.fasta UConnReads_UCONN_miniasm.svsig.gz UConnReads_UCONN_miniasm.var.vcf

echo "\nEnd time:"
date

