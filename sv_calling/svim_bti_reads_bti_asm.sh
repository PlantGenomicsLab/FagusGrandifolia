#!/bin/bash
#SBATCH --job-name=svim
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o svim_%j.out
#SBATCH -e svim_%j.err

hostname
echo "\nStart time:"
date

module load samtools/1.12

svim alignment BTIreads_BTImasurca ../../../../cornell_masurca/alignments/minimap/BTIReads_BTI_masurca.sort.bam ../../../../cornell_masurca/assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta

echo "\nEnd time:"
date

