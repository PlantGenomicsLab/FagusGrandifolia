#!/bin/bash
#SBATCH --job-name=repeat_modeler_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o repeat_modeler_%j.out
#SBATCH -e repeat_modeler_%j.err

echo `hostname`

module load RepeatModeler/1.0.8
module unload perl/5.30.1 
BuildDatabase -name CornellABeech /labs/Wegrzyn/FagusGenome/cornell/Fgra_v0.1.fasta


