#!/bin/bash
#SBATCH --job-name=repeatmask
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattari@uconn.edu
#SBATCH --mem=100G
#SBATCH -o repeatmaskrun_%j.out
#SBATCH -e repeatmaskrun_%j.err

echo `hostname`
module load RepeatMasker
module unload perl/5.30.1

RepeatMasker -pa 16 -lib RM_27421.TueApr72043412020/consensi.fa.classified -xsmall /labs/Wegrzyn/FagusGenome/cornell/Fgra_v0.1.fasta -dir repeatmask


