# RNA-seq samples 

samples included those from the Hubbard Brook Experimental Forest and the ForestGEO project

# Repeat Modeling/Masking 

The genomes were masked using RepeatModeler and RepeatMasker
The scripts can be found in cornell_genome_annotation/repeatM

# Aligning RNA

All the available RNA samples were mapped back to the masked genome using HISAT2. There were a few samples missing, especially for H81 from the ForestGEO project. Overall there were 214 samples that were mapped back to the genome using HISAT2.

Alignment rates ranged from 75-85% for most samples.

# Aligning proteins 

Protein evidence from assembled transcriptomes translated using Transdecoder are being aligned using GenomeThreader. 
