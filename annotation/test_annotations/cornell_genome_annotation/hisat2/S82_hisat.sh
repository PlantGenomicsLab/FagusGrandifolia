#!/bin/bash
#SBATCH --job-name=hisat2_S82
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in S82Fg02 S82Fg03 S82Fg04 S82Fg05 S82Fg06 S82Fg07 S82Fg08 S82Fg10 S82Fg18 S82Fg20 S82Fg23 S82Fg25 S82Fg27 S82Fg28 S82Fg29 S82Fg36 S82Fg39 S82Fg42 S82Fg42 S82Fg45 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 


