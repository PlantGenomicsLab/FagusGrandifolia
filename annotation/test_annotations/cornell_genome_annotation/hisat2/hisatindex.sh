#!/bin/bash
#SBATCH --job-name=indexbuild
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o indexbuild_%j.out
#SBATCH -e indexbuild_%j.err

module load hisat2/2.1.0
hisat2-build -p 8 /labs/Wegrzyn/FagusGenome/fagr/annotation/cornell_annotation/repeatM/repeatmask/Fgra_v0.1.fasta.masked Fagr_masked
