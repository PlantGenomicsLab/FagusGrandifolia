#!/bin/bash
#SBATCH --job-name=hisat2_H81
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o hisat2_%j.out
#SBATCH -e hisat2_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H81Fg01 H81Fg02 H81Fg07 H81Fg08 H81Fg09 H81Fg14 H81Fg24 H81Fg25 H81Fg28 H81Fg29 H81Fg30 H81Fg32 H81Fg36 H81Fg37 H81Fg38 H81Fg40 H81Fg42 H81Fg43 H81Fg45 H81Fg46
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

