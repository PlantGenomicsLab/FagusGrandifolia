#!/bin/bash
#SBATCH --job-name=hisat2_H72
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H72Fg01 H72Fg02 H72Fg07 H72Fg08 H72Fg09 H72Fg14 H72Fg24 H72Fg25 H72Fg28 H72Fg29 H72Fg30 H72Fg32 H72Fg36 H72Fg37 H72Fg38 H72Fg40 H72Fg42 H72Fg43 H72Fg45 H72Fg46 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

