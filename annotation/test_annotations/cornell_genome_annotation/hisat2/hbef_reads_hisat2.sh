#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o hisat2_%j.out
#SBATCH -e hisat2_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in FaFgAl3 FaFgCa1 FaFgCa2 FaFgCo1 FaFgCo2 FaFgCo3 SpFgAl1 SpFgAl2 SpFgAl3 SpFgCa1 SpFgCa2 SpFgCa3 SpFgCo1 SpFgCo2 SpFgCo3
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/fastq_trimmed/fastq_trimmed/${i}_R1_trimmed.fastq" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/fastq_trimmed/fastq_trimmed/${i}_R2_trimmed.fastq" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

