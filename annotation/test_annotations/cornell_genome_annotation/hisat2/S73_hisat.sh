#!/bin/bash
#SBATCH --job-name=hisat2_S73
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in S73Fg02 S73Fg03 S73Fg04 S73Fg05 S73Fg06 S73Fg07 S73Fg08 S73Fg10 S73Fg18 S73Fg20 S73Fg23 S73Fg25 S73Fg27 S73Fg28 S73Fg29 S73Fg36 S73Fg39 S73Fg42 S73Fg42 S73Fg45 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 


