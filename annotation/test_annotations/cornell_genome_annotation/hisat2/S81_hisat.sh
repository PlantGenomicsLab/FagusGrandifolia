#!/bin/bash
#SBATCH --job-name=hisat2_S81
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in S81Fg02 S81Fg03 S81Fg04 S81Fg05 S81Fg06 S81Fg07 S81Fg08 S81Fg10 S81Fg18 S81Fg20 S81Fg23 S81Fg25 S81Fg27 S81Fg28 S81Fg29 S81Fg36 S81Fg39 S81Fg42 S81Fg42 S81Fg45 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 


