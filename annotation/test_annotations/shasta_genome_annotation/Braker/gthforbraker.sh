#!/bin/bash
#SBATCH --job-name=genome_threader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=20G
#SBATCH -o genome_threader1_%j.out
#SBATCH -e genome_threader1_%j.err

module load genomethreader/1.7.1

gth -genomic /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked  -protein /labs/Wegrzyn/FagusGenome/fagr/annotation/SpFgCa1/Trinity.fasta.transdecoder.pep -gff3out -skipalignmentout -o gth.aln

