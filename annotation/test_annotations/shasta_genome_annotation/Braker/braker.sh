#!/bin/bash
#SBATCH --job-name=BRAKERsecond
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=40G
#SBATCH -o Brakersecond_%j.out
#SBATCH -e Brakersecond_%j.err

echo `hostname`
module load BRAKER/2.0.5
module unload singularity/3.1.1
module unload augustus/3.3.3
module load augustus/3.2.3
module load bamtools/2.4.1

export TMPDIR=/labs/Wegrzyn/FagusGenome/fagr/annotation/braker2_temp
export AUGUSTUS_CONFIG_PATH=/home/FCAM/abhattarai/3.2.3/config
export AUGUSTUS_BIN_PATH=/home/FCAM/abhattarai/3.2.3/bin
export AUGUSTUS_SCRIPTS_PATH=/home/FCAM/abhattarai/3.2.3/scripts
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin/
export GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap
module unload perl/5.28.1
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/
cp ~/local_gm_key_64 ~/.gm_key

org="/labs/Wegrzyn/FagusGenome/fagr/annotation"
bam="$org/sorted_bam2/merged_samples1.bam"
genome="$org/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked"
protein="$org/gth.aln"

braker.pl --cores=16 --genome="$genome" --species=AmericanBeech8 --bam="$bam" --prot_aln="$protein" --prg=gth --gth2traingenes --softmasking 1 --gff3

