#!/bin/bash
#SBATCH --job-name=samtools_merge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=10G
#SBATCH -o samtools_merge%j.out
#SBATCH -e samtools_merge%j.err

echo `hostname`
module load samtools

samtools merge merged_samples1.bam sorted_FaAgCa3.bam sorted_FaFgAl1.bam sorted_FaFgAl2.bam sorted_FaFgAl3.bam sorted_FaFgCa1.bam sorted_FaFgCa2.bam sorted_FaFgCo1.bam sorted_FaFgCo2.bam sorted_FaFgCo3.bam sorted_SpFgAl1.bam sorted_SpFgAl2.bam sorted_SpFgAl3.bam sorted_SpFgCa1.bam sorted_SpFgCa2.bam sorted_SpFgCa3.bam sorted_SpFgCo1.bam sorted_SpFgCo2.bam sorted_SpFgCo3.bam

