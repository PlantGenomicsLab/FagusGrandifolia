#!/bin/bash
#SBATCH --job-name=quickcopy
#SBATCH -c 1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=END
#SBATCH --mem=10
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o qcopy-%j.out
#SBATCH -e qcopy-%j.err

for i in acya amtr arth bepe eugr fasy gibi juhi jure nyco orsa pive potr prpe qulo quro qusu tegr vivi
do
cp "/labs/Wegrzyn/AcerGenomes/comparative/data/${i}/filtered.fasta" /labs/Wegrzyn/FagusGenome/fagr/annotation/orthofinder/species2
mv species2/filtered.fasta species2/"${i}_filtered.fasta"
done

