#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -c 20
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o orthofinder-%j.out
#SBATCH -e orthofinder-%j.err

module load OrthoFinder/2.3.3
module load muscle
module load DLCpar/1.0
module load FastME
module load diamond/0.9.25
module load mcl
module load anaconda/4.4.0

orthofinder -f /labs/Wegrzyn/FagusGenome/fagr/annotation/orthofinder/species2 -S diamond -t 20

