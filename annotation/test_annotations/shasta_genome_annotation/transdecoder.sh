#!/bin/bash
#SBATCH --job-name=transdecoder1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o transdecoder1_%j.out
#SBATCH -e transdecoder1_%j.err

echo `hostname`

module load TransDecoder/5.3.0

for i in FaFgCo1 FaFgCo2 FaFgCo3 SpFgAl1 SpFgAl2 SpFgAl3 SpFgCa1 SpFgCa2 SpFgCa3 SpFgCo1 SpFgCo2 SpFgCo3
do

mkdir "${i}"
cd "${i}"
TransDecoder.LongOrfs -t "/labs/Wegrzyn/HBEF/hbef_transcriptomes/data/assembly/fagr/trinity_out_${i}/Trinity.fasta"
TransDecoder.Predict -t "/labs/Wegrzyn/HBEF/hbef_transcriptomes/data/assembly/fagr/trinity_out_${i}/Trinity.fasta"

cd /labs/Wegrzyn/FagusGenome/fagr/annotation/
done 


