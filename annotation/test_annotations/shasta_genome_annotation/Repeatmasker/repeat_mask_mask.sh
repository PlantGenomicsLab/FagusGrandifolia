#!/bin/bash
#SBATCH --job-name=repeatmaskrunhimem
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattari@uconn.edu
#SBATCH --mem=120G
#SBATCH -o repeatmaskrun_%j.out
#SBATCH -e repeatmaskrun_%j.err

echo `hostname`
module load RepeatModeler/1.0.8
module unload perl/5.28.1
RepeatMasker -pa 12 -lib consensi.fa.classified -xsmall /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_polished_shasta_assembly_default_minlen_3kb.fasta
