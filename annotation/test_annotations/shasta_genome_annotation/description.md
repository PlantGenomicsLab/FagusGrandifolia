## Transdecoder

Transdecoder was used on the transcriptomes assembled in the HBEF folder. The samples include:

* FaFgAl1
* FaFgAl2
* FaFgAl3
* FaFgCa1
* FaFgCa2
* FaFgCa3
* FaFgCo1
* FaFgCo2
* FaFgCo3
* SpFgAl1
* SpFgAl2
* SpFgCa1
* SpFgCa2
* SpFgCa3
* SpFgCo1
* SpFgCo2
* SpFgCo3

The script for transdecoder can be found in the HBEF/Annotation folder under transdecoder.sh

## Masking the Genome

The assembled genome using Shasta was masked using RepeatModeler/RepeatMasker. The genome was softmasked. 

    The scripts can be found at HBEF/Repeatmasker

## Aliging Transcripts

The transcripts were then aligned to the masked genome using HISAT2.
    
    The scripts can be found at HBEF/HISAT2
    
**The aligment rates are as follows:**

| Sample | Alignment Rate |
| ------ | ------ |
| FaFgAl1 | 95.09% |
| FaFgAl2 | 94.83% |
| FaFgAl3 | 95.14% |
| FaFgCa1 | 94.52% |
| FaFgCa2 | 95.46% |
| FaFgCa3 | 93.79% |
| FaFgCo1 | 94.76% |
| FaFgCo2 | 92.34% |
| FaFgCo3 | 93.80% |
| SpFgAl1 | 93.81% |
| SpFgAl2 | 92.72% |
| SpFgCa1 | 44.67% |
| SpFgCa2 | 92.38% |
| SpFgCa3 | 95.11% |
| SpFgCo1 | 91.99% |
| SpFgCo2 | 90.73% |
| SpFgCo3 | 93.49% |


After aligning, the bam files were merged using samtools merge, and the merged bam file can be found at

    /labs/Wegrzyn/FagusGenome/fagr/annotation/sorted_bam/merged_samples1.bam


## GenomeThreader 

The pep files from the transdecoder output were used for alignment with GenomeThreader. For the first annotation, only one sample was used (SpFgCa1) as protein evidence. The sample was aligned to the genome using GenomeThreader. 

    The file can be found at HBEF/Annotation/Braker/SpFgCa1_Trinity.fasta.transdecoder.pep
    The script can be found at HBEF/Annotation/Braker/gthforbraker.sh

For the improved annotation, all pep files were merged and used as protein evidence. 

    The script can be found at HBEF/Annotation/Braker/gth_pep_total.sh
    
## Braker 

Genes were predicted using Braker 2.0.5 using RNA-seq and protein evidence 

    The script can be found at HBEF/Annotation/Braker/braker.sh
    
## gFACs 

Structural annotation and filtering was performed using gFACs. Multiple steps were involved to filter out multi exonic and mono exonic genes. The gFACs_procedure.md describes the order of the scripts as well as intermediate steps.

    The scripts can be found at HBEF/Annotation/gfacs
    
## EnTAP

Functional annotation and filtering was performed using EnTAP. 

    The script can be found at HBEF/Annotation/entap

outputs from EnTAP are found at:

    /labs/Wegrzyn/FagusGenome/fagr/annotation/entap




