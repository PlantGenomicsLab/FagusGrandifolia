The gFACs procedure is based on a script by Susan McEvoy

First part:

The first script (gfacs_pt1.sh) the gene output from braker into two groups, mono-exonic and multi-exonic. 

The options used to isolate the mono-exonic genes are as follows:

*  splice-table
*  unique-genes-only
*  rem-multiexonics
*  rem-all-incompletes
*  rem-genes-without-start-codon
*  rem-genes-without-stop-codon
*  get-protein-fasta

The output folder is mono_o. 

The options used to isolate the multi-exonic genes are as follows:

*  splice-table
*  unique-genes-only
*  rem-monoexonics
*  min-exon-size 6

The output folder is multi_o

Analysis was then continued in mono_o:

* made a directory called interproscan
* Ran the script interproscan.sh
* ran the following commands in an interactive session:

```
sed 's/\s.*$//' interproscan/genes_without_introns.fasta.faa.tsv | uniq > pfam_ids.txt
    # this extracts the unique gene ids that were supported by protein evidence (given in the interproscan output)
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList pfam_ids.txt --idPath . --out pfam_gene_table.txt
    # this uses the python script filtergFACsGeneTable.py to filter the mono-exonic gFACs gene table using the gene ids
```

Analysis in multi_o:

* ran the following commands in an interactive session to remove genes that were both 5' incomplete and 3' incomplete

```
grep '5_INC+3_INC' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > fullinc_ids.txt
    # This line isolates the gene ids for incomplete genes (both 5' and 3' incomplete)
grep 'gene' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > gene_table_ids.txt
    # This line extracts all the gene ids in the multiexonic table
grep -vxFf fullinc_ids.txt gene_table_ids.txt > comp_inc_ids.txt
    # This line takes the full gene id and removes the incomplete gene ids and then outputs a file with the gene ids of complete genes. 
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList comp_inc_ids.txt --idPath . --out comp_inc_gene_table.txt
    # This line uses the python script to filter the incomplete genes out of the multi-exonic gFACs gene table 
```
* Isolating genes without start/stop codons, and removing genes that are missing both 
    *  Ran the script gfacs_pt2.sh
    *  Ran the following commands:

```
grep 'gene' start_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' | uniq > starts_ids.txt
grep 'gene' stop_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' |  uniq > stops_ids.txt
grep -vxFf starts_ids.txt comp_inc_ids.txt > missing_starts_ids.txt
grep -vxFf stops_ids.txt comp_inc_ids.txt > missing_stops_ids.txt
grep -xFf missing_starts_ids.txt missing_stops_ids.txt > missing_both_ids.txt
grep -vxFf missing_both_ids.txt comp_inc_ids.txt > completes_partials_ids.txt
python ../filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt
cat mono_o/pfam_gene_table.txt  multi_o/completes_partials_gene_table.txt > mono_multi_gene_table.tx
```
* the completes_partials_ids.txt includes the genes that are missing a start or a stop but not both 
* the python script filters the gFACs gene table to remove the genes missing both start and stop codons 
* the filtered gFACs table from mono exonics was joined with the filtered gFACs table from multi exonics 
* the final script gfacs_pt3.sh outputs the gff3 and gtf formats as well as a statistics table



