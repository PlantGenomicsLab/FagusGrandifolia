#!/bin/bash
#SBATCH --job-name=gfacsfinal
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o gfacs_final-%j.o
#SBATCH -e gfacs_final-%j.e

module load perl
org="/labs/Wegrzyn/FagusGenome/fagr/annotation"
genome="$org/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked"
alignment="mono_multi_gene_table.txt"
script="/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--splice-table \
--get-protein-fasta \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O final_o \
"$alignment"


