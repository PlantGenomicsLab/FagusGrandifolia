gFACs LOG

Version: 1.1.1 (11/12/2019)
Time of run: Tue Jan  7 21:57:25 2020

Command:/labs/Wegrzyn/gFACs/gFACs.pl -f gFACs_gene_table --no-processing --statistics --splice-table --get-protein-fasta --create-gff3 --create-gtf --fasta /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked -O final_o mono_multi_gene_table.txt

You have specfied a format:	GFACS all version in gene_table.txt format

You have specified an output directory: final_o

Format command:	perl /labs/Wegrzyn/gFACs/format_scripts/gFACs_gene_table.pl mono_multi_gene_table.txt final_o  


Flags:


Format Check!
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_table_fixer.pl final_o 

Incompleteness Check! Genes will be labeled as 5' or 3' incompletes.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_editor.pl final_o 

You have specified a fasta! Indexing it...
Creating index file from /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked

--splice-table has been activated. Creating splice_table.txt catalog.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/splice_table.pl /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked final_o  >> final_o//gFACs_log.txt
Results:
	gc_ag	1351	1.011%
	gt_ag	132176	98.952%
	at_ac	49	0.037%

--get-protein-fasta has been activated. Protein fasta will be created called genes_without_introns.fasta.faa.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/get_protein_fasta.pl /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked final_o 

--create-gtf has been activated. A gtf file called out.gtf will be created.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/add_start_stop_to_gene_table.pl /labs/Wegrzyn/FagusGenome/fagr/annotation/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked final_o 
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gtf_creator.pl final_o//start_and_stop_gene_table.txt final_o mono_multi_gene_table.txt 

--create-gff3 has been activated. A gff3 file called out.gff3 will be created.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gff3_creator.pl final_o//gene_table.txt final_o final_o//gene_table.txt  

--statistics has been activated. Statistics will be printed to statistics.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/classic_stats.pl final_o//gene_table.txt > final_o//statistics.txt

Completed! Have a great day!

