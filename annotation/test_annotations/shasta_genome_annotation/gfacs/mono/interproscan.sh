#!/bin/bash
#SBATCH --job-name=interproscan_pfam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o interproscan_pfam_%j.out
#SBATCH -e interproscan_pfam_%j.err

module load interproscan

sed 's/*$//g' ../genes_without_introns.fasta.faa > genes_without_introns.fasta.faa

interproscan.sh -appl Pfam -i genes_without_introns.fasta.faa
