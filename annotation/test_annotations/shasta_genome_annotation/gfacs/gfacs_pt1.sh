#!/bin/bash
#SBATCH --job-name=gfacspart1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o gfacsp1-%j.o
#SBATCH -e gfacsp1-%j.e

module load perl

org="/labs/Wegrzyn/FagusGenome/fagr/annotation"
genome="$org/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked"
alignment="$org/braker/AmericanBeech8/augustus.hints.gff3"
script="/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-multiexonics \
--rem-all-incompletes \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--get-protein-fasta \
--fasta "$genome" \
-O mono_o \
"$alignment"

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-monoexonics \
--min-exon-size 6 \
--fasta "$genome" \
-O multi_o \
"$alignment"
