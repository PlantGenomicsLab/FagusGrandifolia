#!/bin/bash
#SBATCH --job-name=filtering
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=20G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o gfacs_filtering-%j.o
#SBATCH -e gfacs_filtering-%j.e

python ../filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt

