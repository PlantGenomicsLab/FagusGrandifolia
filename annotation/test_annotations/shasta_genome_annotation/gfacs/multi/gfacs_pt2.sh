#!/bin/bash
#SBATCH --job-name=gfacspart2
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o gfacspt2-%j.o
#SBATCH -e gfacspt2-%j.e

module load perl

org="/labs/Wegrzyn/FagusGenome/fagr/annotation"
genome="$org/fagr_polished_shasta_assembly_default_minlen_3kb.fasta.masked"
alignment="comp_inc_gene_table.txt"
script="/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-start-codon \
--fasta "$genome" \
-O start_o \
"$alignment"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-stop-codon \
--fasta "$genome" \
-O stop_o \
"$alignment"

