#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -c 16
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=200G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o entap-%j.o
#SBATCH -e entap-%j.e

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

/labs/Wegrzyn/EnTAP/EnTAP --runP \
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
-d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.92.dmnd \
-d /labs/Wegrzyn/Transcriptomics/Araport11/arabidopsis_at11.dmnd \
-i /labs/Wegrzyn/FagusGenome/fagr/annotation/gfacs_test1/final_o/genes_without_introns.fasta.faa \
--out-dir /labs/Wegrzyn/FagusGenome/fagr/annotation/entap \
-c bacteria -c archaea -c opisthokonta -c rhizaria -c fungi --threads 16

