#!/bin/bash
#SBATCH --job-name=repeat_modeler_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o repeat_modeler_%j.out
#SBATCH -e repeat_modeler_%j.err

echo `hostname`

module load RepeatModeler/1.0.8
module unload perl/5.30.1 
BuildDatabase -name MascuraABeech /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_rmv_contam_input/seqs/final.purged.fa


