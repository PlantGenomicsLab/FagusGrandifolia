#!/bin/bash
#SBATCH --job-name=repeat_modeler_general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=150G
#SBATCH -o repeat_modeler_general_%j.out
#SBATCH -e repeat_modeler_general_%j.err

echo `hostname`
module load RepeatModeler
module unload perl/5.28.1

RepeatModeler -pa 30 -database MascuraABeech


