#!/bin/bash
#SBATCH --job-name=repeatmask
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattari@uconn.edu
#SBATCH --mem=50G
#SBATCH -o repeatmaskrun_%j.out
#SBATCH -e repeatmaskrun_%j.err

echo `hostname`
module load RepeatMasker
module unload perl/5.30.1

RepeatMasker -pa 16 -lib RM_50943.ThuApr21231172020/consensi.fa.classified -xsmall /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_rmv_contam_input/seqs/final.purged.fa -dir repeatM


