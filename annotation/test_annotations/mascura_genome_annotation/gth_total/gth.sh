#!/bin/bash
#SBATCH --job-name=genome_threader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o genome_threader1_%j.out
#SBATCH -e genome_threader1_%j.err

module load genomethreader/1.7.1

gth -genomic /labs/Wegrzyn/FagusGenome/fagr/annotation/mascura_annotation/repeatM/final.purged.fa.masked -protein /labs/Wegrzyn/FagusGenome/fagr/annotation/transdecoder_output/pep_files/total_pep_files.fasta -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o gth.aln -force -gcmaxgapwidth 1000000


