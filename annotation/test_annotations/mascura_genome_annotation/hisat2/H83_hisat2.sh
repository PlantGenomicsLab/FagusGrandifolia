#!/bin/bash
#SBATCH --job-name=hisat2_H83
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H83Fg01 H83Fg02 H83Fg07 H83Fg08 H83Fg09 H83Fg14 H83Fg24 H83Fg25 H83Fg28 H83Fg29 H83Fg30 H83Fg32 H83Fg36 H83Fg37 H83Fg38 H83Fg40 H83Fg42 H83Fg43 H83Fg45 H83Fg46
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

