#!/bin/bash
#SBATCH --job-name=hisat2_H71
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H71Fg01 H71Fg02 H71Fg07 H71Fg08 H71Fg09 H71Fg14 H71Fg24 H71Fg25 H71Fg28 H71Fg29 H71Fg30 H71Fg32 H71Fg36 H71Fg37 H71Fg38 H71Fg40 H71Fg42 H71Fg43 H71Fg45 H71Fg46
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

