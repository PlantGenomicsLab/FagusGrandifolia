#!/bin/bash
#SBATCH --job-name=hisat2_H73
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H73Fg01 H73Fg02 H73Fg07 H73Fg08 H73Fg09 H73Fg14 H73Fg24 H73Fg25 H73Fg28 H73Fg29 H73Fg30 H73Fg32 H73Fg36 H73Fg37 H73Fg38 H73Fg40 H73Fg42 H73Fg43 H73Fg45 H73Fg46
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

