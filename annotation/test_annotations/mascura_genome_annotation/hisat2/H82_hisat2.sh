#!/bin/bash
#SBATCH --job-name=hisat2_H82
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in H82Fg01 H82Fg02 H82Fg07 H82Fg08 H82Fg09 H82Fg14 H82Fg24 H82Fg25 H82Fg28 H82Fg29 H82Fg30 H82Fg32 H82Fg36 H82Fg37 H82Fg38 H82Fg40 H82Fg42 H82Fg43 H82Fg45 H82Fg46 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

