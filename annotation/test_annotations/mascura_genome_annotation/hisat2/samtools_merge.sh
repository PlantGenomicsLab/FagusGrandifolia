#!/bin/bash
#SBATCH --job-name=samtools_merge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o samtools_merge%j.out
#SBATCH -e samtools_merge%j.err

echo `hostname`
module load samtools
var=$(find *.bam)

samtools merge merged_samples_mascura.bam $var

