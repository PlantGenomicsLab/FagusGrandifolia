#!/bin/bash
#SBATCH --job-name=hisat2_S71
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in S71Fg23 S71Fg25 S71Fg27 S71Fg28 S71Fg29 S71Fg36 S71Fg39 S71Fg42 S71Fg45 
do 
hisat2 -x Fagr_masked -1 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_1P.fq.gz" -2 "/labs/Wegrzyn/FagusGenome/fagr/annotation/trimmomatic/${i}_2P.fq.gz" -p 16 -S "${i}.sam" --dta
samtools view -@ 16 -uhS "${i}.sam" | samtools sort -@ 16 -o "sorted_${i}.bam"
done 

