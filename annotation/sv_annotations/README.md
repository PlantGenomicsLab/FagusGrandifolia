# Genome Annotations (SV versions)  

$PWD = /core/labs/Wegrzyn/FagusGenome/fagr    

## Starting Assemblies  

**CornellMasurca**  $PWD/cornell_masurca/assembly/allmaps/bti_masurca_purgehap_allmaps_2x.fasta  

**CornellMiniasm**  $PWD/cornell_miniasm/assembly/allmaps/Fgra_v0.1.fasta  

**UConnMasurca** $PWD/uconn_masurca/assembly/allmaps/uconn_masurca_purgehap_allmaps_2x.fasta  

**UConnMiniasm**  $PWD/uconn_miniasm/assembly/allmaps/merged_file_pre_ALLMAPS_Round2_UConn_Miniasm_chrRemoved_Final.fasta

## Soft-masking  

**CornellMasurca**  
Repeat library: $PWD/cornell_masurca/assembly/repeats/RM_2869953.TueSep210907192021/  
Soft-masked: $PWD/cornell_masurca/assembly/repeats/outputfiles/bti_masurca_purgehap_allmaps_2x.fasta.masked 

**CornellMiniasm**  
Repeat library: $PWD/cornell_miniasm/assembly/repeats2/RM_4102500.SatSep250736232021/  
Soft-masked: $PWD/cornell_miniasm/assembly/repeats2/outputfiles/Fgra_v0.1.masked

**UConnMasurca**	 
Repeat library: $PWD/uconn_masurca/assembly/repeats/RM_239928.SatSep250725262021/  
Soft-masked: $PWD/uconn_masurca/assembly/repeats/outputfiles/uconn_masurca_purgehap_allmaps_2x.fasta.masked    

**UConnMiniasm**  
Repeat library: $PWD/uconn_miniasm/assembly/repeats2/RM_381471.SatSep251145352021/  
Soft-masked: $PWD/uconn_miniasm/assembly/repeats/outputfiles/merged_file_pre_ALLMAPS_Round2_UConn_Miniasm_chrRemoved_Final.fasta.masked  

## Scaffold Renaming  

**CornellMasurca**  
Pattern: cornell_masurca_0001  
Renamed: $PWD/cornell_masurca/assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta    

**CornellMiniasm**  
Pattern: none  
Renamed: $PWD/cornell_miniasm/assembly/Fgra_v0.1_masked.fasta

**UConnMasurca**  
Pattern: uconn_masurca_0001  
Renamed: $PWD/uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta     

**UConnMiniasm**   
Pattern: uconn_miniasm_0001  
Renamed: $PWD/uconn_miniasm/assembly/fagr_uconn_miniasm_masked.fasta  

## RNA-seq Alignments  

**UConn Trimmed RNA-seq:** $PWD_PREVIOUS/annotation/trimmomatic/  
**HBEF Trimmed RNA-seq:** /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/  

**CornellMasurca**	$PWD/cornell_masurca/alignments/hisat/merged_samples_mascura_cornell.bam  


**CornellMiniasm**  $PWD/cornell_miniasm/alignments/hisat/merged_samples_cornell.bam  


**UConnMasurca**	$PWD/uconn_masurca/alignments/hisat/merged_samples_allmaps.bam  


**UConnMiniasm**	$PWD/uconn_miniasm/alignments/hisat/merged_uconn_miniasm_round2.bam  

## De novo Transcriptome Assembly (used in Braker)  

The transcriptome is assembled from HBEF RNA-seq only.  

* Trinity Assembler -   
/core/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/original/fagr/trinity_out\*/  
/core/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/evigene_tr2aacds/Trinity_all.fasta

* Frame selection: Evigene tr2aacds script  
Output directory includes 'okay' and 'okayalt' sets
/core/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/evigene_tr2aacds/okayset  

## Protein Alignments  

GenomeThreader:  

Cornell Masurca was used as a test on including protein alignments as input for Braker.  Alignments for the other assemblies were not needed.
**CornellMascura**	$PWD/cornell_masurca/alignment/gth/gth.aln  


## Gene Prediction  

Braker 2.1.5:  

**CornellMascura**	$PWD/cornell_masurca/annotation/braker_rnaseq/braker_o/braker/augustus.hints.*  

**CornellMiniasm**  $PWD/cornell_miniasm/annotation/braker_rnaseq/braker_o/braker/augustus.hints.*  

**UConnMasurca**	$PWD/uconn_masurca/annotation/braker_rnaseq/braker_o/braker/augustus.hints.*  

**UConnMiniasm**	$PWD/cornell_miniasm/annotation/braker_rnaseq/braker_o/braker/augustus.hints.*  


## Gene Model Filtering  

gFACs:  

**CornellMascura**  $PWD/cornell_masurca/annotation/gfacs_filter/final_o/  

**CornellMiniasm**   $PWD/cornell_miniasm/annotation/gfacs_filter/final_o/  

**UConnMasurca**  $PWD/uconn_masurca/annotation/gfacs_filter/final_o/  

**UConnMiniasm**  $PWD/uconn_miniasm/annotation/gfacs_filter/final_o/  


## Functional Annotation  
Identify functional information with EnTAP.  
--taxon Fagus_grandifolia -c bacteria -c amoeba -c insecta -c fungi  
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/  complete.protein.faa.95.dmnd  
gene family assignment using EggNOG and InterProScan with Pfam database

**CornellMascura**  $PWD/cornell_masurca/annotation/entap/output/final_results/   

**CornellMiniasm**  $PWD/cornell_miniasm/annotation/entap/output/final_results/  
										
**UConnMasurca**  $PWD/uconn_masurca/annotation/entap/output/final_results/

**UConnMiniasm**  $PWD/uconn_miniasm/annotation/entap/output/final_results/     

Filter structural annotation files by genes annotated with EnTAP  
**CornellMascura**  $PWD/cornell_masurca/annotation/gfacs_entap/  

**CornellMiniasm**   $PWD/cornell_miniasm/annotation/gfacs_entap/ 

**UConnMasurca**  $PWD/uconn_masurca/annotation/gfacs_entap/  

**UConnMiniasm**  $PWD/uconn_miniasm/annotation/gfacs_entap/  


## Transcript Overlaps  
Align transcripts to genome using gmap for transcript models.  Use gfacs_entap/out.gtf for gene (braker) models.  

gmap:  

**CornellMascura**  $PWD/cornell_masurca/alignments/gmap/gfacs_o/out.gtf  

**CornellMiniasm**  $PWD/cornell_miniasm/alignments/gmap/gfacs_o/out.gtf

**UConnMasurca**  $PWD/uconn_masurca/alignments/gmap/gfacs_o/out.gtf  

**UConnMiniasm** $PWD/uconn_miniasm/alignments/gmap/gfacs_o/out.gtf  

gffCompare:  
1) braker/gff2bed.sh 2) transcriptome/gff2bed.sh 3) braker/filter.sh 4) transcriptome/filter.sh 5) braker/remPartial/filter.sh 6) transcriptome/remPartial/filter.sh  7) cat.sh creates final.gff

**CornellMascura**  $PWD/cornell_masurca/annotation/bedtools/final.gff  

**CornellMiniasm**  $PWD/cornell_miniasm/annotation/bedtools/final.gff  

**UConnMasurca**  $PWD/uconn_masurca/annotation/bedtools/final.gff  

**UConnMiniasm**  $PWD/uconn_miniasm/annotation/bedtools/final.gff

## Rename Genes  
adjust renamegtfgenes.py for naming pattern. Run gfacs on the out_renamed.gtf to create the final set of annotation files: .gtf, .gff3, genes.fasta (CDS), genes.fasta.faa

**CornellMascura**  $PWD/cornell_masurca/annotation/annotation/gfacs_renamed/  

**CornellMiniasm**   $PWD/cornell_miniasm/annotation/annotation/gfacs_renamed/  

**UConnMasurca**  $PWD/uconn_masurca/annotation/gfacs_renamed 

**UConnMiniasm**  $PWD/uconn_miniasm/annotation/gfacs_renamed/  





