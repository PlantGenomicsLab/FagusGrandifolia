#!/bin/bash
#SBATCH --job-name=rename_headers
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o rename_headers_%j.out
#SBATCH -e rename_headers_%j.err

hostname
echo "\nStart time:"
date

awk '/^>/{print ">fagr_" sprintf("%05d", ++i); next}{ print }' < centroids_fagr.faa > fagr_transcriptome.faa

echo "\nEnd time:"
date

