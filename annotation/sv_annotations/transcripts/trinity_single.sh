#!/bin/bash
#SBATCH -J trinity_refguided
#SBATCH -o trin_ref_%j.out
#SBATCH -e trin_ref_%j.err
#SBATCH -N 1
#SBATCH --mem=180G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu

module load trinity/2.8.5
module load samtools

Trinity --genome_guided_bam /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/hisat/map/sorted_bam/FaFgCo3.bam --genome_guided_max_intron 10000 --max_memory 180G --CPU 24 --full_cleanup --output /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/assembly/ref-based/trinity/trinity_out_FaFgCo3.bam > trinity.FaFgCo3.bam.Run.Log.txt
