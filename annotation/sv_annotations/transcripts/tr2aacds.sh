#!/bin/bash
#SBATCH --job-name=tr2aacds
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o tr2aacds_%j.out
#SBATCH -e tr2aacds_%j.err

hostname
echo "\nStart time:"
date

#for i in ../assembly/original/fagr/trinity_out_*/
#do
#cat $i/Trinity.fasta > Trinity_all.fasta
#done

#/isg/shared/apps/evigene/20190101/scripts/rnaseq/trformat.pl Trinity_all.fasta > Trinity_all_formatted.fasta

module load cdhit/4.6.8
module load exonerate/2.4.0
module load blast/2.11.0

/isg/shared/apps/evigene/20190101/scripts/prot/tr2aacds.pl -NCPU 35 -MAXMEM 400 -log -cdna Trinity_all_formatted.fasta

echo "\nEnd time:"
date

