#!/bin/bash
#SBATCH --job-name=rename
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o rename_%j.out
#SBATCH -e rename_%j.err

hostname
echo "\nStart time:"
date

cat *Fa*.bam/*.fasta *SpFgAl*.bam/*.fasta *SpFgCa2.bam/*.fasta *SpFgCa3.bam/*.fasta *SpFgCo1.bam/*.fasta *SpFgCo3.bam/*.fasta > tmp
awk '/^>/{print ">fagr_" sprintf("%06d", ++i); next}{ print }' < tmp > trinity.headers.fasta
rm tmp

echo "\nEnd time:"
date

