#!/bin/bash
#SBATCH --job-name=renameheader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o renameheader_%j.out
#SBATCH -e renameheader_%j.err

hostname
echo "\nStart time:"
date

awk '/^>/{print ">uconn_masurca_" sprintf("%04d", ++i); next}{ print }' < repeats/outputfiles/uconn_masurca_purgehap_allmaps_2x.fasta.masked  > fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta

echo "\nEnd time:"
date

