#!/bin/bash
#SBATCH --job-name=lengths
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o lengths_%j.out
#SBATCH -e lengths_%j.err

hostname
echo "\nStart time:"
date

python getlengths.py

echo "\nEnd time:"
date

