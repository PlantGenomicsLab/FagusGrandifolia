#!/bin/bash
#SBATCH --job-name=genome_threader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=120G
#SBATCH -o genome_threader1_%j.out
#SBATCH -e genome_threader1_%j.err

module load genomethreader/1.7.1
module load genometools/1.5.10

gt seqtransform -addstopaminos Trinity_all_formatted.all.headers.aa > tmp_all

gth -genomic ../../assembly/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta -protein tmp_all -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o gth_all.aln -force -gcmaxgapwidth 1000000


