#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gfacs-%j.o
#SBATCH -e gfacs-%j.e

module load perl/5.28.1

org="/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly"
genome="$org/fagr_bti_masurca_purgehap_allmaps_2x_masked.fasta"
alignment="gth_all.aln"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f genomethreader_1.6.6_gff3 \
--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--fasta "$genome" \
-O gfacs_all_o \
"$alignment"



