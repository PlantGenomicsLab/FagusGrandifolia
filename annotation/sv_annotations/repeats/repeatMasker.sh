#!/bin/bash
# Submission script for Xanadu
#SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --mem=350GB
#SBATCH --job-name=repeatmasker
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com

echo $filename

module load perl/5.28.1
export PATH=/core/labs/Wegrzyn/annotationtool/software/RepeatMasker/4.0.6:$PATH

RepeatMasker -lib FagrUconnMasurca-families.fa -pa 30 -gff -a -noisy -low -xsmall -dir outputfiles ../allmaps/uconn_masurca_purgehap_allmaps_2x.fasta

