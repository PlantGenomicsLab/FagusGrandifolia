#!/bin/bash
#SBATCH --job-name=RM2UMas
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=34
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mem=500G
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com


module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/
module load RepeatModeler/2.01
module load genometools/1.6.1
module load mafft/7.471
export LTRRETRIEVER_PATH=/core/labs/Wegrzyn/annotationtool/software/LTR_retriever
module load cdhit/4.8.1
module load ninja/0.95


BuildDatabase -name FagrUconnMasurca uconn_masurca_purgehap_allmaps_2x.fasta

nohup RepeatModeler -database FagrUconnMasurca -pa 34 -LTRStruct
