#!/bin/bash
#SBATCH --job-name=gmapB
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH -o gmapB_%j.out
#SBATCH -e gmapB_%j.err

#build gmap index
module load gmap/2019-06-10

indexdir=/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/alignments/gmap
genomepath=/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta
gmap_build -D $indexdir -d fagr_uconn_masurca_genome $genomepath 

