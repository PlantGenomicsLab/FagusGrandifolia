#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=120G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gfacs-%j.o
#SBATCH -e gfacs-%j.e

module load perl/5.28.1

org="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly"
genome="$org/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta"
alignment="fagr_uconn_masurca_gmap_okall.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

options="--statistics \
--statistics-at-every-step \
--allowed-inframe-stop-codons 0 \
--unique-genes-only \
--min-exon-size 6 \
--min-intron-size 9 \
--min-CDS-size 300 \
--create-gtf"

perl "$script" \
-f gmap_2017_03_17_gff3 \
"$options" \
--fasta "$genome" \
-O gfacs_o \
"$alignment"



