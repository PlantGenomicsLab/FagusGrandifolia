#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -o gmap_%j.o
#SBATCH -e gmap_%j.e
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=120G
#SBATCH --cpus-per-task=25

module load gmap/2019-06-10

idx="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/alignments/gmap"
prefix="fagr_uconn_masurca"
fasta="/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/alignments/gmap/Trinity_all_formatted.okall.headers.cds"
# larger genomes will require the gmapl command instead of just 'gmap' 
# if you have problems (like if gmap crashes or if all your alignments have in frame stop codons) then remove the full length flag and use the -T flag

gmap -a 1 --cross-species --fulllength -D "$idx" -d "$prefix"_genome -f gff3_gene "$fasta" --nthreads=25 --min-intronlength=9 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "$prefix"_gmap_okall.gff3 2> "$prefix"_gmap_okall.error



