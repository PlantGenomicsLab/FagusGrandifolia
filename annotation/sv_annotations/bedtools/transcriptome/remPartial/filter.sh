#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

module load bedtools
bedtools intersect -nonamecheck -a ../transcriptome_remInternal_equivalent.bed -b ../../braker/braker_wlr__remInternal.bed -wa -wb > AnyOverlap.txt

bedtools intersect -nonamecheck -f 1.0 -a ../transcriptome_remInternal_equivalent.bed -b ../../braker/braker_wlr__remInternal.bed -wa -wb > CompleteOverlap.txt

diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt 

##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt 
python ../../ReportOverlap.py PartialOverlap.txt PartialOverlap_ReportOverlap.txt

###resolve genes that have little coverage (less than 85%) and are on the same strand
awk -F '\t' '(NR>=1)&&(($6==$12)&&($15<.85))' PartialOverlap_ReportOverlap.txt > resolve.txt 
awk -F '\t' '(NR>=1)&&(($3-$2)<($9-$8))' CompleteOverlap.txt > LongerGeneModel.txt
awk -F '\t' '(NR>=1)&&(($3-$2)==($9-$8))' CompleteOverlap.txt > EqualGeneModel.txt 

###discard genes where coverage is less than 0.85 but on different strand
awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15<.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - > discardGmap
###discard genes where coverage is more than 0.85 and genes are on the same strand
awk -F '\t' '(NR>=1)&&(($6==$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - >> discardGmap
###discard genes where coverage is more than 0.85 and gene are on different strnads,
awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - >> discardGmap

###use the discardGmap file to remove genes that are partially overlapping with braker and are shorter - this will keep ALL the transcriptome alignments NOT ONLY the ones that are overlapping
#python /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/6_Overlap_Analysis/scripts/filterBed.py discardGmap ../transcriptome_remInternal_equivalent.bed transcriptome_remInternal_remEquivalent__remPartial.bed

### create gmap gtf of ALL alignments (that were not discarded in the process)
#cut -f4 transcriptome_remInternal_remEquivalent__remPartial.bed | sort | uniq >> gmap_genes.txt
#python ../../createGFF.py --gff ../out.gtf --nameList gmap_genes.txt --out transcriptome_gene_filtered_overlapped.gff


#OR if you just want to keep the gmap alignment that are ONLY overlapping your ab initio braker genes, then do the following:
#these are gmap genes that are complete encapsulating a braker gene
cut -f4 ../../braker/keepLongerGenes.bed > accepted_transcriptome_alns.txt
#these are gmap genes that are partially overlapping a braker gene but the gmap alignment is longer:
cut -f4 ../../braker/remPartial/keepGmap | sort | uniq >> accepted_transcriptome_alns.txt
# sort these and make sure they are unique
sort accepted_transcriptome_alns.txt | uniq - > accepted_transcriptome_alns_uniq.txt
# only keep genes from the gmap gtf that are in keepGmap
python ../../createGFF.py --gff ../out.gtf --nameList accepted_transcriptome_alns_uniq.txt --out transcriptome_gene_filtered_overlapped.gff
