#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

hostname
echo "\nStart time:"
date

module load bedtools
bedtools intersect -nonamecheck -a trans.bed -b ../braker/braker.bed -wa -wb > AnyOverlap.txt
bedtools intersect -nonamecheck -f 1.0 -a trans.bed -b ../braker/braker.bed -wa -wb > CompleteOverlap.txt
diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt
##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt
#Braker is the LONGER gene model
awk -F '\t' '(NR>=1)&&(($3-$2)<($9-$8))' CompleteOverlap.txt > LongerGeneModel.txt
#Boh genes are equivalent in terms of length
awk -F '\t' '(NR>=1)&&(($3-$2)==($9-$8))' CompleteOverlap.txt > EqualGeneModel.txt
cut -f7,8,9,10,11 LongerGeneModel.txt > keepLongerGenes.bed
#find the shorter transcriptome alignment and save them in discardGmap
cut -f4 LongerGeneModel.txt | sort - | uniq > discardGmap
#find the equivalent length transcriptome alignment and save them in discardGmap (since we prefer the ab initio model derived from the genome)
cut -f4 EqualGeneModel.txt | sort - | uniq >> discardGmap
###remove gmap genes that are completely encapsulated by longer gene models
python /core/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/6_Overlap_Analysis/scripts/filterBed.py discardGmap trans.bed transcriptome_remInternal_equivalent.bed

echo "\nEnd time:"
date

