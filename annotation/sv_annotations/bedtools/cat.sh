#!/bin/bash
#SBATCH --job-name=cat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o cat_%j.out
#SBATCH -e cat_%j.err

hostname
echo "\nStart time:"
date

cat braker/remPartial/braker_genes_filtered_overlapped.gff transcriptome/remPartial/transcriptome_gene_filtered_overlapped.gff > final.gff

echo "\nEnd time:"
date

