import sys

fastanames=[]
fastalines=[]
lines = [line.rstrip('\n') for line in open(sys.argv[1])]

print len(lines)
match=0
partial=0
encapsulate=0

with open (sys.argv[2], "w") as xyz:
    xyz.write("query_scaff\tquery_start\tquery_end\tquery_id\tquery_frame\tquery_strand\tsubject_scaff\tsubject_start\tsubject_end\tsubject_end\tsubject_frame\tsubject_strand\tquery_len\tsubject_len\tcoverage\n")
    for l in lines:
        cols=l.split('\t')
        q_len=int(cols[2])-int(cols[1])
        t_len=int(cols[8])-int(cols[7])
        q_s=int(cols[1])
        q_e=int(cols[2])
        t_s=int(cols[7])
        t_e=int(cols[8])
        if (q_s==t_s and q_e==t_e):
            match+=1
            covg=1
        elif (t_s<=q_s and t_e<q_e):
            overlap=t_e-q_s
            covg=float(overlap)/t_len
            partial+=1
        elif (t_s>q_s and t_e>=q_e):
            overlap=q_e-t_s
            covg=float(overlap)/t_len
            partial+=1
        elif (t_s>q_s and t_e<q_e):
            overlap=t_e-t_s
            covg=float(overlap)/t_len
            partial+=1
        elif (t_s<q_s and t_e>=q_e):
            overlap=q_e-q_s
            covg=float(overlap)/t_len
            partial+=1
        else:
            print "you did not account for something"
            covg=0
        xyz.write("%s\t%s\t%s\t%s\n" %(l,q_len,t_len,covg))

print match
print partial
print encapsulate
