#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

hostname
echo "\nStart time:"
date

module load bedtools
bedtools intersect -nonamecheck -a ../braker_wlr__remInternal.bed -b ../../transcriptome/transcriptome_remInternal_equivalent.bed -wa -wb > AnyOverlap.txt

bedtools intersect -nonamecheck -f 1.0 -a ../braker_wlr__remInternal.bed -b ../../transcriptome/transcriptome_remInternal_equivalent.bed -wa -wb > CompleteOverlap.txt

diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt

##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt
python ../../ReportOverlap.py PartialOverlap.txt PartialOverlap_ReportOverlap.txt

###resolve genes that have little coverage (less than 85%) and are on the same strand
awk -F '\t' '(NR>=1)&&(($6==$12)&&($15<.85))' PartialOverlap_ReportOverlap.txt > resolve.txt

#these should be empty-if they are not something is wrong!
awk -F '\t' '(NR>=1)&&(($3-$2)<($9-$8))' CompleteOverlap.txt > LongerGeneModel.txt
awk -F '\t' '(NR>=1)&&(($3-$2)==($9-$8))' CompleteOverlap.txt > EqualGeneModel.txt

###discard genes where coverage is less than 0.85 but on different strand, check if braker is the short gene model, if so append that gene model to discardBraker
awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15<.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - > discardBraker
###discard genes where coverage is more than 0.85 and genes are on the same strand, check if braker is the short gene model, if so append that gene model to discardBraker
awk -F '\t' '(NR>=1)&&(($6==$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - >> discardBraker
###discard genes where coverage is more than 0.85 and gene are on different strnads, check if braker is the short gene model, if so append that gene model to discardBraker
awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f4 - | sort - | uniq - >> discardBraker
###use the discardBraker file to remove braker genes that are partially overlapping with other gene models and are shorter
python /core/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/6_Overlap_Analysis/scripts/filterBed.py discardBraker ../braker_wlr__remInternal.bed braker_wlr__remInternal__remPartial.bed

### using the bed file, we will create the final braker gff
cut -f4 braker_wlr__remInternal__remPartial.bed > braker_genes.txt
python ../../createGFF.py --gff ../out.gtf --nameList braker_genes.txt --out braker_genes_filtered_overlapped.gff

###this is to figure out which gmap alignments to keep (only useful if you want to only keep gmap alignments that overlap your ab initio gene models and NOT the alignments that don't overlap)

awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15<.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f10 - | sort - | uniq - > keepGmap
awk -F '\t' '(NR>=1)&&(($6==$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f10 - | sort - | uniq - >> keepGmap
awk -F '\t' '(NR>=1)&&(($6!=$12)&&($15>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($9-$8))' - | cut -f10 - | sort - | uniq - >> keepGmap

echo "\nEnd time:"
date

