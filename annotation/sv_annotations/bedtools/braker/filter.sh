#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err
#SBATCH --qos=general
#SBATCH --qos=general


module load bedtools
bedtools intersect -nonamecheck -a braker.bed -b ../transcriptome/trans.bed -wa -wb > AnyOverlap.txt
bedtools intersect -nonamecheck -f 1.0 -a braker.bed -b ../transcriptome/trans.bed -wa -wb > CompleteOverlap.txt
diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt
##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt
##gmap/transcriptome is the LONGER gene model
awk -F '\t' '(NR>=1)&&(($3-$2)<($9-$8))' CompleteOverlap.txt > LongerGeneModel.txt
#both genes are equivalent in terms of length
awk -F '\t' '(NR>=1)&&(($3-$2)==($9-$8))' CompleteOverlap.txt > EqualGeneModel.txt
#I don't know why i do this...it's never used...
cut -f7,8,9,10,11 LongerGeneModel.txt > keepLongerGenes.bed
# find the shorter braker gene and save them in discardBraker
cut -f4 LongerGeneModel.txt | sort - | uniq > discardBraker
###remove braker genes that are completely encapsulated by a longer transcriptome alignment
python /core/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/6_Overlap_Analysis/scripts/filterBed.py discardBraker braker.bed braker_wlr__remInternal.bed

