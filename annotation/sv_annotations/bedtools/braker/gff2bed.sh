#!/bin/bash
#SBATCH --job-name=gff2bed
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gff2bed_%j.out
#SBATCH -e gff2bed_%j.err
#SBATCH --qos=general
#SBATCH --qos=general

module load biopython/1.70

cp ../../gfacs_entap/gfacs_o/out.gtf .
sed -i 's/.*\tgene\t/###\n&/' out.gtf 
python gff2bed.py --gff out.gtf --out braker.bed
