#!/bin/bash
#SBATCH --job-name=hisat2umas19
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 47
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=350G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

DIR="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/alignments/hisat"

for f in /core/labs/Wegrzyn/FagusGenome/fagr/reads/rnaseq/uzay_2019_rnaseq/*_R1_trimmed.fastq.gz
do 
i=$(echo $f | sed 's/_R1_trimmed.fastq.gz//')
b=$(basename $i)
hisat2 -x fagr_uconn_masurca -1 "${i}_R1_trimmed.fastq.gz" -2 "${i}_R2_trimmed.fastq.gz" -p 47 -S "${b}.sam" --dta
samtools view -@ 47 -uhS "$DIR/${b}.sam" | samtools sort -@ 47 -m 7G -o "$DIR/sorted_${b}.bam"
done 

