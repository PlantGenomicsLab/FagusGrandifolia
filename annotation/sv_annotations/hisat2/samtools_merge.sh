#!/bin/bash
#SBATCH --job-name=samtools_merge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=320G
#SBATCH -o samtools_merge%j.out
#SBATCH -e samtools_merge%j.err

module load samtools/1.9

VAR=""
for i in sorted_*.bam
do
  VAR="${VAR}${i} "
done
echo "${VAR}"
samtools merge merged_uconn_masurca.bam ${VAR}

