#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=120G
#SBATCH -o hisat2_%j.out
#SBATCH -e hisat2_%j.err

module load hisat2/2.1.0
module load samtools/1.9

for i in FaFgAl1 FaFgAl2 FaFgAl3 FaFgCa1 FaFgCa2 FaAgCa3 FaFgCo1 FaFgCo2 FaFgCo3 SpFgAl1 SpFgAl2 SpFgCa1 SpFgCa2 SpFgCa3 SpFgCo1 SpFgCo2 SpFgCo3
do 
hisat2 -x fagr_uconn_masurca -1 "/core/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/${i}_R1_trimmed.fastq.gz" -2 "/core/labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/${i}_R2_trimmed.fastq.gz" -p 32 -S "${i}.sam" --dta
samtools view -@ 32 -uhS "${i}.sam" | samtools sort -@ 32 -o "sorted_${i}.bam"
done 

