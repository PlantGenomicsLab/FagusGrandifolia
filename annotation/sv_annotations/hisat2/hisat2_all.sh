#!/bin/bash
#SBATCH --job-name=hisat2fagr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 47
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=250G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
module load samtools/1.9

DIR="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/alignments/hisat"
LIBS="/core/labs/Wegrzyn/FagusGenome/fagr/reads/rnaseq/forestgeo1/trimmomatic/"
LIBNAMES="/core/labs/Wegrzyn/FagusGenome/fagr/reads/rnaseq/forestgeo1/trimmomatic/filtered_library_names.txt"

while read f
do
hisat2 -x fagr_uconn_masurca -1 "$LIBS/${f}_1P.fq.gz" -2 "$LIBS/${f}_2P.fq.gz" -p 47 -S "${f}.sam" --dta
samtools view -@ 47 -uhS "$DIR/${f}.sam" | samtools sort -@ 47 -m 7G -o "$DIR/sorted_${f}.bam"
done < $LIBNAMES

