#!/bin/bash
#SBATCH --job-name=indexbuild
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mem=100G
#SBATCH -o indexbuild_%j.out
#SBATCH -e indexbuild_%j.err

module load hisat2/2.1.0
hisat2-build -p 30 ../../assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta fagr_uconn_masurca
