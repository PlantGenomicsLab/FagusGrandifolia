#!/bin/bash
#SBATCH --job-name=entapUMas
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=400G
#SBATCH --qos=himem
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=himem2

module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.35-74.0
module load TransDecoder/5.3.0


/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP --runP --ini /core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/annotation/entap/entap_config.ini  -i /core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/annotation/gfacs_filter/final_o/genes_no_asterisk.fasta.faa -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.95.dmnd --threads 36 --out-dir /core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/annotation/entap/output
