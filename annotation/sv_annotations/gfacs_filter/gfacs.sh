#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=145G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gfacs-%j.o
#SBATCH -e gfacs-%j.e

module load perl/5.28.1

genome="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta"
alignment="../braker_rnaseq/braker_o/braker/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d mono_o ]; then
        mkdir mono_o
fi
if [ ! -d multi_o ]; then
        mkdir multi_o
fi

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-multiexonics \
--rem-all-incompletes \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--get-protein-fasta \
--fasta "$genome" \
-O mono_o \
"$alignment"

perl "$script" \
-f braker_2.05_gff3 \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--rem-monoexonics \
--rem-5prime-3prime-incompletes \
--rem-genes-without-start-and-stop-codon \
--min-exon-size 6 \
--fasta "$genome" \
-O multi_o \
"$alignment"

cd mono_o
if [ ! -d interproscan ]; then
	mkdir interproscan
fi
cd interproscan

echo "#!/bin/bash
#SBATCH --job-name=interproscan_pfam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o interproscan_pfam_%j.out
#SBATCH -e interproscan_pfam_%j.err

module load interproscan

sed 's/*$//g' ../genes.fasta.faa > genes.fasta.faa

interproscan.sh -appl Pfam -i genes.fasta.faa" > interproscan_pfam.sh

sbatch --wait interproscan_pfam.sh

cd ../

sed 's/\s.*$//' interproscan/genes.fasta.faa.tsv | uniq > pfam_ids.txt

python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList pfam_ids.txt --idPath . --out pfam_gene_table.txt

cd ../

cat mono_o/pfam_gene_table.txt  multi_o/gene_table.txt > mono_multi_gene_table.txt

if [ ! -d final_o ]; then
	mkdir final_o
fi
alignment="mono_multi_gene_table.txt"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--splice-table \
--get-protein-fasta \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O final_o \
"$alignment"

