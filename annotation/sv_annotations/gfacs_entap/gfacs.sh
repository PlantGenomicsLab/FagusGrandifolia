#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gfacs_%j.out
#SBATCH -e gfacs_%j.err

hostname
echo "\nStart time:"
date

module load perl/5.28.1
genome="/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/fagr_uconn_masurca_purgehap_allmaps_2x_masked.fasta"
alignment="../gfacs_filter/final_o/gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--create-gtf \
--fasta "$genome" \
--entap-annotation /core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/annotation/entap/output/final_results/final_annotations_lvl0.tsv \
--annotated-all-genes-only \
-O gfacs_o \
"$alignment"


echo "\nEnd time:"
date

