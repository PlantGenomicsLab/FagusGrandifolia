# DNA Sequencing 
American Beech leaf tissue was sequenced using Oxford Nanopore PromethION v9.4 and Illumina short-read sequencing.  

## PromethION long-read sequencing

### UConn
FASTQ: /core/labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fagr_promethion_reads_pass.fastq  
FAST5: /core/labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fast5_pass/  
FASTA: /core/labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fasta/fagr_promethion_reads_pass.fasta
Nanoplot: /core/labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fasta/nanoplot_fagr_promethion_reads_pass  
[Nanoplot report html files](https://gitlab.com/PlantGenomicsLab/FagusGrandifolia/-/tree/master/assembly/test_assemblies/nanoplot_fagr_promethion_reads) 

Centrifuge filtered:
/core/labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam_updated_abvf_index.fastq

Post-filter Sequencing Coverage:  

### BTI  
Reference full set: /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/Fagus_run1-4.fastq.gz  
Reference filtered < 10k: /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/Fagus_run1-4.10k.fastq.gz  

Centrifuge filtered:
/core/labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_cornell_run1-4.10k_reads_pass_rmv_contam_updated_abvf_index.fastq

**Long-read Sequencing Coverage:** ? 


## Illumina short-read sequencing

### UConn

Reference individual:
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R1_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R2_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R1_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R2_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R1_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R2_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R1_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R2_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R1_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R2_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R1_001.fastq.gz
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R2_001.fastq.gz

Sickle trimmed: 
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_

Kraken filtered:
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/kraken/


### BTI  
Reference individual:   
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC8g_S4_R1_001.fastq 
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC8g_S4_R2_001.fastq  

Resequenced individuals:
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC10g_S5_R1_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC10g_S5_R2_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC1g_S1_R1_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC1g_S1_R2_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC5g_S2_R1_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC5g_S2_R2_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC7g_S3_R1_001.fastq
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/ArnotC7g_S3_R2_001.fastq

Sickle trimmed:
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/cornell_reads/illumina_dna/sickle

Kraken filtered:
* /core/labs/Wegrzyn/FagusGenome/fagr/reads/kraken/

Raw Sequencing Coverage: ?  