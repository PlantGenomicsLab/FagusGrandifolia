#!/bin/bash
#SBATCH --job-name=fagr_masurca_hybrid_assembly_rmv_contam_input_purge_dup
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=150G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
module load python/3.6.3
module load purge_dups/1.0.0
module load bwa/0.7.5a
#module load busco/3.0.2b
#module unload augustus
#export PATH=/home/CAM/astarovoitov/augustus/bin:/home/CAM/astarovoitov/augustus/scripts:$PATH
#export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config

run_purge_dups.py -p bash config_masurca_hybrid_assembly_rmv_contam_input.json /isg/shared/apps/purge_dups/1.0.0/src/ purge_dup_masurca_hybrid_assembly_rmv_contam_input
