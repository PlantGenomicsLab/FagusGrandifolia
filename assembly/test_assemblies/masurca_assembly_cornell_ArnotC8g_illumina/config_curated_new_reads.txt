DATA
PE= aa 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/ArnotC8g_S4_001_unclassified_1.fastq /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/ArnotC8g_S4_001_unclassified_2.fastq
NANOPORE= /projects/EBP/Wegrzyn/fagr/cornell/reads/Fagus_run1-4.10k.fastq.gz
END

PARAMETERS
GRAPH_KMER_SIZE = auto
USE_LINKING_MATES = 0
LIMIT_JUMP_COVERAGE = 300
CA_PARAMETERS =  cgwErrorRate=0.15
KMER_COUNT_THRESHOLD = 1
NUM_THREADS = 40
JF_SIZE = 4500000000
SOAP_ASSEMBLY=0
FLYE_ASSEMBLY=0
END

