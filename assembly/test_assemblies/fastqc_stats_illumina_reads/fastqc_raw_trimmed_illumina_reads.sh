#!/bin/bash
#SBATCH --job-name=fastqc_raw_trimmed_illumina_reads
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

#################################################################
# FASTQC of raw reads 
#################################################################

module load fastqc/0.11.7

fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-1_S1_L001_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-1_S1_L001_R2_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-1_S1_L002_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-1_S1_L002_R2_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-2_S2_L001_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-2_S2_L001_R2_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-2_S2_L002_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-2_S2_L002_R2_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-3_S3_L001_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-3_S3_L001_R2_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-3_S3_L002_R1_001.fastq
fastqc --outdir illumina_fastqc/ illumina_raw_reads/trimmed_FG-3_S3_L002_R2_001.fastq
