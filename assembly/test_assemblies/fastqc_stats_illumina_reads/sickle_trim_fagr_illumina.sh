#!/bin/bash
#SBATCH --job-name=fastq_trimming_pafricana_transcriptome
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=200G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
################################################################# # Trimming of Reads using Sickle ################################################################# 
module load sickle/1.33

sickle pe -f FG-1_S1_L001_R1_001.fastq.gz -r FG-1_S1_L001_R2_001.fastq.gz -t sanger -o trimmed_FG-1_S1_L001_R1_001.fastq -p trimmed_FG-1_S1_L001_R2_001.fastq -s trimmed_singles_FG-1_S1_L001.fastq -q 30 -l 40
sickle pe -f FG-1_S1_L002_R1_001.fastq.gz -r FG-1_S1_L002_R2_001.fastq.gz -t sanger -o trimmed_FG-1_S1_L002_R1_001.fastq -p trimmed_FG-1_S1_L002_R2_001.fastq -s trimmed_singles_FG-1_S1_L002.fastq -q 30 -l 40
sickle pe -f FG-2_S2_L001_R1_001.fastq.gz -r FG-2_S2_L001_R2_001.fastq.gz -t sanger -o trimmed_FG-2_S2_L001_R1_001.fastq -p trimmed_FG-2_S2_L001_R2_001.fastq -s trimmed_singles_FG-2_S2_L001.fastq -q 30 -l 40
sickle pe -f FG-2_S2_L002_R1_001.fastq.gz -r FG-2_S2_L002_R2_001.fastq.gz -t sanger -o trimmed_FG-2_S2_L002_R1_001.fastq -p trimmed_FG-2_S2_L002_R2_001.fastq -s trimmed_singles_FG-2_S2_L002.fastq -q 30 -l 40
sickle pe -f FG-3_S3_L001_R1_001.fastq.gz -r FG-3_S3_L001_R2_001.fastq.gz -t sanger -o trimmed_FG-3_S3_L001_R1_001.fastq -p trimmed_FG-3_S3_L001_R2_001.fastq -s trimmed_singles_FG-3_S3_L001.fastq -q 30 -l 40
sickle pe -f FG-3_S3_L002_R1_001.fastq.gz -r FG-3_S3_L002_R2_001.fastq.gz -t sanger -o trimmed_FG-3_S3_L002_R1_001.fastq -p trimmed_FG-3_S3_L002_R2_001.fastq -s trimmed_singles_FG-3_S3_L002.fastq -q 30 -l 40

