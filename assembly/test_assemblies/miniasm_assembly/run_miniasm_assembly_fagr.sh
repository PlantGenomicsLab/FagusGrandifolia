#!/bin/bash
#SBATCH --job-name=miniasm_assembly_fagr_promethion_rmv_contam_input
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=500G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
module load miniasm/0.3
module load racon/0.5.0

minimap2 -x ava-ont -t 64 /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam.fastq /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam.fastq | gzip -1 > fagr_promethion_reads_pass_rmv_contam.paf.gz
miniasm -R -f /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam.fastq fagr_promethion_reads_pass_rmv_contam.paf.gz > fagr_miniasm_assembly_promethion_rmv_contam_input.gfa
awk '$1 ~/S/ {print ">"$2"\n"$3}' fagr_miniasm_assembly_promethion_rmv_contam_input.gfa > fagr_miniasm_assembly_promethion_rmv_contam_input.fasta