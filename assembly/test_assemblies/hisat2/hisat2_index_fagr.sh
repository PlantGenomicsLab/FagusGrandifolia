#!/bin/bash
#SBATCH --job-name=hisat2index_fagr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=200G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#make limulus genome into hisat2 index
module load hisat2/2.1.0
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_shasta_assembly_default_minlen/consensus.fasta medaka_fagr_shasta_assembly_default_minlen_hisatindex2
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_polished_shasta_assembly_default_minlen_3kb.fasta fagr_polished_shasta_assembly_default_minlen_3kb_hisatindex2
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_illumina_masurca_purgehap/fagr_masurca_purgehap_purgedup/seqs/curated.purged.fa fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_hisatindex2
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_flye_assembly/consensus.fasta medaka_fagr_flye_assembly_hisatindex2
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/consensus.fasta medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_hisatindex2
hisat2-build -p 32 /labs/Wegrzyn/FagusGenome/fagr/masurca_illumina_only_assembly/CA/final.genome.scf.fasta masurca_illumina_only_assembly_hisatindex2
