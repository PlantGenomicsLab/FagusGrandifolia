#!/bin/bash
#SBATCH --job-name=hisat2_align_fagr_medaka-shasta_curated-masurca_nanopolish-shasta_masurca-ill-only_medaka-flye_medaka-miniasm_FaFgAl1__trimmed_dta_flag
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0

hisat2 --dta -p 32 -x medaka_fagr_shasta_assembly_default_minlen_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S medaka_fagr_shasta_assembly_default_minlen_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam
hisat2 --dta -p 32 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam
hisat2 --dta -p 32 -x fagr_polished_shasta_assembly_default_minlen_3kb_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S fagr_polished_shasta_assembly_default_minlen_3kb_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam
hisat2 --dta -p 32 -x masurca_illumina_only_assembly_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S fagr_masurca_illumina_only_assembly_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam
hisat2 --dta -p 32 -x medaka_fagr_flye_assembly_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S medaka_fagr_flye_assembly_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam
hisat2 --dta -p 32 -x medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_hisatindex2 \
-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R1_trimmed.fastq \
-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/SpFgCa1_R2_trimmed.fastq \
-S medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_alignment_trimmed_SpFgCa1_hisat_dta_flag.sam

###hisat2 --dta -p 32 -x medaka_fagr_shasta_assembly_default_minlen_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S medaka_fagr_shasta_assembly_default_minlen_alignment_trimmed_FaFgAl1_hisat.sam
###hisat2 --dta -p 32 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_alignment_trimmed_FaFgAl1_hisat_dta_flag.sam
###hisat2 --dta -p 32 -x fagr_polished_shasta_assembly_default_minlen_3kb_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S fagr_polished_shasta_assembly_default_minlen_3kb_alignment_trimmed_FaFgAl1_hisat_dta_flag.sam
###hisat2 --dta -p 32 -x masurca_illumina_only_assembly_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S fagr_masurca_illumina_only_assembly_alignment_trimmed_FaFgAl1_hisat_dta_flag.sam
###hisat2 --dta -p 32 -x medaka_fagr_flye_assembly_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S medaka_fagr_flye_assembly_alignment_trimmed_FaFgAl1_hisat_dta_flag.sam
###hisat2 --dta -p 32 -x medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_hisatindex2 \
###-1 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R1_trimmed.fastq \
###-2 /labs/Wegrzyn/HBEF/trees/hbef_transcriptomes/data/fastq_trimmed/FaFgAl1_R2_trimmed.fastq \
###-S medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_alignment_trimmed_FaFgAl1_hisat_dta_flag.sam

