#!/bin/bash
#SBATCH --job-name=bowtie_align_fagr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 48
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=450G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bowtie2/2.3.5.1

bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-1_S1_L001_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-1_S1_L001_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-1_S1_L001.sam -p 32
bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-1_S1_L002_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-1_S1_L002_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-1_S1_L002.sam -p 32
bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-2_S2_L001_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-2_S2_L001_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-2_S2_L001.sam -p 32
bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-2_S2_L002_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-2_S2_L002_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-2_S2_L002.sam -p 32
bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-3_S3_L001_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-3_S3_L001_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-3_S3_L001.sam -p 32
bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-3_S3_L002_R1_001.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/trimmed_FG-3_S3_L002_R2_001.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_read_FG-3_S3_L002.sam -p 32

bowtie2 -x fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_1.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_2.fastq -S fagr_masurca_assembly_curated_purgehap_purgedup_illumina_trimmed_kraken_unclassified_read_FG-1_S1_L001.sam -p 32
bowtie2 -x medaka_fagr_flye_assembly_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_1.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_2.fastq -S medaka_fagr_flye_assembly_illumina_trimmed_kraken_unclassified_read_FG-1_S1_L001.sam -p 48
bowtie2 -x medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_1.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_2.fastq -S medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_illumina_trimmed_kraken_unclassified_read_FG-1_S1_L001.sam -p 48
bowtie2 -x fagr_polished_shasta_assembly_default_minlen_3kb_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_1.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_2.fastq -S fagr_polished_shasta_assembly_default_minlen_3kb_illumina_trimmed_kraken_unclassified_read_FG-1_S1_L001.sam -p 48
bowtie2 -x masurca_illumina_only_assembly_idx -1 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_1.fastq -2 /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/unclassified_2.fastq -S masurca_illumina_only_assembly_illumina_trimmed_kraken_unclassified_read_FG-1_S1_L001.sam -p 48
