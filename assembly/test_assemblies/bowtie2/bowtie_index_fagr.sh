#!/bin/bash
#SBATCH --job-name=bowtie_index_fagr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bowtie2/2.3.5.1

bowtie2-build /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_illumina_masurca_purgehap/fagr_masurca_purgehap_purgedup/seqs/curated.purged.fa fagr_masurca_hybrid_assembly_curated_purgehap_purgedup_idx --threads 32
bowtie2-build /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_flye_assembly/consensus.fasta medaka_fagr_flye_assembly_idx --threads 48
bowtie2-build /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_polished_shasta_assembly_default_minlen_3kb.fasta fagr_polished_shasta_assembly_default_minlen_3kb_idx --threads 48
bowtie2-build /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/consensus.fasta medaka_fagr_miniasm_assembly_promethion_rmv_contam_input_idx --threads 48
bowtie2-build /labs/Wegrzyn/FagusGenome/fagr/masurca_illumina_only_assembly/CA/final.genome.scf.fasta masurca_illumina_only_assembly_idx --threads 48
