#!/bin/bash
#SBATCH --job-name=busco_fagus_grandifolia_shasta_assembly_minlen_500_viridiplantae
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus
export PATH=/home/CAM/astarovoitov/augustus/bin:/home/CAM/astarovoitov/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config
###if your run crashes uncomment the following:
#module unload blast/2.7.1
#module load blast/2.2.31

run_BUSCO.py -i /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/2ShastaRun_minlen_500/Assembly.fasta -l /labs/Wegrzyn/Moss/viridiplantae_odb10/ -o fagr_shasta_assembly_minlen_500_viridiplantae -m geno -c 1

