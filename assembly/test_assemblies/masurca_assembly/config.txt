DATA
PE= aa 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R2_001.fastq.gz
PE= ab 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R2_001.fastq.gz
PE= ac 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R2_001.fastq.gz
PE= ad 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R2_001.fastq.gz
PE= ae 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R2_001.fastq.gz
PE= af 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R2_001.fastq.gz
NANOPORE= /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fastq.tar.gz 
END

PARAMETERS
EXTEND_JUMP_READS=0
GRAPH_KMER_SIZE = auto
USE_LINKING_MATES = 0
USE_GRID=1
GRID_ENGINE=SLURM
GRID_QUEUE=general
GRID_BATCH_SIZE=100000000
LHE_COVERAGE=25
MEGA_READS_ONE_PASS=0
LIMIT_JUMP_COVERAGE = 300
CA_PARAMETERS =  cgwErrorRate=0.15
CLOSE_GAPS=1
NUM_THREADS = 16
JF_SIZE = 600000000
SOAP_ASSEMBLY=0
FLYE_ASSEMBLY=0
END

