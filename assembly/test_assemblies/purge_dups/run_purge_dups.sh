#!/bin/bash
#SBATCH --job-name=fagr_masurca_purgehap_purge_dup
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
module load python/3.6.3
module load zlib/1.2.11
module load bwa/0.7.5a


python /home/CAM/astarovoitov/purge_dups/scripts/run_purge_dups.py -p bash config_masurca_assembly_post_purgehap_purgedup.json /home/CAM/astarovoitov/purge_dups/src/ fagr_masurca_purgehap_purgedup
