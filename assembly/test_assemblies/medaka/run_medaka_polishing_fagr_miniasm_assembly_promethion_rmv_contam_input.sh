#!/bin/bash
#SBATCH --job-name=medaka_polishing_fagr_miniasm_assembly_promethion_rmv_contam_input
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=125G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load medaka/0.11.4
module load python/3.6.3
module unload tabix/0.2.6
module load zlib/1.2.11

medaka_consensus -i fagr_promethion_reads_pass_rmv_contam.fastq -d fagr_miniasm_assembly_promethion_rmv_contam_input.fasta -o medaka_fagr_miniasm_assembly_promethion_rmv_contam_input -t 32 -m r941_prom_high_g303
