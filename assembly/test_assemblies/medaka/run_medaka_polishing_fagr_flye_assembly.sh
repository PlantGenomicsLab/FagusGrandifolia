#!/bin/bash
#SBATCH --job-name=medaka_polishing_fagr_flye_assembly
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=125G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load medaka/0.11.4
module load python/3.6.3
module unload tabix/0.2.6
module load zlib/1.2.11

cp /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fagr_promethion_reads_pass.fastq /labs/Wegrzyn/FagusGenome/fagr/medaka/
cp /labs/Wegrzyn/FagusGenome/fagr/flye_assembly/assembly.fasta /labs/Wegrzyn/FagusGenome/fagr/medaka/
medaka_consensus -i fagr_promethion_reads_pass.fastq -d assembly.fasta -o medaka_fagr_flye_assembly -t 32 -m r941_prom_high_g303
