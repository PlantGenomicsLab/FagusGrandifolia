#!/bin/bash
#SBATCH --job-name=medaka_polishing_fagr_shasta_assembly_default_minlen
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=300G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load medaka/0.11.4
module load python/3.6.3
module unload tabix/0.2.6
module load zlib/1.2.11
#module load vcftools/0.1.16
#export TF_FORCE_GPU_ALLOW_GROWTH=true

### cp /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/1Shastarun_default_minlen/Assembly.fasta /labs/Wegrzyn/FagusGenome/fagr/medaka/
### cp /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/fagr_promethion_reads_pass.fasta /labs/Wegrzyn/FagusGenome/fagr/medaka/
medaka_consensus -i fagr_promethion_reads_pass.fasta -d Assembly.fasta -o medaka_fagr_shasta_assembly_default_minlen -t 48 -m r941_prom_high_g303
