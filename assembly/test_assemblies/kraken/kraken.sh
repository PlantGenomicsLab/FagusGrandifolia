#!/bin/bash
#SBATCH --job-name=kraken_fagus_grandifolia_raw_illumina_reads
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=200G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load kraken/2.0.8-beta
module load jellyfish/2.2.6

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-1_S1_L001_kraken_general.out \
        --unclassified-out FG-1_S1_L001_unclassified#.fastq \
        --classified-out FG-1_S1_L001_classified#.fastq      \
        --report FG-1_S1_L001_kraken_report.txt \
        --use-mpa-style

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-1_S1_L002_kraken_general.out \
        --unclassified-out FG-1_S1_L002_unclassified#.fastq \
        --classified-out FG-1_S1_L002_classified#.fastq      \
        --report FG-1_S1_L002_kraken_report.txt \
        --use-mpa-style

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-2_S2_L001_kraken_general.out \
        --unclassified-out FG-2_S2_L001_unclassified#.fastq \
        --classified-out FG-2_S2_L001_classified#.fastq      \
        --report FG-2_S2_L001_kraken_report.txt \
        --use-mpa-style

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-2_S2_L002_kraken_general.out \
        --unclassified-out FG-2_S2_L002_unclassified#.fastq \
        --classified-out FG-2_S2_L002_classified#.fastq      \
        --report FG-2_S2_L002_kraken_report.txt \
        --use-mpa-style

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-3_S3_L001_kraken_general.out \
        --unclassified-out FG-3_S3_L001_unclassified#.fastq \
        --classified-out FG-3_S3_L001_classified#.fastq      \
        --report FG-3_S3_L001_kraken_report.txt \
        --use-mpa-style

kraken2 -db /isg/shared/databases/kraken2/Standard \
        --paired /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R2_001.fastq.gz \
        --use-names \
        --threads 8 \
        --output FG-3_S3_L002_kraken_general.out \
        --unclassified-out FG-3_S3_L002_unclassified#.fastq \
        --classified-out FG-3_S3_L002_classified#.fastq      \
        --report FG-3_S3_L002_kraken_report.txt \
        --use-mpa-style
