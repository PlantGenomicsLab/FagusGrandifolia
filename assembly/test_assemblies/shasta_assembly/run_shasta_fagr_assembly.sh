#!/bin/bash
#SBATCH --job-name=shasta_assembly_fagr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 64
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=500G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load shasta/0.2.0

shasta --input fagr_promethion_reads_pass.fasta --memoryMode anonymous --memoryBacking 4K --threads 64

