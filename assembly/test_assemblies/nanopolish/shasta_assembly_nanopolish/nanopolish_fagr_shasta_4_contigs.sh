#!/bin/bash
#SBATCH --job-name=nanopolish_fagr_shasta_assembly_4_contigs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load nanopolish/0.11.1
module load python/3.6.3
module load biopython/1.70
module load parallel/20180122

python nanopolish_makerange.py fagr_shasta_assembly_4_contigs.fasta | parallel --results nanopolish.results -P 9 --tmpdir nanopolish_contigs_4_tmp/ \
nanopolish variants --consensus -o nanopolish_polished_fagr_shasta_contigs_4/fagr_shasta_assembly_polished.{1}.vcf -w {1} -r /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/fagr_promethion_reads_pass.fasta -b fagr_shasta_assembly_reads.sorted.bam -g fagr_shasta_assembly_4_contigs.fasta -t 4 --min-candidate-frequency 0.1

