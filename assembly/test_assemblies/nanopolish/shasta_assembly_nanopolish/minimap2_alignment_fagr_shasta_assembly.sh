#!/bin/bash
#SBATCH --job-name=minimap2_alignment_fagr_shasta_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.15
module load samtools/1.7

minimap2 -ax map-ont -t 8 /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/1Shastarun/Assembly.fasta /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/fagr_promethion_reads_pass.fasta | samtools sort -o /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_shasta_assembly_reads.sorted.bam -T reads.tmp

samtools index /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_shasta_assembly_reads.sorted.bam

