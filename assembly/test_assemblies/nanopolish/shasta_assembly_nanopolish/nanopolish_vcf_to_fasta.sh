#!/bin/bash
#SBATCH --job-name=nanopolish_fagr_polished_shasta_assembly_default_minlen_vcf_to_fasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load nanopolish/0.11.1
module load python/3.6.3
module load biopython/1.70

nanopolish vcf2fasta --skip-checks -g /labs/Wegrzyn/FagusGenome/fagr/busco_analysis/fagr_shasta_assembly_default_minlen_3kb.fasta -f nanopolish_contig_vcf_names.fofn > fagr_polished_shasta_assembly_default_minlen_3kb.fasta


