import sys
from Bio import SeqIO

genome_assembly = "/labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/1Shastarun/Assembly.fasta"

fasta1 = "fagr_shasta_assembly_1_contigs.fasta"
fasta2 = "fagr_shasta_assembly_2_contigs.fasta"
fasta3 = "fagr_shasta_assembly_3_contigs.fasta"
fasta4 = "fagr_shasta_assembly_4_contigs.fasta"
fasta5 = "fagr_shasta_assembly_5-6_contigs.fasta"
fasta6 = "fagr_shasta_assembly_7-9_contigs.fasta"

with open(fasta1, "w") as a, open(fasta2, "w") as b, open(fasta3, "w") as c, open(fasta4, "w") as d, open(fasta5, "w") as e, open(fasta6, "w") as f:
	for seq in SeqIO.parse(genome_assembly, 'fasta'):
		if int(seq.id[0]) == 1:
			SeqIO.write(seq, a, "fasta")
		elif int(seq.id[0]) == 2:
			SeqIO.write(seq, b, "fasta")
		elif int(seq.id[0]) == 3:
			SeqIO.write(seq, c, "fasta")
		elif int(seq.id[0]) == 4:
			SeqIO.write(seq, d, "fasta")
		elif int(seq.id[0]) > 4 and int(seq.id[0]) <= 6:
			SeqIO.write(seq, e, "fasta")
		else:
			SeqIO.write(seq, f, "fasta")

