#!/bin/bash
#SBATCH --job-name=minimap2_alignment_fagr_masurca_hybrid_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.15
module load samtools/1.7

minimap2 -ax map-ont -t 8 /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/CA.mr.41.15.15.0.02/final.genome.scf.fasta /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fastq.tar.gz | samtools sort -o fagr_masurca_hybrid_assembly_reads.sorted.bam -T reads.tmp

samtools index fagr_masurca_hybrid_assembly_reads.sorted.bam

