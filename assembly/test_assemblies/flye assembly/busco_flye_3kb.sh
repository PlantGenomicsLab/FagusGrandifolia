#!/bin/bash
#SBATCH --job-name=busco_fagus_grandifolia_flye_assembly_embryophyta
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus
export PATH=/home/CAM/ssarker/augustus/bin:/home/CAM/ssarker/augustus/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config
###if your run crashes uncomment the following:
module unload blast/2.7.1
module load blast/2.2.31

run_BUSCO.py -i /labs/Wegrzyn/FagusGenome/fagr/flye_assembly/fagr_flye_assembly_3kb.fasta -l /isg/shared/databases/busco_lineages/embryophyta_odb10/ -o flye3bk_fagr_assembly_embryophyta -m geno -c 1
