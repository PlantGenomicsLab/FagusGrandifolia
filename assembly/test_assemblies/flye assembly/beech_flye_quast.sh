#!/bin/bash
#SBATCH --job-name=flye_beech_quast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

hostname
module load quast/5.0.2

quast.py /labs/Wegrzyn/FagusGenome/fagr/flye_assembly/assembly.fasta -o flye_quast
