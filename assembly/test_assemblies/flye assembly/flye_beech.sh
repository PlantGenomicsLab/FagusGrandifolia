#!/bin/bash
#SBATCH --job-name=flye_assembly_beech
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o %x_%flye.out
#SBATCH -e %x_%flye.err


module load flye/2.4.2
flye --nano-raw /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fagr_promethion_reads_pass.fastq --genome-size 1g --out-dir /labs/Wegrzyn/FagusGenome/fagr/flye_assembly --threads 36
