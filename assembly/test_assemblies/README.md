# *Fagus grandifolia* Genome Assembly  

Spreadsheet:  
https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit?usp=sharing  

All scripts involved in the genome assembly process are located in **fagus_grandifolia_genome_assembly** directory.

## Contents:
### **[1) DNA Sequencing:](https://gitlab.com/PlantGenomicsLab/HBEF#1-dna-sequencing-1)**  
Description of Illumina short-read and Oxford Nanopore long-read sequencing raw data  
### **[2) Refine Input Reads:](https://gitlab.com/PlantGenomicsLab/HBEF#2-refine-input-reads-1)**
Centrifuge and Kraken quality control of raw Illumina and Oxford Nanopore reads  
### **[3) Genome Assembly:](https://gitlab.com/PlantGenomicsLab/HBEF#3-genome-assembly-1)**
Draft genome assembly using a variety of assembly software and refined input reads  
#### Genome Assembly Software and Input Overview:
- [Shasta Assembly](https://gitlab.com/PlantGenomicsLab/HBEF#shasta-assembly)
- [Flye Assembly](https://gitlab.com/PlantGenomicsLab/HBEF#flye-assembly)
- [Masurca Short-read Assembly](https://gitlab.com/PlantGenomicsLab/HBEF#masurca-short-read-assembly)
- [Masurca Hybrid Assembly](https://gitlab.com/PlantGenomicsLab/HBEF#masurca-hybrid-assembly)
- [Masurca Hybrid Assembly with Clean Read Input](https://gitlab.com/PlantGenomicsLab/HBEF#masurca-hybrid-assembly-with-clean-read-input)
- [Masurca Hybrid Assembly with New Cornell ArnotC8g Illumina Read Input](https://gitlab.com/PlantGenomicsLab/HBEF#masurca-hybrid-assembly-with-new-cornell-arnotc8g-illumina-read-input)
- [Miniasm Assembly with Clean Read Input](https://gitlab.com/PlantGenomicsLab/HBEF#miniasm-assembly-with-clean-read-input)

### **[4) Improve Genome Assemblies:](https://gitlab.com/PlantGenomicsLab/HBEF#4-improve-genome-assemblies-1)**
Improving genome assembly quality using polishing software
#### Polished Genome Assembly Overview:
- [Shasta Assembly (Nanopolish)](https://gitlab.com/PlantGenomicsLab/HBEF#nanopolish-to-improve-shasta-genome-assembly)
- [Masurca Hybrid Assembly (purge-haplotigs)](https://gitlab.com/PlantGenomicsLab/HBEF#purge-haplotigs-to-improve-masurca-hybrid-assembly)
- [Masurca Hybrid Assembly (purge-haplotigs & purge-dups)](https://gitlab.com/PlantGenomicsLab/HBEF#purge-dups-to-improve-masurca-hybrid-assembly) : **Highest Quality Assembly**
- [Masurca Hybrid Assembly with Clean Read Input (purge-dups)](https://gitlab.com/PlantGenomicsLab/HBEF#purge-dups-to-improve-masurca-hybrid-assembly-with-clean-read-input)
- [Masurca Hybrid Assembly with New Cornell ArnotC8g Illumina Read Raw & Curated Input (purge-dups)](https://gitlab.com/PlantGenomicsLab/HBEF#purge-dups-to-improve-masurca-hybrid-assembly-with-new-cornell-arnotc8g-illumina-read-input)
- [Miniasm Assembly with Clean Read Input (Medaka)](https://gitlab.com/PlantGenomicsLab/HBEF#medaka-polishing-to-improve-miniasm-assembly-with-clean-read-input)
- [Miniasm Assembly with Clean Read Input (Medaka & purge-dup)](https://gitlab.com/PlantGenomicsLab/HBEF#purge-dups-to-improve-medaka-polished-miniasm-assembly-with-clean-read-input)
- [Flye Assembly (Medaka)](https://gitlab.com/PlantGenomicsLab/HBEF#medaka-polishing-to-improve-flye-assembly)
- [Shasta Assembly (Medaka)](https://gitlab.com/PlantGenomicsLab/HBEF#medaka-polishing-to-improve-shasta-assembly)

### **[5) Analysis:](https://gitlab.com/PlantGenomicsLab/HBEF#5-analysis-1)**
Assessment of genome assembly quality with analysis software  
#### Analysis Software Used:
- QUAST
- [BUSCO](https://gitlab.com/PlantGenomicsLab/HBEF#busco-analysis)
- [Bowtie2 Short-read Alignment](https://gitlab.com/PlantGenomicsLab/HBEF#bowtie2-assembly-to-short-read-illumina-data-alignment)
- [Hisat2 RNASeq Alignment](https://gitlab.com/PlantGenomicsLab/HBEF#hisat2-assembly-to-rnaseq-data-alignment)

## **1) DNA Sequencing.**  
American Beech Tree tissue was sequenced using Oxford Nanopore PromethION v9.4 and Illumina Short-read Sequencing.  
Raw long-read sequencing data located at:  
*     /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fast5_pass/
Basecalled long-read sequencing data located at:  
*     /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fastq_pass/

### Nanoplot Analysis of Promethion Long-read Data
Nanoplot report html file for raw long-reads located in **nanoplot_fagr_promethion_reads** directory.  
**Long-read Sequencing Coverage:** 190x  

Raw short-read Illumina data located at:  
*     /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/

**Raw Short-Read Sequencing Coverage:** 206x  

## **2) Refine Input Reads.**  

## **Centrifuge Classification:**  
**Full Centrifuge Classification Report:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=533338587  
Script is located in **centrifuge** directory.  

F. grandifolia input long-reads were analyzed with centrifuge sequence classification program, results are on project spreadsheet.  
Script information in **centrifuge.sh**  
min-hit-len parameter is raised to 50 to correct for false-positive classification hits in error-prone minion data.  
*     Input: Raw long-reads (190x coverage), and Bacteria, Aarchaea, Viruses, and Human genomic sequence index database 
      Output: Centrifuge sequence classification report  
      (Genomic sequence index available for download on official Centrifuge website: https://ccb.jhu.edu/software/centrifuge/manual.shtml)  
**Note:** Shasta and Flye long-read only assemblies ran with cleaned reads did not have significantly improved BUSCO completeness scores. The initial sequecing reads were used for every step of the Shasta and Flye long-read only genome assemblies.

**Primary Contaminants in Long-read Data:**  
- synthetic construct
    - NumReads: 6917
- Synechococcus sp. JA-3-3Ab
    - NumReads: 2839
- Synechococcales
    - NumReads: 1945

### Nanoplot Analysis of Promethion Long-read Data Post Centrifuge QC
Nanoplot report html file for raw long-reads post-QC located in **nanoplot_fagr_promethion_reads** directory.  

**Number of Reads Removed by Centrifuge QC:** 22,883  
**Long-read Sequencing Coverage Post-QC:** 189x  

## **Kraken Classification of Short-Read Data:**
**Full Kraken Classification Report:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=109705018  
Script is located in **kraken** directory.  

F. grandifolia input short-read sequences were analyzed with Kraken classification program.  
*     Input: Illumina paired short-read data (206x coverage), Kraken classification database
      Output: Unclassified and classified short-read data for each read set  
**Short-Read Sequencing Coverage After Kraken QC:**  46x  
**Genomescope Analysis of FG-2_S2 short-read set data post-QC:** http://qb.cshl.edu/genomescope/analysis.php?code=8jfDwuKsk7Yh0U34UBL7  
**Genomescope Analysis of FG-1_S1 and FG-2_S2 combined short-read sets post-QC:** http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=7HkU53hP9z075QvXDt2l

## **3) Genome Assembly.**  
Draft genome assembly is created using basecalled sequencing reads.  

## **Shasta Assembly**  
Scripts are located in **shasta_assembly** directory.  

*     Input: Basecalled Oxford Nanopore raw long-read sequences in fasta format (190x coverage)
      Output: Draft long-read genome assembly  
**FastqToFasta.py** converts basecalled PromethION fastq sequencing reads to fasta format.  
**run_shasta_fagr_assembly.sh** runs the Shasta genome assembly program with basecalled fasta reads as input and minimum accepted input read length of 10,000 base pairs.
- Key Parameters in **run_shasta_fagr_assembly.sh**:  
    - --memoryMode anonymous --memoryBacking 4K  
        - Optimal Shasta assembly memory allocation settings that can run without sudo priveleges.  

**run_shasta_fagr_assembly_minlen_500.sh** runs the Shasta genome assembly program with basecalled fasta reads as input and minimum accepted input read length of 500 base pairs.  
- Key Parameters in **run_shasta_fagr_assembly_minlen_500.sh**:  
    - --memoryMode anonymous --memoryBacking 4K  
        - Optimal Shasta assembly memory allocation settings that can run without sudo priveleges.  
    - --Reads.minReadLength 500  
        - Discards reads less than 500bp before assembly.  


**Fagus grandifolia Initial Shasta Assembly (min read length 10,000):**  
*     /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/1Shastarun/Assembly.fasta
      
      Genome Size: 482954338
      # of Contigs: 3729
      N50: 478723
      BUSCO:
        Embryophyta: C:76.2%[S:73.6%,D:2.6%],F:14.0%,M:9.8%,n:1375
        Viridiplantae: C:77.9%[S:74.9%,D:3.0%],F:15.1%,M:7.0%,n:430

**Fagus grandifolia Initial Shasta Assembly (min read length 500):**  
*     /labs/Wegrzyn/FagusGenome/fagr/shasta_assembly/2ShastaRun_minlen_500/Assembly.fasta
      
      Genome Size: 457821275
      # of Contigs: 4723
      N50: 592541
      BUSCO:
        Embryophyta: C:72.6%[S:71.3%,D:1.3%],F:16.1%,M:11.3%,n:1375
        Viridiplantae: C:76.9%[S:75.3%,D:1.6%],F:14.9%,M:8.2%,n:430

**Full Shasta QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1742128903  
**Full Shasta BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=666815748  

## **Flye Assembly**  
Script is located in **flye assembly** directory.  

*     Input: Basecalled Oxford Nanopore raw long-read sequences in fastq format (190x coverage)
      Output: Draft long-read genome assembly  
**flye_beech.sh** runs the Flye genome assembly program with basecalled fastq sequencing reads as input.  
- Key Parameters in **flye_beech.sh**:
    - --genome-size 1g
        - Rough genome size estimate.  

**Fagus grandifolia Initial Flye Assembly:**  
*     /labs/Wegrzyn/FagusGenome/fagr/flye_assembly/assembly.fasta
      
      Genome Size: 606864528
      # of Contigs: 4620
      N50: 333584
      BUSCO:
        Embryophyta: C:55.2%[S:51.8%,D:3.4%],F:22.2%,M:22.6%,n:1375
        Viridiplantae: C:60.3%[S:56.3%,D:4.0%],F:22.6%,M:17.1%,n:430

**Full Flye QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1526725252  
**Full Flye BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1402613224  

## **Masurca Short-read Assembly**  
Scripts are located in **masurca_assembly** directory.  

*     Input: Basecalled short-read Illumina sequencing reads in fastq format (206x coverage)  
      Output: Draft Illumina short-read genome assembly
**config_illumina_only.txt** contains information on input reads and parameters for Masurca genome assembly run.  
**run_masurca_fagr_short_read_assembly.sh** runs Masurca genome assembly program with short-read Illumina sequencing reads as input.
**Note About run_masurca_fagr_short_read_assembly.sh:**  Omitted line is the command used to run the "create mega reads step" on SLURM, consult the out file after every step for details on running the command and follow format in **run_masurca_fagr_short_read_assembly.sh** to configure the command properly with the UCHC job submission system.

- Key Parameters in **config_illumina_only.txt**:
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - USE_GRID=0
        - Running Masurca on the grid is currently not supported by the UCHC cluster.
    - MEGA_READS_ONE_PASS=0
        - Two passes of mega-reads for higher quality assembly.
    - JF_SIZE = 6000000000
        - JF_SIZE = rough genome size estimate x 10

**Fagus grandifolia Masurca Illumina Read Only Assembly:**  
*     /labs/Wegrzyn/FagusGenome/fagr/masurca_illumina_only_assembly/CA/final.genome.scf.fasta
      
      Genome Size: 402940948
      # of Contigs: 123062
      N50: 5288
      BUSCO:
        Embryophyta: C:63.4%[S:62.0%,D:1.4%],F:26.1%,M:10.5%,n:1375
        Viridiplantae: C:64.4%[S:61.6%,D:2.8%],F:25.8%,M:9.8%,n:430

**Full Masurca Short Read Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Short Read Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Masurca Hybrid Assembly**  
Scripts are located in **masurca_assembly** directory.  

*     Input: Basecalled short-read Illumina sequencing reads (206x coverage) and long-read Nanopore sequencing reads in fastq format (190x coverage)  
      Output: Draft masurca hybrid genome assembly  
**config.txt** contains information on input reads and parameters for Masurca genome assembly run, the assembly run using this config file only completed the "create mega-reads" step.  
The masurca assembly process encountered an error with perl during the gap consensus step after the mega-reads were created. **config_updated.txt** contains updated information to resolve the issue by not using the grid and using a revised genome estimate.  
**run_masurca_fagr_hybrid_assembly.sh** runs Masurca genome assembly program with short-read Illumina and long-read Nanopore sequencing reads as input.  
**Note about run_masurca_fagr_hybrid_assembly.sh:**  The line commented out in the script is the command used to run the "create mega reads step" on SLURM with multiple batch jobs, consult the out file when prompted for instructions on running the command. You will need to modify the default command in the out file to configure it properly with the UCHC job submission system, follow the format in **run_masurca_fagr_hybrid_assembly.sh**.

- Key Parameters in **config_updated.txt**:
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

**Fagus grandifolia Masurca Hybrid Assembly:**  
*     /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_assembly_initial_promethion_illumina/CA.mr.41.15.15.0.02/final.genome.scf.fasta

      Genome Size: 809911540
      # of Contigs: 4612
      N50: 295754
      BUSCO:
        Embryophyta: C:97.8%[S:53.2%,D:44.6%],F:0.3%,M:1.9%,n:1375
        Viridiplantae: C:96.5%[S:50.7%,D:45.8%],F:0.5%,M:3.0%,n:430

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Masurca Hybrid Assembly with Clean Read Input**
Scripts are located in **masurca_assembly_rmv_contam_input** directory.

*     Input: Basecalled short-read Illumina sequencing reads post-Kraken QC (46x) and long-read Nanopore sequencing reads post-Centrifuge QC (189x coverage) in fastq format
      Output: Draft masurca hybrid genome assembly
**config_hybrid_rmv_contam.txt** contains information on input reads and parameters for Masurca genome assembly run.  
**run_masurca_fagr_hybrid_assembly_rmv_contam_input.sh** runs Masurca genome assembly program with clean short-read Illumina and long-read Nanopore sequencing reads as input.  

- Key Parameters in **config_hybrid_rmv_contam.txt**:
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

**Fagus grandifolia Masurca Hybrid Assembly (rmv contam input):**  
*     /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/CA.mr.41.15.15.0.02/final.genome.scf.fasta

      Genome Size: 673389093
      # of Contigs: 4089
      N50: 308359
      BUSCO:
        Embryophyta: C:96.8%[S:70.8%,D:26.0%],F:1.1%,M:2.1%,n:1614
        Viridiplantae: C:97.2%[S:68.5%,D:28.7%],F:0.9%,M:1.9%,n:425

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Masurca Hybrid Assembly with New Cornell ArnotC8g Illumina Read Input**  
Scripts are located in **masurca_assembly_cornell_ArnotC8g_illumina** directory.  

*     Input: Basecalled short-read Illumina sequencing reads and long-read Nanopore sequencing reads in fastq format  
      Output: Draft masurca hybrid genome assembly  
**config_raw_new_reads.txt** contains information on input reads and parameters for Masurca genome assembly run  
**config_curated_new_reads.txt** contains information on input reads and parameters for Masurca genome assembly run using curated Kraken filtered Illumina reads  
**run_masurca_fagr_hybrid_assembly_new_reads.sh** runs Masurca genome assembly program with short-read Illumina and long-read Nanopore sequencing reads as input.  

- Key Parameters in **config_[].txt**:
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

**Fagus grandifolia Masurca Hybrid Raw Cornell ArnotC8g Illumina Reads Assembly:**  
*     /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_new_reads/CA.mr.41.15.10.0.02/final.genome.scf.fasta

      Genome Size: 722411997
      # of Contigs: 2308
      N50: 949582
      BUSCO:
        Embryophyta: C:98.2%[S:60.5%,D:37.7%],F:0.8%,M:1.0%,n:1614
        Viridiplantae: C:98.8%[S:57.9%,D:40.9%],F:0.7%,M:0.5%,n:425

**Fagus grandifolia Masurca Hybrid Cornell Curated Kraken Filtered ArnotC8g Illumina Reads Assembly:**  
*     /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_curated_illumina_new_reads/CA.mr.41.15.10.0.02/final.genome.scf.fasta

      Genome Size: 692033416
      # of Contigs: 2715
      N50: 557109
      BUSCO:
        Embryophyta: C:97.2%[S:62.6%,D:34.6%],F:1.0%,M:1.8%,n:1614
        Viridiplantae: C:98.1%[S:62.1%,D:36.0%],F:0.9%,M:1.0%,n:425

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Miniasm Assembly with Clean Read Input**  
Script is located in **miniasm_assembly** directory.

*     Input: Basecalled long-read Nanopore sequencing reads post-Centrifuge QC (189x) in fastq format
      Output: Draft miniasm genome assembly
**run_miniasm_assembly_fagr.sh** runs miniasm genome assembly program with clean long-read Nanopore sequencing reads as input.  

- Key Parameters in **run_miniasm_assembly_fagr.sh**:
    - minimap2 -x ava-ont
        - Set to Nanopore read overlap
    - miniasm -R
        - -R parameter discards clearly contaminated read from input

**Fagus grandifolia Miniasm Assembly (rmv contam input):**  
*     /labs/Wegrzyn/FagusGenome/fagr/miniasm_assembly/fagr_miniasm_assembly_promethion_rmv_contam_input.fasta

      Genome Size: 936802676
      # of Contigs: 4277
      N50: 272840
      BUSCO:
        Embryophyta: C:1.0%[S:1.0%,D:0.0%],F:4.0%,M:95.0%,n:1614
        Viridiplantae: C:1.4%[S:1.2%,D:0.2%],F:13.6%,M:85.0%,n:425

**Full Miniasm Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=172959533  
**Full Miniasm Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1006319488  

## **4) Improve Genome Assemblies.**  

## **Nanopolish to Improve Shasta Genome Assembly.**  
All scripts are located in **nanopolish/shasta_assembly_nanopolish/**  

Initial genome assemblies are improved using Nanopolish to correct error-prone long-read sequences.  
*     Input: Basecalled Oxford Nanopore raw long-read sequences in fasta format, assembly file, and sorted basecalled reads to assembly alignment in bam format  
      Output: Polished long-read shasta genome assembly  

**Note:** Draft Shasta assembly with default minimum read length of 10,000 was selected for polishing.  

Shasta Assembly Nanopolish:  
1. Basecalled fasta input reads are linked to signal-level nanopore raw fast5 data using nanopolish index.
    * **nanopolish_index_fagr_assembly.sh** indexes input fasta reads.
2. Basecalled fasta input reads are aligned to the initial Shasta assembly using minimap2, and the bam alignment file is sorted and indexed using samtools.
    * **minimap2_alignment_fagr_shasta_assembly.sh** script performs minimap2 alignment of input reads to Shasta assembly and samtools processing of alignment file.
3. Genome assembly is divided with biopython to speed up nanopolish processing.
    * **divide_genome_assembly.py** divides the Shasta assembly into six parts based on the first digit of each contig id. number.
    * Each part of the divided genome is named according to the starting digits of the contigs they contain i.e fagr_shasta_assembly_[DIGITS_1-9]_contigs.fasta 
4. Polished genome assembly contig variant call format (VCF) files are created with nanopolish.
    * Each part of the divided genome has a nanopolish script with individual output and temporary directories. The input assembly file, output directory, and temporary directory are the only parameters that need to be changed when calling nanopolish in the script. The scripts are named according to the divided genome assembly i.e **nanopolish_fagr_shasta_[DIGITS_1-9]_contigs.sh**
5. Final polished genome assembly is generated using nanopolish vcf2fasta
    * Create a fofn file with the paths to all vcf files (This can be done by using the 'find files' tool in file visualisation programs such as WinSCP to find any files starting with 'polished' and copying all the resulting file names into a text document).  
    * **nanopolish_vcf_to_fasta.sh** creates a polished assembly by reading vcf file paths from **nanopolish_contig_vcf_names.fofn**.

**Fagus grandifolia Polished Shasta Assembly (default minlen 10,000, <3kb contigs removed):**
*     /labs/Wegrzyn/FagusGenome/fagr/nanopolish/shasta_assembly_nanopolish/fagr_polished_shasta_assembly_default_minlen_3kb.fasta
      
      Genome Size: 477769144
      # of Contigs: 3513
      N50: 473078
      BUSCO:
        Embryophyta: C:90.1%[S:86.2%,D:3.9%],F:5.8%,M:4.1%,n:1375
        Viridiplantae: C:92.1%[S:88.1%,D:4.0%],F:4.0%,M:3.9%,n:430

**Full Shasta QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1742128903  
**Full Shasta BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=666815748  

## **Purge Haplotigs to Improve Masurca Hybrid Assembly:**  
Scripts are located in **purge_haplotigs** directory.  

Purge haplotigs was run on Masurca Hybrid Assembly to identify syntenic contigs and reduce the duplication redundancy of the assembly.  
    a. **minmap2_alignment_fagr_masurca_hybrid_assembly.sh** aligns the Masurca hybrid assembly to the basecalled Nanopore long-read data.  
    b. **fagr_purgehap.sh** runs purge haplotigs on the Masurca hybrid assembly in three steps.

- Key Parameters in **fagr_purgehap.sh**: 
    - First Step:
        - -b fagr_masurca_hybrid_assembly_reads.sorted.bam
            - Path to long-read to assembly alignment file.
        - -g /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/CA.mr.41.15.15.0.02/final.genome.scf.fasta
            - Path to assembly file.
    - Second Step:
        - -l 10
            - Low read depth cutoff.
        - -m 130 
            - Midpoint between 'haploid' and 'diploid' peaks.
        - -h 190
            - High read depth cutoff.
        - Consult the purgehap tutorial website for a more concrete visual example of how to set these values: https://bitbucket.org/mroachawri/purge_haplotigs/wiki/Tutorial
    - Third Step:
        - -b fagr_masurca_hybrid_assembly_reads.sorted.bam
            - Path to long-read to assembly alignment file.
    
**Fagus grandifolia Masurca Hybrid Assembly (purgehap):**
*     /labs/Wegrzyn/FagusGenome/fagr/purge_haplotigs/curated.fasta
      
      Genome Size: 598044450
      # of Contigs: 2244
      N50: 430483
      BUSCO:
        Embryophyta: C:97.1%[S:71.3%,D:25.8%],F:0.6%,M:2.3%,n:1375
        Viridiplantae: C:96.8%[S:69.8%,D:27.0%],F:0.5%,M:2.7%,n:430

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Purge-dups to Improve Masurca Hybrid Assembly:**  
Scripts are located in **purge_dups** directory.  

Purge-dups was run on the Curated Purgehap Masurca Hybrid Assembly to reduce the duplication redundancy of the assembly.  
**run_purge_dups.sh** runs purge-dups on the Curated Purgehap Masurca Hybrid Assembly. **config_masurca_assembly_post_purgehap_purgedup.json** contains information to run purge-dups.  
**pb.fofn** and **10x.fofn** contain paths to long-read data and short-read data respectively.  

Command to Generate Purge-dups Config File:  
*     /scripts/pd_config.py -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/ -s 10x.fofn -n config_masurca_assembly_purgehap.json /labs/Wegrzyn/FagusGenome/fagr/purge_haplotigs/curated.fasta pb.fofn
    - -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/
        - Path to working directory.
    - -n config_masurca_assembly_purgehap.json
        - Name of config file being generated.
    - /labs/Wegrzyn/FagusGenome/fagr/purge_haplotigs/curated.fasta
        - Path to assembly file.

- Key Parameters in **config_masurca_assembly_post_purgehap_purgedup.json**:
    - "out_dir": "fagr_masurca_purgehap_purgedup"
        - Name of ouptut directory.
    - "ispb": 0
        - Set this parameter to 0 if running with Illumina short-read data.

- Key Parameters in **run_purge_dups.sh**:
    - -p bash
        - Workload management platform, bash runs purge-dups locally.
    - /home/CAM/astarovoitov/purge_dups/src/
        - Path to purge_dups sourcecode.

**Fagus grandifolia Masurca Hybrid Assembly (post purge haplotigs & purg-dups):** **Highest Quality Assembly**    
*     /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_illumina_masurca_purgehap/fagr_masurca_purgehap_purgedup/seqs/curated.purged.fa
      
      Genome Size: 479544430
      # of Contigs: 2058
      N50: 456139
      BUSCO:
        Embryophyta: C:96.1%[S:91.3%,D:4.8%],F:1.7%,M:2.2%,n:1614
        Viridiplantae: C:97.7%[S:91.3%,D:6.4%],F:1.4%,M:0.9%,n:425

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Purge-dups to Improve Masurca Hybrid Assembly with Clean Read Input:**  
Scripts are located in **purge_dups_masurca_hybrid_assembly_rmv_contam_input** directory.  

Purge-dups was run on the Curated Purgehap Masurca Hybrid Assembly to reduce the duplication redundancy of the assembly.  
**run_purge_dups_fagr_masurca_hybrid_assembly_rmv_contam_input.sh** runs purge-dups on the Curated Purgehap Masurca Hybrid Assembly. **config_masurca_hybrid_assembly_rmv_contam_input.json** contains information to run purge-dups.  
**pb.fofn** and **10x.fofn** contain paths to long-read data and short-read data respectively.  

Command to Generate Purge-dups Config File:  
*     /scripts/pd_config.py -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/ -s 10x.fofn -n config_masurca_hybrid_assembly_rmv_contam_input.json /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/CA.mr.41.15.15.0.02/final.genome.scf.fasta pb.fofn
    - -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/
        - Path to working directory.
    - -n config_masurca_hybrid_assembly_rmv_contam_input.json
        - Name of config file being generated.
    - /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/CA.mr.41.15.15.0.02/final.genome.scf.fasta
        - Path to assembly file.

- Key Parameters in **config_masurca_hybrid_assembly_rmv_contam_input.json**:
    - "out_dir": "purge_dup_masurca_hybrid_assembly_rmv_contam_input"
        - Name of ouptut directory.
    - "ispb": 0
        - Set this parameter to 0 if running with Illumina short-read data.

- Key Parameters in **run_purge_dups_fagr_masurca_hybrid_assembly_rmv_contam_input.sh**:
    - -p bash
        - Workload management platform, bash runs purge-dups locally.
    - /home/CAM/astarovoitov/purge_dups/src/
        - Path to purge_dups sourcecode.

**Fagus grandifolia Masurca Hybrid Assembly (rmv contam input & purgedup):**  
*     /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_rmv_contam_input/seqs/final.purged.fa
      
      Genome Size: 480523771
      # of Contigs: 2630
      N50: 430395
      BUSCO:
        Embryophyta: C:96.2%[S:91.2%,D:5.0%],F:1.7%,M:2.1%,n:1614
        Viridiplantae: C:96.3%[S:90.4%,D:5.9%],F:1.9%,M:1.8%,n:425

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Purge-dups to Improve Masurca Hybrid Assembly with New Cornell ArnotC8g Illumina Read Input:**  
Scripts are located in **purge_dups_masurca_hybrid_assembly_cornell_ArnotC8g_illumina** directory.  

Purge-dups was run on the Masurca Assemblies using the Cornell long-read & ArnotC8g Illumina short-read raw & curated input to reduce the duplication redundancy of the assemblies.  
**run_purge_dups_fagr_masurca_hybrid_assembly_raw_cornell_ArnotC8g_illumina.sh** runs purge-dups on the Masurca Hybrid Raw Cornell ArnotC8g Illumina Reads Assembly. **config_masurca_hybrid_assembly_raw_cornell_ArnotC8g_new_reads.json** contains information to run purge-dups.  
**run_purge_dups_fagr_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina.sh** runs purge-dups on the Masurca Hybrid Cornell Curated Kraken Filtered ArnotC8g Illumina Reads Assembly. **config_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina_new_reads.json** contains information to run purge-dups.  
**pb.fofn** contains the path to the long-read data.  
**10x.fofn** and **10x_curated.fofn** contain paths to the raw and curated Kraken filtered Illumina reads respectively.  
Command to Generate Purge-dups Config File:  
*     /scripts/pd_config.py -l purge_dup_masurca_hybrid_assembly_[] -s 10x[].fofn -n config_masurca_hybrid_assembly_[].json /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_[]/CA.mr.41.15.10.0.02/final.genome.scf.fasta pb.fofn
    - -l purge_dup_masurca_hybrid_assembly_[]
        - Path to working directory.
    - -n config_masurca_hybrid_assembly_[].json
        - Name of config file being generated.
    - /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_[]/CA.mr.41.15.10.0.02/final.genome.scf.fasta
        - Path to assembly file.

- Key Parameters in **config_masurca_hybrid_assembly_[].json**:
    - "out_dir": "final.genome.scf"
        - Name of ouptut directory.
    - "ispb": 0
        - Set this parameter to 0 if running with Illumina short-read data.

- Key Parameters in **run_purge_dups_fagr_masurca_hybrid_assembly_[].sh**:
    - -p bash
        - Workload management platform, bash runs purge-dups locally.
    - /home/CAM/astarovoitov/purge_dups/src/
        - Path to purge_dups sourcecode.

**Fagus grandifolia Masurca Hybrid Raw Cornell ArnotC8g Illumina Reads Assembly (purgedup):**  
*     /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_raw_cornell_ArnotC8g_new_reads/final.genome.scf/seqs/final.purged.fa
      
      Genome Size: 506387496
      # of Contigs: 1564
      N50: 1369076
      BUSCO:
        Embryophyta: C:97.7%[S:92.9%,D:4.8%],F:1.1%,M:1.2%,n:1614
        Viridiplantae: C:98.8%[S:92.2%,D:6.6%],F:0.9%,M:0.3%,n:425

**Fagus grandifolia Masurca Hybrid Cornell Curated Kraken Filtered ArnotC8g Illumina Reads Assembly (purgedup):**  
*     /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina_new_reads/final.genome.scf/seqs/final.purged.fa
      
      Genome Size: 491993836
      # of Contigs: 1810
      N50: 805044
      BUSCO:
        Embryophyta: C:96.6%[S:91.4%,D:5.2%],F:1.5%,M:1.9%,n:1614
        Viridiplantae: C:97.2%[S:90.8%,D:6.4%],F:2.4%,M:0.4%,n:425

**Full Masurca Hybrid Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614  
**Full Masurca Hybrid Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183  

## **Medaka Polishing to Improve Miniasm Assembly with Clean Read Input**  
Script is located in **medaka** directory.

*     Input: Basecalled long-read Nanopore sequencing reads post-Centrifuge QC in fastq format and Miniasm Assembly file
      Output: Polished Miniasm genome assembly
**run_medaka_polishing_fagr_miniasm_assembly_promethion_rmv_contam_input.sh** runs medaka polishing on Miniasm assembly.  

- Key Parameters in **run_medaka_polishing_fagr_miniasm_assembly_promethion_rmv_contam_input.sh**:
    - -m r941_prom_high_g303
        - Corresponds to {pore}_{device}_{caller variant}_{caller version} (Pore r941, PromethION sequencing device, high accuracy basecaller, version g303)

**Fagus grandifolia Miniasm Assembly (rmv contam input & medaka polishing):**  
*     /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/consensus.fasta

      Genome Size: 953892243
      # of Contigs: 4324
      N50: 277894
      BUSCO:
        Embryophyta: C:91.9%[S:60.0%,D:31.9%],F:4.0%,M:4.1%,n:1614
        Viridiplantae: C:94.6%[S:63.5%,D:31.1%],F:3.3%,M:2.1%,n:425

**Full Miniasm Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=172959533  
**Full Miniasm Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1006319488  

## **Purge-dups to Improve Medaka Polished Miniasm Assembly with Clean Read Input:**  
Scripts are located in **purge_dups_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input** directory.  

Purge-dups was run on the Medaka Polished Miniasm Assembly to reduce the duplication redundancy of the assembly.  
**run_purge_dups_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.sh** runs purge-dups on the Curated Purgehap Masurca Hybrid Assembly. **config_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.json** contains information to run purge-dups.  
**pb.fofn** contain the path to long-read data.  

Command to Generate Purge-dups Config File:  
*     /scripts/pd_config.py -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/ -n config_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.json /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/consensus.fasta pb.fofn
    - -l /labs/Wegrzyn/FagusGenome/fagr/purge_dup/
        - Path to working directory.
    - -n config_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.json
        - Name of config file being generated.
    - /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/consensus.fasta
        - Path to assembly file.

- Key Parameters in **config_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.json**:
    - "out_dir": "purge_dup_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input"
        - Name of ouptut directory.
    - "ispb": 0
        - Set this parameter to 0 if running with Illumina short-read data.

- Key Parameters in **run_purge_dups_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input.sh**:
    - -p bash
        - Workload management platform, bash runs purge-dups locally.
    - /home/CAM/astarovoitov/purge_dups/src/
        - Path to purge_dups sourcecode.

**Fagus grandifolia Miniasm Assembly (rmv contam input, medaka polishing, & purgedup):**  
*     /labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_medaka_fagr_miniasm_assembly_promethion_rmv_contam_input/seqs/consensus.purged.fa
      
      Genome Size: 655613573
      # of Contigs: 2275
      N50: 383100
      BUSCO:
        Embryophyta: C:87.5%[S:71.0%,D:16.5%],F:5.8%,M:6.7%,n:1614
        Viridiplantae: C:90.9%[S:75.1%,D:15.8%],F:5.9%,M:3.2%,n:425

**Full Miniasm Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=172959533  
**Full Miniasm Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1006319488  

## **Medaka Polishing to Improve Flye Assembly**  
Script is located in **medaka** directory.

*     Input: Basecalled long-read Nanopore sequencing reads post-Centrifuge QC in fastq format and Flye Assembly file
      Output: Polished Flye genome assembly
**run_medaka_polishing_fagr_flye_assembly.sh** runs medaka polishing on Flye assembly.  

- Key Parameters in **run_medaka_polishing_fagr_flye_assembly.sh**:
    - -m r941_prom_high_g303
        - Corresponds to {pore}_{device}_{caller variant}_{caller version} (Pore r941, PromethION sequencing device, high accuracy basecaller, version g303)

**Fagus grandifolia Flye Assembly (medaka polishing):**  
*     /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_flye_assembly/consensus.fasta

      Genome Size: 599480499
      # of Contigs: 4881
      N50: 328287
      BUSCO:
        Embryophyta: C:95.5%[S:81.6%,D:13.9%],F:2.9%,M:1.6%,n:1614
        Viridiplantae: C:95.5%[S:78.8%,D:16.7%],F:3.5%,M:1.0%,n:425

**Full Flye QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1526725252  
**Full Flye BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1402613224  

## **Medaka Polishing to Improve Shasta Assembly**  
Script is located in **medaka** directory.

*     Input: Basecalled long-read Nanopore sequencing reads post-Centrifuge QC in fastq format and Flye Assembly file
      Output: Polished Flye genome assembly
**run_medaka_polishing_fagr_shasta_assembly_default_minlen.sh** runs medaka polishing on Flye assembly.  

- Key Parameters in **run_medaka_polishing_fagr_shasta_assembly_default_minlen.sh**:
    - -m r941_prom_high_g303
        - Corresponds to {pore}_{device}_{caller variant}_{caller version} (Pore r941, PromethION sequencing device, high accuracy basecaller, version g303)

**Fagus grandifolia Shasta Assembly (default minlen 10,000 & medaka polishing):**  
*     /labs/Wegrzyn/FagusGenome/fagr/medaka/medaka_fagr_shasta_assembly_default_minlen/consensus.fasta

      Genome Size: 481671186
      # of Contigs: 3553
      N50: 476209
      BUSCO:
        Embryophyta: C:95.0%[S:90.0%,D:5.0%],F:3.3%,M:1.7%,n:1614
        Viridiplantae: C:94.6%[S:89.2%,D:5.4%],F:4.9%,M:0.5%,n:425

**Full Shasta QUAST Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1742128903  
**Full Shasta BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=666815748  

## **5) Analysis.**

### **BUSCO Analysis:**  
Script is located in **busco_analysis** directory.  

All Fagus grandifolia genome assemblies were analyzed with busco/3.0.2b, results are on the project spreadsheet.  
*     Input: Genome assembly file being analyzed, and BUSCO lineage species data set.  
      Ouput: BUSCO genome assembly completeness report.  
BUSCO Lineage Data Sets: viridiplantae_odb10 and embryophyta_odb10 data sets were used to analyze completeness of F. grandifolia genome assemblies.  

### **Bowtie2 Assembly to Short-Read Illumina Data Alignment:**  
Scripts are located in the **bowtie2** directory.  

Curated assemblies were aligned to trimmed kraken unclassified illumina reads to assess genome quality  
**bowtie_index_fagr.sh** creates an index of a each curated genome assembly  
**bowtie_align_fagr.sh** aligns trimmed illumina short-reads to each curated genome assembly  

**Bowtie2 Short-Read to Assembly Alignment Results:**  https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1259004930  

### **Hisat2 Assembly to RNASeq Data Alignment**  
Scripts are located in **hisat2** directory  

Curated assemblies were aligned to trimmed RNASeq data to assess genome quality  
**hisat2_index_fagr.sh** creates an index of each curated genome assembly  
**hisat2_alignment.sh** aligns trimmed RNASeq data to each curated genome assembly

**Hisat2 RNASeq to Assembly Alignment Results:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1791527412  

