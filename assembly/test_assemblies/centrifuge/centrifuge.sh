#!/bin/bash
#SBATCH --job-name=centrifuge_fagr_promethion_reads
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

centrifuge -x p+h+v --report-file centrifuge_fagr_promethion_reads_report.tsv --quiet --min-hitlen 50 -q /labs/Wegrzyn/FagusGenome/fagr/reads/promethion_reads/fagr_promethion_reads_pass.fastq

