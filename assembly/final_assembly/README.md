# *Fagus grandifolia* Genome Assembly (reference version)

Masurca Hybrid Assembly with New Cornell ArnotC8g Nanopore and Illumina Read Input 

## DNA Sequencing 
American Beech leaf tissue was sequenced using ??? and Illumina short-read sequencing.  

### PromethION long-read sequencing
Basecalled long-read sequencing data located at:  
/projects/EBP/Wegrzyn/fagr/cornell/reads/Fagus_run1-4.10k.fastq.gz  

Nanoplot report html file for raw long-reads located in **nanoplot_fagr_promethion_reads** directory.  
**Long-read Sequencing Coverage:** ? 

Nanopore data was used **unfiltered**.

### Illumina short read sequencing
**Raw reads:**   
* /projects/EBP/Wegrzyn/fagr/cornell/reads/illumina_dna/ArnotC8g_S4_R1_001.fastq 
* /projects/EBP/Wegrzyn/fagr/cornell/reads/illumina_dna/ArnotC8g_S4_R2_001.fastq  
Raw Sequencing Coverage: ?  

**Kraken filtered sequencing (all contams removed):**  
* /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/ArnotC8g_S4_001_unclassified_1.fastq
* /labs/Wegrzyn/FagusGenome/fagr/reads/kraken/ArnotC8g_S4_001_unclassified_2.fastq  
**Filtered Sequencing Coverage:** ?  

**Kraken Classification of Short-Read Data:**
**Full Kraken Classification Report:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=109705018  
Script is located in **kraken** directory.  

**Short-Read Sequencing Coverage After Kraken QC:**  ?  
**Genomescope Analysis short-read set data post-QC:** ?


## **Genome Assembly**  

### **Masurca Hybrid Assemblies with Cornell ArnotC8g Illumina Raw and Kraken Filtered (Curated) Reads**  
Scripts are located in **masurca_assembly_cornell_ArnotC8g_illumina** directory.  (Full path? I'm not sure where this is on Xanadu)

* config_raw_new_reads.txt** contains information on input reads and parameters for Masurca genome assembly run  
* config_curated_new_reads.txt** contains information on input reads and parameters for Masurca genome assembly run using curated Kraken filtered Illumina reads  
* run_masurca_fagr_hybrid_assembly_new_reads.sh** runs Masurca genome assembly program with short-read Illumina and long-read Nanopore sequencing reads as input.  

- Key Parameters in **config_[].txt**:
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean read length of each Illumina read. (Is this right?)
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

#### Raw Reads Assembly  
/labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_new_reads/CA.mr.41.15.10.0.02/final.genome.scf.fasta

      Genome Size: 722411997
      # of Contigs: 2308
      N50: 949582
      BUSCO:
        Embryophyta: C:98.2%[S:60.5%,D:37.7%],F:0.8%,M:1.0%,n:1614
        Viridiplantae: C:98.8%[S:57.9%,D:40.9%],F:0.7%,M:0.5%,n:425

#### Kraken Filtered Assembly   
/labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_curated_illumina_new_reads/CA.mr.41.15.10.0.02/final.genome.scf.fasta

      Genome Size: 692033416
      # of Contigs: 2715
      N50: 557109
      BUSCO:
        Embryophyta: C:97.2%[S:62.6%,D:34.6%],F:1.0%,M:1.8%,n:1614
        Viridiplantae: C:98.1%[S:62.1%,D:36.0%],F:0.9%,M:1.0%,n:425


## Post-assembly Processing  

### Conterminator

Raw read assembly only
* https://github.com/martin-steinegger/conterminator
* Run microbial and fungal separately
* Mapping files
* Filter based on identifiers
* Quast
* Busco
* Group will review stats to see which assembly to move forward with.

### Medaka

See example here.  Do we need details of BTI nanopore technology for config?
https://gitlab.com/PlantGenomicsLab/FagusGrandifolia/-/tree/master/draft_assembly/test_assemblies#medaka-polishing-to-improve-miniasm-assembly-with-clean-read-input
* Quast  
* BUSCO  
* Verify stats with group in Slack and then move forward with Purge-dups section

### Purge-dups  
Scripts are located in **purge_dups_masurca_hybrid_assembly_cornell_ArnotC8g_illumina** directory.  

Purge-dups was run on the Masurca Assemblies using the Cornell long-read & ArnotC8g Illumina short-read raw & curated input to reduce the duplication redundancy of the assemblies.  
**run_purge_dups_fagr_masurca_hybrid_assembly_raw_cornell_ArnotC8g_illumina.sh** runs purge-dups on the Masurca Hybrid Raw Cornell ArnotC8g Illumina Reads Assembly. 
**config_masurca_hybrid_assembly_raw_cornell_ArnotC8g_new_reads.json** contains information to run purge-dups.  
**run_purge_dups_fagr_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina.sh** runs purge-dups on the Masurca Hybrid Cornell Curated Kraken Filtered ArnotC8g Illumina Reads Assembly. 
**config_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina_new_reads.json** contains information to run purge-dups.  
**pb.fofn** contains the path to the long-read data.  
**10x.fofn** and **10x_curated.fofn** contain paths to the raw and curated Kraken filtered Illumina reads respectively.  

Command to Generate Purge-dups Config File:  
/scripts/pd_config.py -l purge_dup_masurca_hybrid_assembly_[] -s 10x[].fofn -n config_masurca_hybrid_assembly_[].json /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_[]/CA.mr.41.15.10.0.02/final.genome.scf.fasta pb.fofn
    - -l purge_dup_masurca_hybrid_assembly_[]
        - Path to working directory.
    - -n config_masurca_hybrid_assembly_[].json
        - Name of config file being generated.
    - /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_[]/CA.mr.41.15.10.0.02/final.genome.scf.fasta
        - Path to assembly file.

- Key Parameters in **config_masurca_hybrid_assembly_[].json**:
    - "out_dir": "final.genome.scf"
        - Name of ouptut directory.
    - "ispb": 0
        - Set this parameter to 0 if running with Illumina short-read data.

- Key Parameters in **run_purge_dups_fagr_masurca_hybrid_assembly_[].sh**:
    - -p bash
        - Workload management platform, bash runs purge-dups locally.
    - /home/CAM/astarovoitov/purge_dups/src/
        - Path to purge_dups sourcecode.

#### Raw Reads Assembly Purge-dup   
/labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_raw_cornell_ArnotC8g_new_reads/final.genome.scf/seqs/final.purged.fa
      
      Genome Size: 506387496
      # of Contigs: 1564
      N50: 1369076
      BUSCO:
        Embryophyta: C:97.7%[S:92.9%,D:4.8%],F:1.1%,M:1.2%,n:1614
        Viridiplantae: C:98.8%[S:92.2%,D:6.6%],F:0.9%,M:0.3%,n:425

#### Kraken Filtered Assembly Purge-dup  
/labs/Wegrzyn/FagusGenome/fagr/purge_dup/purge_dup_masurca_hybrid_assembly_cornell_curated_ArnotC8g_illumina_new_reads/final.genome.scf/seqs/final.purged.fa
      
      Genome Size: 491993836
      # of Contigs: 1810
      N50: 805044
      BUSCO:
        Embryophyta: C:96.6%[S:91.4%,D:5.2%],F:1.5%,M:1.9%,n:1614
        Viridiplantae: C:97.2%[S:90.8%,D:6.4%],F:2.4%,M:0.4%,n:425

## Analysis   

### QUAST   
[All Masurca Hybrid Assembly QUAST Stats](https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=275674614)    
[All Masurca Hybrid Assembly BUSCO Stats](https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1501874183)  

### BUSCO
Script is located in **busco_analysis** directory.  

<span style="color:red">This should be updated to the latest version of busco</span> 
All Fagus grandifolia genome assemblies were analyzed with busco/3.0.2b, results are on the project spreadsheet.  
*     Input: Genome assembly file being analyzed, and BUSCO lineage species data set.  
      Ouput: BUSCO genome assembly completeness report.  
BUSCO Lineage Data Sets: viridiplantae_odb10 and embryophyta_odb10 data sets were used to analyze completeness of F. grandifolia genome assemblies.  

### Bowtie2 Alignment Short-Read Illumina Data to Assembly  
Scripts are located in the **bowtie2** directory.  

<span style="color:red">Not done yet for this assembly</span>   
Curated assemblies were aligned to trimmed kraken unclassified illumina reads to assess genome quality  
**bowtie_index_fagr.sh** creates an index of a each curated genome assembly  
**bowtie_align_fagr.sh** aligns trimmed illumina short-reads to each curated genome assembly  

**Bowtie2 Short-Read to Assembly Alignment Results:**  https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1259004930  

### Hisat2 Alignment - RNASeq Data to Assembly  
Scripts are located in **hisat2** directory  

**<span style="color:red">Not done yet for this assembly</span>**  
Curated assemblies were aligned to trimmed RNASeq data to assess genome quality  
**hisat2_index_fagr.sh** creates an index of each curated genome assembly  
**hisat2_alignment.sh** aligns trimmed RNASeq data to each curated genome assembly

**Hisat2 RNASeq to Assembly Alignment Results:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=1791527412  

