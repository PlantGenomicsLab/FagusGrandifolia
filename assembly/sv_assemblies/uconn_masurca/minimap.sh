#!/bin/bash
#SBATCH --job-name=minimap2_alignment_fagr_masurca_hybrid_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.18
module load samtools/1.9

minimap2 -ax map-ont -t 12 /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/masurca_hybrid_assembly_centrifuge_updated_index_filtered_promethion_raw_illumina_reads/CA.mr.41.17.15.0.02/primary.genome.scf.fasta /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam_updated_abvf_index.fastq --secondary=no | samtools sort -o fagr_masurca_updated_reads_minimap.sorted.bam -T reads.tmp

samtools index fagr_masurca_updated_reads_minimap.sorted.bam

