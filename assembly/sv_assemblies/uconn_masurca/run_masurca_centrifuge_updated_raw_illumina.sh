#!/bin/bash
#SBATCH --job-name=masurca_assembly_fagr_centrifuge_updated_index_filtered_promethion_raw_illumina_reads
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=36
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=185G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.1.1
module load MaSuRCA/4.0.3

### sbatch -D /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/mr_pass1 -J create_mega_reads -a 1-127 -c 16 -p general -q general --mem=100G -N 1 mr_pass1/create_mega_reads.sh

./assemble.sh




