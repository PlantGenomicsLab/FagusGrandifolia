# UConn Masurca Genome Assembly  

Masurca hybrid assembly with UConn Nanopore Centrifuge filtered and Illumina raw read input 


## Genome Assembly  

### Masurca
/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/masurca_hybrid_assembly_centrifuge_updated_index_filtered_promethion_raw_illumina_reads  

* **config.txt** contains information on input reads and parameters for Masurca genome assembly run.  
* **run_masurca_centrifuge_updated_raw_illumina.sh** runs Masurca using parameters set in config.txt.  

- Key Parameters:
  - PE= aa 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R1_001.fastq /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L001_R2_001.fastq
  - PE= ab 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-1_S1_L002_R2_001.fastq.gz
  - PE= ac 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L001_R2_001.fastq.gz
  - PE= ad 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-2_S2_L002_R2_001.fastq.gz
  - PE= ae 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L001_R2_001.fastq.gz
  - PE= af 300 50 /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R1_001.fastq.gz /labs/Wegrzyn/FagusGenome/fagr/reads/illumina_raw_reads/FG-3_S3_L002_R2_001.fastq.gz
  - NANOPORE= /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_promethion_reads_pass_rmv_contam_updated_abvf_index.fastq
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean insert length of Illumina reads. 
        - 50 is the insert standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

#### Assembly Output  
/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/masurca_hybrid_assembly_centrifuge_updated_index_filtered_promethion_raw_illumina_reads/CA.mr.41.15.15.0.02/primary.genome.scf.fasta  (449745515 kb)  

      Genome Size: 443396780  
      # of Contigs: 784  
      N50: 884622  
      BUSCO:  
        Embryophyta: C:96.7%[S:88.5%,D:8.2%],F:0.7%,M:2.6%,n:1614  
        Viridiplantae: C:97.8%[S:84.9%,D:12.9%],F:0.0%,M:2.2%,n:425  

## Post-assembly Processing  

### Purge haplotigs      

Purge haplotigs was run to reduce the undercollapsed heterozygosity in the assembly.  
/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/masurca_hybrid_assembly_centrifuge_updated_index_filtered_promethion_raw_illumina_reads/CA.mr.41.17.15.0.02/purgehaplotigs  
**minimaps.sh** alignment required to run purge haplotigs  
**fagr_purgehap.sh** contains 3 commands needed for purge haplotigs. Run the first command only, reviewing the resulting png to set parameters for the second command. Second and third can be run together.  
![peaks](fagr_masurca_updated_reads_minimap.sorted.bam.histogram.png)

**Output:**  
/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/masurca_hybrid_assembly_centrifuge_updated_index_filtered_promethion_raw_illumina_reads/CA.mr.41.17.15.0.02/purgehaplotigs/curated.fasta (443223429 kb)  
      
      Genome Size: 436967337  
      # of Contigs: 741  
      N50: 923769  
      BUSCO:  
        Embryophyta: C:96.7%[S:88.8%,D:7.9%],F:0.7%,M:2.6%,n:1614  
        Viridiplantae: C:98.1%[S:85.6%,D:12.5%],F:0.2%,M:1.7%,n:425  

## Scaffolding

### ALLMAPS  

*  

**Output:** 
/core/labs/Wegrzyn/FagusGenome/fagr/uconn_masurca/assembly/allmaps/uconn_masurca_purgehap_allmaps_2x.fasta

      Genome Size:   436757176  
      # of Contigs:  401 
      N50:   23368681
      BUSCO:
        Embryophyta: C:96.8%[S:89.1%,D:7.7%],F:0.7%,M:2.5%,n:1614
        Viridiplantae:   C:98.2%[S:86.4%,D:11.8%],F:0.2%,M:1.6%,n:425

**Full Masurca Hybrid Assembly QUAST and BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=141887741  

