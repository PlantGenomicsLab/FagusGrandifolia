# BTI Masurca Genome Assembly  

Masurca hybrid assembly with BTI ArnotC8g Nanopore < 10k filtered and Centrifuge filtered and Illumina raw read input 


## Genome Assembly  

### Masurca  
/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/masurca/  

* **config.txt** contains information on input reads and parameters for Masurca genome assembly run.  
* **run.sh** runs Masurca using parameters set in config.txt.   

- Key Parameters:
  - PE= aa 300 50 /projects/EBP/Wegrzyn/fagr/cornell_reads/illumina_dna/ArnotC8g_S4_R1_001.fastq /projects/EBP/Wegrzyn/fagr/cornell_reads/illumina_dna/ArnotC8g_S4_R2_001.fastq
  - NANOPORE= /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_cornell_run1-4.10k_reads_pass_rmv_contam_updated_abvf_index.fastq
    - PE= aa 300 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 300 is the mean insert length of Illumina reads.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 4500000000
        - JF_SIZE = rough genome size estimate x 10

#### Assembly Output  
/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta

      Genome Size: 476537480
      # of Contigs: 634
      N50: 1073956
      BUSCO:
        Embryophyta: C:96.9%[S:88.4%,D:8.5%],F:0.8%,M:2.3%,n:1614
        Viridiplantae: C:97.9%[S:90.4%,D:7.5%],F:0.5%,M:1.6%,n:425

## Post-assembly Processing  

### Purge haplotigs      

Purge haplotigs was run to reduce the undercollapsed heterozygosity in the assembly.  
/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/masurca/CA.mr.41.17.15.0.02/purgehaplotigs  
**minimap.sh** alignment required to run purge haplotigs  
**fagr_purgehap.sh** contains 3 commands needed for purge haplotigs. Run the first command only, reviewing the resulting png to set parameters for the second command. Second and third can be run together.  
![peaks](fagr_masurca_bti_updated_reads_minimap.sorted.bam.histogram.png)  

**Output:**  
/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/masurca/CA.mr.41.17.15.0.02/purgehaplotigs/curated.fasta
      
      Genome Size: 468385953
      # of Contigs: 576
      N50: 1082620
      BUSCO:
        Embryophyta: C:96.9%[S:88.7%,D:8.2%],F:0.8%,M:2.3%,n:1614
        Viridiplantae: C:97.9%[S:90.6%,D:7.3%],F:0.5%,M:1.6%,n:425

## Scaffolding

### ALLMAPS  

*  

**Output:** 
/core/labs/Wegrzyn/FagusGenome/fagr/cornell_masurca/assembly/allmaps/bti_masurca_purgehap_allmaps_2x.fasta  

      Genome Size: 468417453  
      # of Contigs: 271  
      N50: 27544964  
      BUSCO:
        Embryophyta: C:97.0%[S:89.1%,D:7.9%],F:0.7%,M:2.3%,n:1614
        Viridiplantae: C:97.9%[S:90.4%,D:7.5%],F:0.5%,M:1.6%,n:425  

**Full Masurca Hybrid Assembly QUAST and BUSCO Stats:** https://docs.google.com/spreadsheets/d/1BpejI6GWbQ1wIbJyBY2dbGRJqK0T32BIdYyORMEscrk/edit#gid=141887741  
