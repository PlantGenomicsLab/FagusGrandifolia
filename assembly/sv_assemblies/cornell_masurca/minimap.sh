#!/bin/bash
#SBATCH --job-name=minimap2cornell
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.18
module load samtools/1.9

minimap2 -ax map-ont -t 20 /labs/Wegrzyn/FagusGenome/fagr/slm_masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta /labs/Wegrzyn/FagusGenome/fagr/centrifuge_classification/fagr_cornell_run1-4.10k_reads_pass_rmv_contam_updated_abvf_index.fastq --secondary=no | samtools sort -@ 20 -m 15G -o fagr_masurca_bti_updated_reads_minimap.sorted.bam -T $HOME/reads.tmp

samtools index fagr_masurca_bti_updated_reads_minimap.sorted.bam

