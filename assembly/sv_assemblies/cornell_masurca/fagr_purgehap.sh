#!/bin/bash
#SBATCH --job-name=purgehre_fagr_masurca_hybrid_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=140G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o purgehapre_%j.out
#SBATCH -e purgehapre_%j.err

module load bedtools/2.25.0
module load samtools/1.7
module load minimap2/2.17
module load MUMmer/4.0.2
module load perl/5.28.1
module load R/3.6.0

#/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs readhist -b fagr_masurca_bti_updated_reads_minimap.sorted.bam -g /labs/Wegrzyn/FagusGenome/fagr/slm_masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta -t 25

#/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs contigcov -i fagr_masurca_bti_updated_reads_minimap.sorted.bam.gencov -l 2 -m 37 -h 125

#/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs contigcov -i fagr_masurca_bti_updated_reads_minimap.sorted.bam.gencov -l 2 -m 45 -h 125 -o coverage_stats2.csv

#/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs purge -g /labs/Wegrzyn/FagusGenome/fagr/slm_masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta -c coverage_stats.csv -b fagr_masurca_bti_updated_reads_minimap.sorted.bam -t 25

/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs purge -g /labs/Wegrzyn/FagusGenome/fagr/slm_masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta -c coverage_stats2.csv -b fagr_masurca_bti_updated_reads_minimap.sorted.bam -t 25 -o curated2

