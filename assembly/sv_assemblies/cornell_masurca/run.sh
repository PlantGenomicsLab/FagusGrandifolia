#!/bin/bash
#SBATCH --job-name=slmmasurca
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=47
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load perl/5.32.1
### sbatch -D /labs/Wegrzyn/FagusGenome/fagr/masurca_assembly/mr_pass1 -J create_mega_reads -a 1-127 -c 16 -p himem -q himem --mem=100G -N 1 mr_pass1/create_mega_reads.sh

#/labs/Wegrzyn/local_software/MaSuRCA-4.0.3/bin/masurca config.txt

./assemble.sh


